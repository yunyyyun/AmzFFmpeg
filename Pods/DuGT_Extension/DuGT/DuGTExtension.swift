//
//  DuGTExtension.swift
//  DuGT
//
//  Created by mengwei on 2020/11/12.
//  Copyright © 2020 mengwei. All rights reserved.
//

import Foundation
import CTMediator

public extension CTMediator {
    func DuGT_startValidate(challenge: String, gt: String, serverStatus: Int, result: @escaping (Bool) -> Void) {
        var params: [AnyHashable: Any] = [:]
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuGT"
        params["challenge"] = challenge
        params["gt"] = gt
        params["serverStatus"] = serverStatus
        params["result"] = result
        self.performTarget("DuGTManager", action: "startValidate", params: params, shouldCacheTarget: false)
    }
}


