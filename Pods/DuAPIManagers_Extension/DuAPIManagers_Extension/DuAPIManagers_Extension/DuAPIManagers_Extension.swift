//
//  DuAPIManagers_Extension.swift
//  DuAPIManagers_Extension
//
//  Created by casa on 2020/3/18.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator
import CTNetworkingSwift
import Alamofire

let ModuleName = "DuAPIManagers"

public extension CTMediator {
    
    func DuAPIManagers_commonErrorHandler(response: AFDataResponse<Data?>?) -> Bool {
        let params = [
            "response": response as Any,
            kCTMediatorParamsKeySwiftTargetModuleName: "DuAppEngineModule"
            ] as [String : Any]
        performTarget("DuAppErrorHandler", action: "commonErrorHandler", params: params, shouldCacheTarget: true)
        return true
    }
    
    func DuAPIManagers_loginAPIManager(paramSource:CTNetworkingAPIManagerParamSource, delegate:CTNetworkingAPIManagerCallbackDelegate) -> CTNetworkingAPIManager? {
        let params = [
            "paramSource":paramSource,
            "delegate":delegate,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        if let loginAPIManager = performTarget(ModuleName, action: "loginAPIManager", params: params, shouldCacheTarget: true) as? CTNetworkingAPIManager {
            return loginAPIManager
        }
        return nil
    }
    
    func DuAPIManagers_prepareForPasswordLogin(loginAPIManager:CTNetworkingAPIManager) {
        let params = [
            "loginAPIManager":loginAPIManager as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "prepareForPasswordLogin", params: params, shouldCacheTarget: true)
    }
    
    func DuAPIManagers_prepareForVerifyCodeLogin(loginAPIManager:CTNetworkingAPIManager) {
        let params = [
            "loginAPIManager":loginAPIManager,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "prepareForVerifyCodeLogin", params: params, shouldCacheTarget: true)
    }
    
    func DuAPIManagers_loginAPIParams(phoneNumber:String?, password:String?, loginAPIManager:CTNetworkingAPIManager?) -> Parameters? {
        guard let phoneNumber = phoneNumber else { return nil }
        guard let password = password else { return nil }

        let params = [
            "phoneNumber":phoneNumber,
            "password":password,
            "loginAPIManager":loginAPIManager as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        if let apiParams = performTarget(ModuleName, action: "loginAPIParams", params: params, shouldCacheTarget: true) as? Parameters {
            return apiParams
        }
        return nil
    }
    
    func DuAPIManagers_sendVerifyCodeAPIManager(paramSource:CTNetworkingAPIManagerParamSource, delegate:CTNetworkingAPIManagerCallbackDelegate) -> CTNetworkingAPIManager? {
        let params = [
            "paramSource":paramSource,
            "delegate":delegate,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        if let sendVerifyCodeAPIManager = performTarget(ModuleName, action: "sendVerifyCodeAPIManager", params: params, shouldCacheTarget: true) as? CTNetworkingAPIManager {
            return sendVerifyCodeAPIManager
        }
        return nil
    }
    
    func DuAPIManagers_sendVerifyCodeAPIParams(phoneNumber:String?, sendVerifyCodeAPIManager:CTNetworkingAPIManager?) -> Parameters? {
        guard let phoneNumber = phoneNumber else { return nil }

        let params = [
            "phoneNumber":phoneNumber,
            "sendVerifyCodeAPIManager":sendVerifyCodeAPIManager as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        if let apiParams = performTarget(ModuleName, action: "sendVerifyCodeAPIParams", params: params, shouldCacheTarget: true) as? Parameters {
            return apiParams
        }
        return nil
    }
    
    func DuAPIManagers_prepareForSendLoginVerifyCode(sendVerifyCodeAPIManager:CTNetworkingAPIManager) {
        let params = [
            "sendVerifyCodeAPIManager":sendVerifyCodeAPIManager as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "prepareForSendLoginVerifyCode", params: params, shouldCacheTarget: true)
    }
    
    func DuAPIManagers_changeAPIEnv(callback:((String)->Void)?) {
        let params = [
            "callback":callback as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName,
        ] as [AnyHashable : Any]
        performTarget(ModuleName, action: "changeAPIEnv", params: params, shouldCacheTarget: false)
    }
    
    func DuAPIManagers_currentEnvDescription() -> String {
        let params = [
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        if let result = performTarget(ModuleName, action: "currentEnvDescription", params: params, shouldCacheTarget: false) as? String {
            return result
        }
        return "无法获得当前API环境"
    }
    
    func DuAPIManagers_logout(callback:(() -> Void)?) {
        let params = [
            "callback":callback as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "logout", params: params, shouldCacheTarget: true)
    }
    
    func DuAPIManagers_didReceiveLoginAPIResponse(responseInfo:[AnyHashable:Any]?) {
        let params = [
            "responseInfo":responseInfo as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "didReceiveLoginAPIResponse", params: params, shouldCacheTarget: true)
    }
    
    func DuAPIManager_didChangeAPIEnv(env:String) {
        let params = [
            "env":env as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "didChangeAPIEnv", params: params, shouldCacheTarget: true)
    }

    func DuAPIManagers_currentPHPBaseURL() -> String {
        let params = [
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        if let result = performTarget(ModuleName, action: "currentPHPBaseURL", params: params, shouldCacheTarget: true) as? String {
            return result
        }
        return ""
    }
    
    func DuAPIManagers_currentJAVABaseURL() -> String {
        let params = [
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        if let result = performTarget(ModuleName, action: "currentJAVABaseURL", params: params, shouldCacheTarget: true) as? String {
            return result
        }
        return ""
    }
    
    @objc func DuAPIManagers_enableJavaFormSeverTrustEvaluating(enable: Bool)  {
        let params = [
            "enable":enable as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "enableJavaFormSeverTrustEvaluating", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_setJavaFormCertificatesPath(_ path: String)  {
        let params = [
            "path":path as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "setJavaFormCertificatesPath", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_enableJavaJsonSeverTrustEvaluating(enable: Bool)  {
        let params = [
            "enable":enable as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "enableJavaJsonSeverTrustEvaluating", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_setJavaJsonCertificatesPath(_ path: String)  {
        let params = [
            "path":path as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "setJavaJsonCertificatesPath", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_enablePhpFormSeverTrustEvaluating(enable: Bool)  {
        let params = [
            "enable":enable as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "enablePhpFormSeverTrustEvaluating", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_setPhpFormCertificatesPath(_ path: String)  {
        let params = [
            "path":path as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "setPhpFormCertificatesPath", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_enablePhpJsonSeverTrustEvaluating(enable: Bool)  {
        let params = [
            "enable":enable as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "enablePhpJsonSeverTrustEvaluating", params: params, shouldCacheTarget: true)
    }
    
    @objc func DuAPIManagers_setPhpJsonCertificatesPath(_ path: String)  {
        let params = [
            "path":path as Any,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "setPhpJsonCertificatesPath", params: params, shouldCacheTarget: true)
    }
}
