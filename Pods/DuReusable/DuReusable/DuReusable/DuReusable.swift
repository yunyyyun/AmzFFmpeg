//
//  DuReusable.swift
//  DuReusable
//
//  Created by Todd Cheng on 2020/4/29.
//  Copyright © 2020 DuApp. All rights reserved.
//

import UIKit

public protocol DUReusable: class {
    static var reuseIdentifier: String { get }
}

public extension DUReusable where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: DUReusable {}

extension UITableViewHeaderFooterView: DUReusable {}

extension UICollectionReusableView: DUReusable {}
