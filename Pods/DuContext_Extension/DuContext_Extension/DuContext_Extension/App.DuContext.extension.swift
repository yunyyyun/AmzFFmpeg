//
//  App.DuContext.extension.swift
//  DuContext
//
//  Created by casa on 2020/3/17.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator
import DuShuMeiSDK_Extension

public extension CTMediator {
    func DuContext_App_Version() -> String {
        return DuContext_fetchString(key: .AppVersion)
    }
    
    func DuContext_App_Id() -> String {
        return "duapp"
    }
    
    func DuContext_App_Mode() -> String {
        return "0"
    }

    func DuContext_App_Token() -> String {
        return "JLIjsdLjfsdII%3D%7CMTQxODg3MDczNA%3D%3D%7C07aaal32795abdeff41cc9633329932195"
    }
    
    func DuContext_App_ShuMeiID() -> String {
        return duShuMeiSDK_fetchId()
    }
    
    func DuContext_App_BuildVersion() -> String {
        return DuContext_fetchString(key: .BuildVersion)
    }
    
    func DuContext_App_PinkVersion() -> String {
        return DuContext_fetchString(key: .PinkVersion)
    }
    
    func DuContext_App_Language() -> String {
        return DuContext_fetchString(key: .Language)
    }
    
    func DuContext_App_LanguageCode() -> String {
        return DuContext_fetchString(key: .LanguageCode)
    }
    
    func DuContext_App_IsLoginPagePresenting() -> Bool {
        return DuContext_fetchBool(key: .IsLoginPagePresenting)
    }
    
    func DuContext_App_PostsTitles() -> [Dictionary<AnyHashable, Any>]? {
        if let result = DuContext_fetchAny(key: .PostsTitles) as? [[String: Any]] {
            return result
        }
       
        return nil
    }
    
    func DuContext_App_SetPostsTitles(titles:Any?) {
        guard let postsTitles = titles as? [[AnyHashable:Any]] else { return }
        DuContext_set(key: .PostsTitles, value: postsTitles, shouldWriteToDisk: true)
    }
}
