//
//  User.DuContext.extension.swift
//  DuContext
//
//  Created by casa on 2020/3/17.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

public extension CTMediator {
    
    func DuContext_User_UserId() -> Int? {
        let userId = DuContext_fetchString(key: .UserID)
        if userId.count > 0 {
            return Int(userId)
        }
        return nil
    }
    
    func DuContext_User_UserIdString() -> String {
        return DuContext_fetchString(key: .UserID)
    }
    
    func DuContext_User_UserName() -> String {
        return DuContext_fetchString(key: .UserName)
    }
    
    func DuContext_User_UserIcon() -> String {
        return DuContext_fetchString(key: .UserIcon)
    }
    
    func DuContext_User_Sex() -> Int? {
        return DuContext_fetchOptionalInt(key: .Sex)
    }
    
    func DuContext_User_SexString() -> String {
        if let sex = DuContext_User_Sex() {
            return "\(sex)"
        }
        return ""
    }
    
    func DuContext_User_IdioGraph() -> String {
        return DuContext_fetchString(key: .IdioGraph)
    }
    
    func DuContext_User_FormatTime() -> String {
        return DuContext_fetchString(key: .FormatTime)
    }
    
    func DuContext_User_Banned() -> Int {
        return DuContext_fetchInt(key: .Banned)
    }
    
    func DuContext_User_VIP() -> Int {
        return DuContext_fetchInt(key: .VIP)
    }
    
    func DuContext_User_VIPLogo() -> String {
        return DuContext_fetchString(key: .VIPLogo)
    }
    
    func DuContext_User_Code() -> String {
        return DuContext_fetchString(key: .Code)
    }
    
    func DuContext_User_Amount() -> Int {
        return DuContext_fetchInt(key: .Amount)
    }
    
    func DuContext_User_AmountSign() -> String {
        return DuContext_fetchString(key: .AmountSign)
    }
    
    func DuContext_User_IsOlder() -> Int {
        return DuContext_fetchInt(key: .IsOlder)
    }
    
    func DuContext_User_IsQuestionExpert() -> Int {
        return DuContext_fetchInt(key: .IsQuestionExpert)
    }
    
    func DuContext_User_IsAdmin() -> Int {
        return DuContext_fetchInt(key: .IsAdmin)
    }
    
    func DuContext_User_IsBindMobile() -> Int {
        return DuContext_fetchInt(key: .IsBindMobile)
    }
    
    func DuContext_User_IsComplete() -> String {
        return DuContext_fetchString(key: .IsComplete)
    }
    
    func DuContext_User_JoinDays() -> Int {
        return DuContext_fetchInt(key: .JoinDays)
    }
    
    func DuContext_User_IsCommunityAgreements() -> Int {
        return DuContext_fetchInt(key: .IsCommunityAgreements)
    }
    
    func DuContext_User_CountryCode() -> String {
        return DuContext_fetchString(key: .CountryCode)
    }
    
    func DuContext_User_SpecialList() -> String {
        return DuContext_fetchString(key: .SpecialList)
    }
    
    func DuContext_User_IsMerchant() -> Int {
        return DuContext_fetchInt(key: .IsMerchant)
    }
    
    func DuContext_User_IsReadProtocol() -> String {
        return DuContext_fetchString(key: .IsReadProtocol)
    }
    
    func DuContext_User_IsCertify() -> Int {
        return DuContext_fetchInt(key: .IsCertify)
    }
    
    func DuContext_User_IsRegister() -> Int {
        return DuContext_fetchInt(key: .IsRegister)
    }
    
    func DuContext_User_LoginToken() -> String {
        return DuContext_fetchString(key: .LoginToken)
    }
    
    func DuContext_User_OpenImUserId() -> String {
        return DuContext_fetchString(key: .OpenImUserId)
    }
    
    func DuContext_User_OpenImPassword() -> String {
        return DuContext_fetchString(key: .OpenImPassword)
    }
    
    func DuContext_User_XAuthToken() -> String {
        return DuContext_fetchString(key: .XAuthToken)
    }
    
    func DuContext_User_IsGuest() -> Bool {
        let loginToken = DuContext_User_LoginToken()
        if loginToken.count > 0 {
            return false
        }
        return true
    }
    
    func DuContext_User_Email() -> String {
        return DuContext_fetchString(key: .Email)
    }
    
    func DuContext_User_Mobile() -> String {
        return DuContext_fetchString(key: .Mobile)
    }
    
    func DuContext_User_Language() -> String {
        return DuContext_fetchString(key: .UserLanguage)
    }
    
    func DuContext_User_TimeZone() -> String {
        return DuContext_fetchString(key: .UserTimeZone)
    }
    
    func DuContext_User_IsDel() -> Int {
        return DuContext_fetchInt(key: .IsDel)
    }
    
    func DuContext_User_CreateTime() -> Int {
        return DuContext_fetchInt(key: .CreateTime)
    }
    
    func DuContext_User_ModifyTime() -> Int {
        return DuContext_fetchInt(key: .ModifyTime)
    }
    
    func DuContext_User_InnerUserId() -> Int {
        return DuContext_fetchInt(key: .InnerUserId)
    }
    
    func DuContext_User_Status() -> Int {
        return DuContext_fetchInt(key: .Status)
    }
    
    func DuContext_User_Queue() -> Int {
        return DuContext_fetchInt(key: .Queue)
    }
    
    func DuContext_User_Token() -> String {
        return DuContext_fetchString(key: .Token)
    }
    
    func DuContext_User_IsIdentifyExpert() -> Int {
        return DuContext_fetchInt(key: .IsIdentifyExpert)
    }
    
    func DuContext_User_IsSetLoginPassword() -> Bool {
        return DuContext_fetchBool(key: .IsSetLoginPassword)
    }
    
    func DuContext_User_IsSetWithdrawPassword() -> Bool {
        return DuContext_fetchBool(key: .IsSetWithdrawPassword)
    }

    func DuContext_User_PublishTrendUserName() -> String {
        return DuContext_fetchString(key: .PublishTrendUserName)
    }
    
    func DuContext_User_CustomerURL() -> String {
        return DuContext_fetchString(key: .CustomerURL)
    }
    
    func DuContext_User_AccountType() -> Int {
        return DuContext_fetchInt(key: .AccountType)
    }
    
    func DuContext_User_Account() -> String {
        return DuContext_fetchString(key: .Account)
    }
    
    func DuContext_User_AccountName() -> String {
        return DuContext_fetchString(key: .AccountName)
    }
    
    func DuContext_User_Balance() -> Int {
        return DuContext_fetchInt(key: .Balance)
    }
    
    func DuContext_GuestUserIdString() -> String {
        return DuContext_fetchString(key: .GuestUserId)
    }
    
    func DuContext_User_CleanAll() {
        DuContext_remove(key: .CreateTime)
        DuContext_remove(key: .ModifyTime)
        DuContext_remove(key: .InnerUserId)
        DuContext_remove(key: .IsDel)
        DuContext_remove(key: .UserLanguage)
        DuContext_remove(key: .UserTimeZone)
        DuContext_remove(key: .Email)
        DuContext_remove(key: .LoginToken)
        DuContext_remove(key: .XAuthToken)
        DuContext_remove(key: .UserID)
        DuContext_remove(key: .UserName)
        DuContext_remove(key: .PublishTrendUserName)
        DuContext_remove(key: .UserIcon)
        DuContext_remove(key: .SpecialList)
        DuContext_remove(key: .Sex)
        DuContext_remove(key: .IdioGraph)
        DuContext_remove(key: .Banned)
        DuContext_remove(key: .FormatTime)
        DuContext_remove(key: .Mobile)
        DuContext_remove(key: .VIP)
        DuContext_remove(key: .Code)
        DuContext_remove(key: .Group)
        DuContext_remove(key: .Amount)
        DuContext_remove(key: .AmountSign)
        DuContext_remove(key: .IsIdentifyExpert)
        DuContext_remove(key: .IsQuestionExpert)
        DuContext_remove(key: .RecommendReason)
        DuContext_remove(key: .Account)
        DuContext_remove(key: .AccountName)
        DuContext_remove(key: .AccountType)
        DuContext_remove(key: .IsOlder)
        DuContext_remove(key: .IsAdmin)
        DuContext_remove(key: .IsBindMobile)
        DuContext_remove(key: .IsMerchant)
        DuContext_remove(key: .IsReadProtocol)
        DuContext_remove(key: .JoinDays)
        DuContext_remove(key: .IsCertify)
        DuContext_remove(key: .IsCommunityAgreements)
        DuContext_remove(key: .Balance)
        DuContext_remove(key: .CustomerURL)
        DuContext_remove(key: .IsComplete)
        DuContext_remove(key: .CountryCode)
        DuContext_remove(key: .IsRegister)
        DuContext_remove(key: .OpenImUserId)
        DuContext_remove(key: .OpenImPassword)
        DuContext_remove(key: .Status)
        DuContext_remove(key: .Queue)
        DuContext_remove(key: .Token)
        DuContext_remove(key: .Mobile)
        DuContext_remove(key: .IsSetLoginPassword)
        DuContext_remove(key: .IsSetWithdrawPassword)
        DuContext_remove(key: .PublishTrendUserName)
        DuContext_remove(key: .CookieToken)
        DuContext_remove(key: .GuestUserId)
    }
}
