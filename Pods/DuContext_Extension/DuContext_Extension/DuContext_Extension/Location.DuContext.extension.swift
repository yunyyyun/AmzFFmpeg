//
//  Location.DuContext.extension.swift
//  DuContext
//
//  Created by casa on 2020/3/17.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

public extension CTMediator {
    func DuContext_Location_Latitude() -> String {
        return DuContext_fetchString(key: .Latitude)
    }
    
    func DuContext_Location_Longitude() -> String {
        return DuContext_fetchString(key: .Longitude)
    }
    
    func DuContext_Location_Country_State_City() -> String {
        return DuContext_fetchString(key: .Country_State_City)
    }
    
    func DuContext_Location_City() -> String {
        return DuContext_fetchString(key: .City)
    }
}
