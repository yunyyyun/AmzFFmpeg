//
//  Device.DuContext.extension.swift
//  DuContext
//
//  Created by casa on 2020/3/17.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

public extension CTMediator {
    // is proxy
    func DuContext_Device_IsProxy() -> Bool {
        return DuContext_fetchBool(key: .IsProxy)
    }
    func DuContext_Device_IsProxyString() -> String {
        return DuContext_Device_IsProxy() ? "1" : "0"
    }
    
    // is simulator
    func DuContext_Device_IsSimulator() -> Bool {
        return DuContext_fetchBool(key: .IsSimulator)
    }
    func DuContext_Device_IsSimulatorString() -> String {
        DuContext_Device_IsSimulator() ? "1" : "0"
    }
    
    // is device broken
    func DuContext_Device_isBroken() -> Bool {
        return DuContext_fetchBool(key: .IsRoot)
    }
    func DuContext_Device_isBrokenString() -> String {
        return DuContext_Device_isBroken() ? "1" : "0"
    }
    
    func DuContext_Device_ModelName() -> String {
        return DuContext_fetchString(key: .ModelName)
    }
    
    func DuContext_Device_UUID() -> String {
        return DuContext_fetchString(key: .UUID)
    }
    
    func DuContext_Device_Platform() -> String {
        return "iPhone"
    }
    
    func DuContext_Device_Timestamp() -> String {
        let gap = Int64(DuContext_fetchInt(key: .TimestampGap))
        let now = Int64(Date().timeIntervalSince1970 * 1000)
        return "\(now+gap)"
    }
    
    func DuContext_Device_SetServerTimeStamp(serverTimeStamp:Int64) {
        let now = Int64(Date().timeIntervalSince1970 * 1000)
        DuContext_set(key: .TimestampGap, value: serverTimeStamp-now)
    }
    
    func DuContext_Device_Power() -> String {
        return DuContext_fetchString(key: .Power)
    }
    
    func DuContext_Device_Motion(motionCallBack: @escaping ((String) -> Void)) {
        DuContext_set(key: .Motion, value: motionCallBack)
        _ = DuContext_fetchString(key: .Motion)
    }
    
}
