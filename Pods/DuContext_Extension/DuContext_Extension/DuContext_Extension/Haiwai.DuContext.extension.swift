//
//  Haiwai.DuContext.extension.swift
//  DuContext_Extension
//
//  Created by casa on 2020/3/26.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator
import DuAbroadLocalize

public extension CTMediator {
    
    /// Configs
    
    func DuContext_Haiwai_Config_Terms() -> String {
        return DuContext_fetchString(key: .Terms)
    }
    
    func DuContext_Haiwai_Config_PrivacyPolicy() -> String {
        return DuContext_fetchString(key: .PrivacyPolicy)
    }
    
    func DuContext_Haiwai_Config_IdentifyRules() -> String {
        return DuContext_fetchString(key: .IdentifyRules)
    }
    
    func DuContext_Haiwai_Config_IdentifyTemplateUrl() -> String {
        return DuContext_fetchString(key: .IdentifyTemplateUrl)
    }
    
    func DuContext_Haiwai_Config_IsRegisterCode() -> Bool {
        return DuContext_fetchBool(key: .IsRegisterCode)
    }
    
    func DuContext_Haiwai_Config_Sale() -> String {
        return DuContext_fetchString(key: .Sale)
    }
    
    func DuContext_Haiwai_Config_Deliver() -> String {
        return DuContext_fetchString(key: .Deliver)
    }
    
    /// 卖家须知
    func DuContext_Haiwai_PagePath_FAQ() -> String {
        return DuContext_fetchString(key: .FaqUrl)
    }
    
    /// 买家须知
    func DuContext_Haiwai_BuyerFAQ() -> String {
        return DuContext_fetchString(key: .buyerFAQ)
    }
    
    func DuContext_Haiwai_SellerDeliveryInstructionText() -> String {
        return DuContext_fetchString(key: .SellerDeliveryInstructionText)
    }
    /// 自配送须知
    func DuContext_Haiwai_AinDeliveryMsg() -> String {
        return DuContext_fetchString(key: .AinDeliveryMsg)
    }
    
    func DuContext_Haiwai_share_channel() -> [Int] {
        guard let channels = DuContext_fetchAny(key: .ShareChannels) as? [Int] else {
            return []
        }
        return channels
    }
    
    /// User related
    
    func DuContext_Haiwai_User_isFirstLogin() -> Bool {
        return DuContext_fetchBool(key: .IsFirstLogin)
    }
    
    func DuContext_Haiwai_User_IdentifyGuideViewShown() -> Bool {
        return DuContext_fetchBool(key: .IdentifyGuideViewShown)
    }
    
    func DuContext_Haiwai_User_GuideImage() -> UIImage? {
        /// NOTE: use `UIImage(data:)` will block main thread if image is too large.
        guard let guideImageData = DuContext_fetchAny(key: .GuideImage) as? Data,
            let guideImage = UIImage(data: guideImageData) else { return nil }
        return guideImage
    }
    
    func DuContext_Haiwai_User_RegisterGuideImageString() -> String {
        return DuContext_fetchString(key: .RegisterGuideImageString)
    }
    
    func DuContext_Haiwai_User_UserInfoId() -> Int {
        return DuContext_fetchInt(key: .UserInfoId)
    }
    
    func DuContext_Haiwai_User_PaypalAccount() -> String {
        return DuContext_fetchString(key: .PaypalAccount)
    }
    
    func DuContext_Haiwai_User_SoldNum() -> Int {
        return DuContext_fetchInt(key: .SoldNum)
    }
    
    func DuContext_Haiwai_User_IdentifyNum() -> Int {
        return DuContext_fetchInt(key: .IdentifyNum)
    }
    
    func DuContext_Haiwai_UserIdString() -> String {
        return DuContext_fetchString(key: .UserIdString)
    }
    
    func DuContext_Haiwai_hasPassword() -> Bool {
        return DuContext_fetchBool(key: .hasPassword)
    }
    
    func DuContext_Haiwai_User_UnDeliverNum() -> Int {
        return DuContext_fetchInt(key: .UnDeliverNum)
    }
    
    func DuContext_Haiwai_User_LastLoginEmail(_ region: Region) -> String {
        if let dict = DuContext_fetchAny(key: .LastLoginEmail) as? [String: String], dict.count > 0 {
            if let email = dict[region.rawValue], !email.isEmpty {
                return email
            }
        }
        return ""
    }
    
    func DuContext_Haiwai_User_LastSignInMobile(_ region: Region) -> String {
        if let dict = DuContext_fetchAny(key: .LastSignInMobile) as? [String: String], dict.count > 0 {
            if let mobileNumber = dict[region.rawValue], !mobileNumber.isEmpty {
                return mobileNumber
            }
        }
        return ""
    }
    
    func DuContext_Haiwai_User_AreaCode() -> String {
        return DuContext_fetchString(key: .AreaCode)
    }
    
    func DuContext_Haiwai_WebViewUserAgent() -> String {
        return DuContext_fetchString(key: .WebViewUserAgent)
    }
    
    func DuContext_Haiwai_isFirstLaunch() -> Bool {
        return DuContext_fetchBool(key: .isFirstLaunch)
    }
    
    func DuContext_Haiwai_Device() -> String {
        return DuContext_fetchString(key: .Device)
    }
    
    func DuContext_Haiwai_TimestampForNotificationNotAuthorized() -> TimeInterval {
        guard let timestamp = DuContext_fetchAny(key: .TimestampForNotificationNotAuthorized) as? TimeInterval else { return 0.0 }
        return timestamp
    }
    
    /// Return true if user login token is not empty. Otherwise return false.
    func DuContext_Haiwai_User_isLogin() -> Bool {
        return !DuContext_User_LoginToken().isEmpty
    }
    
    func HaiWai_PersonalPaypal() -> String {
        return DuContext_fetchString(key: .PersonalPaypal)
    }
    
    func DuContext_HaiWai_CommodityDetailFAQPath() -> String {
         return DuContext_fetchString(key: .CommodityDetailFAQPath)
    }
    
    /// Buyer related
    
    func DuContext_Haiwai_Buyer_UnPaymentCount() -> Int {
        return DuContext_fetchInt(key: .buyerUnPaymentCount)
    }
    
    /// 买家须知
    func DuContext_HaiWai_SellAndBuyFAQPath() -> String {
       return DuContext_fetchString(key: .SellAndBuyFAQPath)
    }
    
    func DuContext_HaiWai_biillFAQPath() -> String {
         return DuContext_fetchString(key: .biillFAQPath)
    }
        
    func DuContext_HaiWai_BillFilterSetting() -> String {
         return DuContext_fetchString(key: .BillFilterSetting)
    }
    
    func DuContext_HaiWai_SiteInfosSetting() -> String {
         return DuContext_fetchString(key: .SiteInfosSetting)
    }
    
    func DuContext_HaiWai_ClientBaseConfig() -> String {
        return DuContext_fetchString(key: .clientBaseConfig)
    }
    
    func DuContext_Haiwai_CleanAll() {
        DuContext_remove(key: .UserInfoId)
        DuContext_remove(key: .PaypalAccount)
        DuContext_remove(key: .SoldNum)
        DuContext_remove(key: .IdentifyNum)
        DuContext_remove(key: .UnDeliverNum)
        DuContext_remove(key: .AreaCode)
        DuContext_remove(key: .UserIdString)
        DuContext_remove(key: .buyerUnPaymentCount)
    }
    
}
