//
//  Networking.DuContext.extension.swift
//  DuContext
//
//  Created by casa on 2020/3/17.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

public extension CTMediator {
    func DuContext_Networking_IpAddress() -> String {
        return DuContext_fetchString(key: .IpAddress)
    }
    
    func DuContext_Networking_CookieToken() -> String {
        return DuContext_fetchString(key: .CookieToken)
    }
    
    func DuContext_Networking_StatusDescription() -> String {
        return DuContext_fetchString(key: .NetworkStatusDescriptionString)
    }
    
    func DuContext_Networking_IsReachable() -> Bool {
        return DuContext_fetchBool(key: .IsReachable)
    }
    
    func DuContext_Networking_SetJavaBaseURL(_ javaBaseURL:Any?) {
        guard let urlString = javaBaseURL as? String, urlString.count > 0 else { return }
        let resultURLString = refineURLString(urlString)
        DuContext_set(key: .JavaBaseURL, value: resultURLString, shouldWriteToDisk: true)
    }
    
    func DuContext_Networking_SetPHPBaseURL(_ phpBaseURL:Any?) {
        guard let urlString = phpBaseURL as? String, urlString.count > 0 else { return }
        let resultURLString = refineURLString(urlString)
        DuContext_set(key: .PHPBaseURL, value: resultURLString, shouldWriteToDisk: true)
    }
    
    func DuContext_Networking_JavaBaseURL() -> String {
        let fetchedResult = DuContext_fetchString(key: .JavaBaseURL)
        if fetchedResult.count > 0 {
            return fetchedResult
        }
        return "https://app.dewu.com"
    }
    
    func DuContext_Networking_PHPBaseURL() -> String {
        let fetchedResult = DuContext_fetchString(key: .PHPBaseURL)
        if fetchedResult.count > 0 {
            return fetchedResult
        }
        return "https://m.dewu.com"
    }
}

func refineURLString(_ originURLString:String) -> String {
    var refinedURLString = originURLString.replacingOccurrences(of: " ", with: "")
    if let lastChar = refinedURLString.last, lastChar == "/" {
        refinedURLString.removeLast()
    }
    return refinedURLString
}
