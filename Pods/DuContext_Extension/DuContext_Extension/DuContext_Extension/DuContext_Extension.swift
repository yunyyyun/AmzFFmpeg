//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

let ModuleName = "DuContext"

public extension CTMediator {
    func DuContext_fetchAny(key:DuContextKey) -> Any? {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        return performTarget(ModuleName, action:"fetch", params:params, shouldCacheTarget: true)
    }
    
    func DuContext_set(key:DuContextKey, value:Any, shouldWriteToDisk:Bool = false) {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            "value":value,
            "shouldWriteToDisk":shouldWriteToDisk,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        performTarget(ModuleName, action: "set", params: params, shouldCacheTarget: true)
    }
    
    func DuContext_remove(key: DuContextKey) {
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        performTarget(ModuleName, action: "remove", params: params, shouldCacheTarget: true)
    }
}

extension CTMediator {
    func DuContext_fetchString(key:DuContextKey) -> String {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        let result = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true)
        return "\(result ?? "")"
    }
    
    func DuContext_fetchOptionalString(key:DuContextKey) -> String? {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
        ] as [String : Any]
        guard let result = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true) as? String else { return nil }
        return result
    }
    
    func DuContext_fetchBool(key:DuContextKey) -> Bool {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        guard let result = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true) as? Bool else { return false }
        return result
    }
    
    func DuContext_fetchInt(key:DuContextKey) -> Int {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        guard let result = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true) as? Int else { return 0 }
        return result
    }
    
    func DuContext_fetchOptionalInt(key:DuContextKey) -> Int? {
        DuContext_prepareIfNeeded()
        let params = [
            "key":key,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
            ] as [String : Any]
        guard let result = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true) as? Int else { return nil }
        return result
    }
    
    private func DuContext_prepareIfNeeded() {
        var params = [
            "key":DuContextKey.Token,
            kCTMediatorParamsKeySwiftTargetModuleName:ModuleName,
        ] as [String : Any]
        
        if let token = performTarget(ModuleName, action: "fetch", params: params, shouldCacheTarget: true) as? String, token.count > 0 {
            return
        }
        
        params = [kCTMediatorParamsKeySwiftTargetModuleName:ModuleName]
        performTarget(ModuleName, action: "prepare", params: params, shouldCacheTarget: true)
    }
}
