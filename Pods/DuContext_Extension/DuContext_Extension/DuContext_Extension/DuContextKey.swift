//
//  DuContextKey.swift
//  DuContext
//
//  Created by casa on 2020/3/2.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation

public enum DuContextKey : String {
    // App
    case AppVersion
    case BuildVersion
    case Mode
    case AppId
    case ShumeiID
    case PinkVersion
    case Language
    case LanguageCode
    case IsLoginPagePresenting
    case TimestampGap
    case PostsTitles

    // Device
    case UUID
    case Platform
    case ModelName
    case IsRoot
    case IsProxy
    case IsSimulator
    case Power
    case Motion

    // User
    case CreateTime
    case ModifyTime
    case InnerUserId
    case IsDel
    case UserLanguage
    case UserTimeZone
    case Email
    case LoginToken
    case XAuthToken
    case UserID
    case GuestUserId
    case UserName
    case PublishTrendUserName
    case UserIcon
    case SpecialList
    case Sex
    case IdioGraph
    case Banned
    case FormatTime
    case Mobile
    case VIP
    case VIPLogo
    case Code
    case Group
    case Amount
    case AmountSign
    case IsIdentifyExpert
    case IsQuestionExpert
    case IsSetLoginPassword
    case IsSetWithdrawPassword
    case RecommendReason
    case Account
    case AccountName
    case AccountType
    case IsOlder
    case IsAdmin
    case IsBindMobile
    case IsMerchant
    case IsReadProtocol
    case JoinDays
    case IsCertify
    case IsCommunityAgreements
    case Balance
    case CustomerURL
    case IsComplete
    case CountryCode
    case IsRegister
    case OpenImUserId
    case OpenImPassword
    case Status
    case Queue
    case Token
    
    // Networking
    case CookieToken
    case ApiEnv
    case IpAddress
    case NetworkStatusDescriptionString
    case IsReachable
    case JavaBaseURL
    case PHPBaseURL
    
    // Location
    case Latitude
    case Longitude
    case Country_State_City
    case City
    
    // Haiwai Constants
    
    /// 卖家 FAQ
    case FaqUrl
    case ShareChannels
    case IdentifyTemplateUrl
    case IsRegisterCode
    case Terms
    case PrivacyPolicy
    case Sale
    case Deliver
    case IdentifyRules
    /// 发货须知
    case SellerDeliveryInstructionText
    /// 自配送须知
    case AinDeliveryMsg
    /// 买家 FAQ
    case buyerFAQ
    case UserIdString
    
    /// 同于第三方登录时，是否以三方账号，设置过密码
    case hasPassword
    /// paypal 修改地址
    case PersonalPaypal
    
    case RegisterGuideImageString
    /// Paypal id.
    case UserInfoId
    /// 卖家收款账号，后端字段为 `account`.
    case PaypalAccount
    /// Identify total count.
    case IdentifyNum
    /// sold total count.
    case SoldNum
    /// undeliver total count.
    case UnDeliverNum
    case LastLoginEmail
    case LastSignInMobile
    case IsFirstLogin
    case IdentifyGuideViewShown
    case GuideImage
    case AreaCode
    
    case WebViewUserAgent
    case Device
    /// 用来记录系统通知弹框授权时被拒的时间戳, 然后用此时间戳计算三天后再次弹出通知框提示
    case TimestampForNotificationNotAuthorized
    case isFirstLaunch
    
    /// 买家数据
    
    /// 买家未付款数量
    case buyerUnPaymentCount
    
    /// 230 买卖家FAQ
    /// 商品详情
    case CommodityDetailFAQPath
    
    /// 230 买卖Faq
    case SellAndBuyFAQPath
    
    case biillFAQPath
    /// 231
    case SiteInfos
    
    ///账单筛选配置
    case BillFilterSetting
    
    case SiteInfosSetting
    
    case clientBaseConfig
}
