//
//  Target_DuShuMeiSDK.m
//  DuShuMeiSDK
//
//  Created by casa on 2020/3/9.
//  Copyright © 2020 casa. All rights reserved.
//

#import "Target_DuShuMeiSDK.h"
#import "SmAntiFraud.h"

@implementation Target_DuShuMeiSDK

- (NSString *)Action_test:(NSDictionary *)params
{
    return [SmAntiFraud getSDKVersion];
}

- (void)Action_prepare:(NSDictionary *)params
{
    SmOption *option = [[SmOption alloc] init];
    option.organization = @"rUssDjmVPwiqx8QpjXUk";
    option.channel = @"App Store";
    option.url = @"https://fengkong-proxy.dewu.com/v3/profile/ios";
    option.contactUrl = @"https://fengkong-proxy.dewu.com/v3/profile/ios";
    option.confUrl = @"https://fengkong-proxy.dewu.com/v3/cloudconf";
    option.publicKey = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYVG5aAzjOMxkp8BCTQNwJpqFHx2z7ab/ziA0S0Vj8wKQdfwUDsfcLJwlv96PW6V1qtgwJ7c0W9jWIRYsabRd6xpEMXgFzRQzZrk5rFIQmtj7bgjzBk+KRdp8CxCmxYbo69E8OF6Rd2N0cZL9gFlirs4Dk4Wa+ohOU0mNqb8hUPwIDAQAB";
    [[SmAntiFraud shareInstance] create:option];
}

- (NSString *)Action_fetchId:(NSDictionary *)params
{
    return [[SmAntiFraud shareInstance] getDeviceId];
}

@end
