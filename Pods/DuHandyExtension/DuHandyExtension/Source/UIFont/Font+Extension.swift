//
//  Font+Extension.swift
//  DuHandyExtension
//
//  Created by casa on 2020/7/22.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit

/// 字体粗细设置
///
/// - thin: 极细
/// - light: 细
/// - regular: 常规
/// - medium: 中粗
/// - semibold: 粗
/// - condensedBold: 特粗
/// - boldItalic: 粗斜
public enum DUFontKind: String {
    case none = ""
    case italic = "Italic"
    case thin = "Thin"
    case bold = "Bold"
    case boldItalic = "BoldItalic"
    case thinItalic = "ThinItalic"
    case light = "Light"
    case lightItalic = "LightItalic"
    case regular = "Regular"
    case medium = "Medium"
    case mediumItalic = "MediumItalic"
    case semibold = "Semibold"
    case condensedBold = "CondensedBold"
    case condensedBlack = "CondensedBlack"
    case ultralight = "Ultralight"
    case ultraLightItalic = "UltraLightItalic"
}

public enum DUFontFamily: String {
    case pingFangSC = "PingFangSC"
    case pingFangHK = "PingFangHK"
    case helveticaNeue = "HelveticaNeue"
    case dinCondensed = "DINCondensed"
}

public extension UIFont {

    /// 系统支持的所有字体
    static let allFontNames: Set<String> = Set(UIFont.familyNames.flatMap({ UIFont.fontNames(forFamilyName: $0)}))

    class func pingFangSC(kind: DUFontKind = .regular, fontSize: CGFloat, scaleFont: Bool = false) -> UIFont {
        return UIFont.font(family: .pingFangSC, kind: kind, fontSize: fontSize, scaleFont: scaleFont)
    }

    class func helveticaNeue(kind: DUFontKind = .none, fontSize: CGFloat, scaleFont: Bool = false) -> UIFont {
        return UIFont.font(family: .helveticaNeue, kind: kind, fontSize: fontSize, scaleFont: scaleFont)
    }

    class func dinCondensed(kind: DUFontKind = .none, fontSize: CGFloat, scaleFont: Bool = false) -> UIFont {
        return UIFont.font(family: .dinCondensed, kind: kind, fontSize: fontSize, scaleFont: scaleFont)
    }

    class func font(family: DUFontFamily, kind: DUFontKind, fontSize: CGFloat, scaleFont: Bool = false) -> UIFont {
        let fontNameExt = kind == .none ? family.rawValue : "\(family.rawValue)-\(kind.rawValue)"
        #if DEBUG
        if !UIFont.allFontNames.contains(fontNameExt) {
            fatalError("!!! 系统不存在此字体: \(fontNameExt)")
        }
        #endif
        let newSize = scaleFont ? (fontSize * UIScreen.fontScale) : fontSize
        return UIFont(name: fontNameExt, size: newSize) ?? UIFont.systemFont(ofSize: newSize)
    }
}

public extension UIFont {
    @objc static dynamic let DUFont: String =  "PingFangSC-Light"//@"HelveticaNeue-Thin" //细
    @objc static dynamic let DUFontT: String = "PingFangSC-Medium"//粗
    @objc static dynamic let DUFontR: String = "PingFangSC-Regular"//中等粗细
    @objc static dynamic let DUFontTT: String = "PingFangSC-Semibold"//粗上加粗
    @objc static dynamic let DUFontTh: String = "PingFangSC-Thin"//极细

    @objc static dynamic let DUFontOldM: String =  "HelveticaNeue-Medium"
    @objc static dynamic let DUFontOldL: String =  "HelveticaNeue-Light"
    @objc static dynamic let DUFontOldB: String = "HelveticaNeue-CondensedBold"
    @objc static dynamic let DUFontOldBB: String = "HelveticaNeue-Bold"   //分数使用的字体
    @objc static dynamic let DUFontOldBItalic: String = "HelveticaNeue-BoldItalic"   //分数使用的字体
    @objc static dynamic let DUFontOldR: String = "HelveticaNeue" //分数使用的字体
    @objc static dynamic let DUFontOldT: String = "HelveticaNeue-Thin"

    @objc class dynamic func DUFontRegular(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontR, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontMedium(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontT, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontSemibold(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontTT, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontLight(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFont, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontThin(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontTh, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldB(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldB, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldR(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldR, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldBB(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldBB, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldBI(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldBItalic, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldM(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldM, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldT(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldT, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontOldL(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: DUFontOldL, size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontLucida(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "LucidaGrande", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontLucidaB(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "LucidaGrande-Bold", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontHelvetica(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "HelveticaNeue", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    @objc class dynamic func DUFontItalic(_ fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "HelveticaNeue-MediumItalic", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

}

/// FamilyNames
//- 0 : "Copperplate"
//- 1 : "Heiti SC"
//- 2 : "Apple SD Gothic Neo"
//- 3 : "Thonburi"
//- 4 : "Gill Sans"
//- 5 : "Marker Felt"
//- 6 : "Hiragino Maru Gothic ProN"
//- 7 : "Courier New"
//- 8 : "Kohinoor Telugu"
//- 9 : "Heiti TC"
//- 10 : "Avenir Next Condensed"
//- 11 : "Tamil Sangam MN"
//- 12 : "Helvetica Neue"
//- 13 : "Gurmukhi MN"
//- 14 : "Georgia"
//- 15 : "Times New Roman"
//- 16 : "Sinhala Sangam MN"
//- 17 : "Arial Rounded MT Bold"
//- 18 : "Kailasa"
//- 19 : "Kohinoor Devanagari"
//- 20 : "Kohinoor Bangla"
//- 21 : "Chalkboard SE"
//- 22 : "Apple Color Emoji"
//- 23 : "PingFang TC"
//- 24 : "Gujarati Sangam MN"
//- 25 : "Geeza Pro"
//- 26 : "Damascus"
//- 27 : "Noteworthy"
//- 28 : "Avenir"
//- 29 : "Mishafi"
//- 30 : "Academy Engraved LET"
//- 31 : "Futura"
//- 32 : "Party LET"
//- 33 : "Kannada Sangam MN"
//- 34 : "Arial Hebrew"
//- 35 : "Farah"
//- 36 : "Arial"
//- 37 : "Chalkduster"
//- 38 : "Kefa"
//- 39 : "Hoefler Text"
//- 40 : "Optima"
//- 41 : "Palatino"
//- 42 : "Malayalam Sangam MN"
//- 43 : "Al Nile"
//- 44 : "Lao Sangam MN"
//- 45 : "Bradley Hand"
//- 46 : "Hiragino Mincho ProN"
//- 47 : "PingFang HK"
//- 48 : "Helvetica"
//- 49 : "Courier"
//- 50 : "Cochin"
//- 51 : "Trebuchet MS"
//- 52 : "Devanagari Sangam MN"
//- 53 : "Oriya Sangam MN"
//- 54 : "Rockwell"
//- 55 : "Snell Roundhand"
//- 56 : "Zapf Dingbats"
//- 57 : "Bodoni 72"
//- 58 : "Verdana"
//- 59 : "American Typewriter"
//- 60 : "Avenir Next"
//- 61 : "Baskerville"
//- 62 : "Khmer Sangam MN"
//- 63 : "Didot"
//- 64 : "Savoye LET"
//- 65 : "Bodoni Ornaments"
//- 66 : "Symbol"
//- 67 : "Charter"
//- 68 : "Menlo"
//- 69 : "Noto Nastaliq Urdu"
//- 70 : "Bodoni 72 Smallcaps"
//- 71 : "DIN Alternate"
//- 72 : "Papyrus"
//- 73 : "Hiragino Sans"
//- 74 : "PingFang SC"
//- 75 : "Myanmar Sangam MN"
//- 76 : "Noto Sans Chakma"
//- 77 : "Zapfino"
//- 78 : "Telugu Sangam MN"
//- 79 : "Bodoni 72 Oldstyle"
//- 80 : "Euphemia UCAS"
//- 81 : "Bangla Sangam MN"
//- 82 : "DIN Condensed"

public extension UIFont {

    @objc static dynamic func registerFont(url: URL) {
        if let fontData = try? Data(contentsOf: url),
            let dataProvider = CGDataProvider(data: fontData as CFData) {
            let fontRef = CGFont(dataProvider)
            var errorRef: Unmanaged<CFError>?

            if let fontRef = fontRef, (CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) == false) {
                NSLog("Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
            }
        } else {
            NSLog("Failed to register font - font url invalid.")
        }
    }

    @objc class dynamic func iconfont(fontSize: CGFloat, scaleFont: Bool = false) -> UIFont {
        let scale = UIScreen.main.bounds.size.width / 375
        let fontScale = CGFloat.maximum(scale, 1)
        let newSize = scaleFont ? (fontSize * fontScale) : fontSize
        return UIFont(name: "iconfont", size: newSize) ?? UIFont.systemFont(ofSize: newSize)
    }
}

public enum DUMultiLanguageType: String {
    case traditionalChinese //繁体中文
    case simplifiedChinese  //简体中文
    case english            //英文
    case korean             //韩语
    case japanese           //日语

    func isLetterLanguage() -> Bool {
        switch self {
        case .english:
            return true
        default:
            return false
        }
    }
}

public class DUFontLanguageManager: NSObject {
    @objc public static dynamic let shared = DUFontLanguageManager()
    public var language: DUMultiLanguageType = .traditionalChinese
    private override init() {
        super.init()
    }
}

public enum DUMultiLanguagePingFangHKKind: String {
    case bold = "Bold"
    case regular = "Regular"
    case medium = "Medium"
    case light = "Light"

    var fontKind: DUFontKind {
        switch self {
        case .bold:
            return DUFontKind.bold
        case .regular:
            return DUFontKind.regular
        case .medium:
            return DUFontKind.medium
        case .light:
            return DUFontKind.light
        }
    }
}

public enum DUMultiLanguageTFontSize {
    /// 字母文字 40pt/sp  方块文字 34pt/sp
    case t1
    /// 字母文字 30pt/sp  方块文字 26pt/sp
    case t2
    /// 字母文字 26pt/sp  方块文字 24pt/sp
    case t3
    /// 字母文字 24pt/sp  方块文字 22pt/sp
    case t4
    /// 字母文字 22pt/sp  方块文字 20pt/sp
    case t5
    /// 字母文字 20pt/sp  方块文字 18pt/sp
    case t6
    /// 字母文字 18pt/sp  方块文字 16pt/sp
    case t7
    /// 字母文字 16pt/sp  方块文字 14pt/sp
    case t9
    /// 字母文字 14pt/sp  方块文字 12pt/sp
    case t11
    /// 字母文字 12pt/sp  方块文字 10pt/sp
    case t13
    /// 字母文字 9pt/sp  方块文字 9pt/sp
    case t16
    /// 字母文字 8pt/sp  方块文字 8pt/sp
    case t17

    var fontSize: CGFloat {
        switch self {
        case .t1:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 40 : 34
        case .t2:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 30 : 26
        case .t3:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 26 : 24
        case .t4:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 24 : 22
        case .t5:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 22 : 20
        case .t6:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 20 : 18
        case .t7:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 18 : 16
        case .t9:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 16 : 14
        case .t11:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 14 : 12
        case .t13:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 12 : 10
        case .t16:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 9 : 9
        case .t17:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 8 : 8
        }
    }
}

public enum DUMultiLanguageDinCondensedKind: String {
    case medium = "Medium"

    var fontKind: DUFontKind {
        switch self {
        case .medium:
            return DUFontKind.medium
        }
    }
}

public enum DUMultiLanguageDFontSize {
    /// 字母文字 30pt/sp  方块文字 24pt/sp
    case d1
    /// 字母文字 20pt/sp  方块文字 16pt/sp
    case d2
    /// 字母文字 18pt/sp  方块文字 14pt/sp
    case d3
    /// 字母文字 22pt/sp  方块文字 18pt/sp
    case d4

    var fontSize: CGFloat {
        switch self {
        case .d1:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 30 : 24
        case .d2:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 20 : 16
        case .d3:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 18 : 14
        case .d4:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 22 : 18
        }
    }
}

public enum DUMultiLanguageHelveticaNeueKind: String {
    case medium = "Medium"

    var fontKind: DUFontKind {
        switch self {
        case .medium:
            return DUFontKind.medium
        }
    }
}

public enum DUMultiLanguageHFontSize {
    /// 字母文字 55pt/sp  方块文字 44pt/sp
    case h1
    /// 字母文字 18pt/sp  方块文字 16pt/sp
    case h3
    /// 字母文字 14pt/sp  方块文字 12pt/sp
    case h4
    /// 字母文字 12pt/sp  方块文字 10pt/sp
    case h5
    /// 字母文字 24pt/sp  方块文字 22pt/sp
    case h6
    /// 字母文字 9pt/sp  方块文字 9pt/sp
    case h7
    /// 字母文字 8pt/sp  方块文字 8pt/sp
    case h8
    var fontSize: CGFloat {
        switch self {
        case .h1:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 55 : 44
        case .h3:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 18 : 16
        case .h4:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 14 : 12
        case .h5:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 12 : 10
        case .h6:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 24 : 22
        case .h7:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 9 : 9
        case .h8:
            return DUFontLanguageManager.shared.language.isLetterLanguage() ? 8 : 8
        }
    }
}

public extension UIFont {
    class func pingFangHKMultiLanguage(kind: DUMultiLanguagePingFangHKKind = .medium, size: DUMultiLanguageTFontSize, scaleFont: Bool = false) -> UIFont {
        if DUFontLanguageManager.shared.language.isLetterLanguage() {
            let newSize = scaleFont ? (size.fontSize * UIScreen.fontScale) : size.fontSize
            switch kind {
            case .bold:
                return UIFont.systemFont(ofSize: newSize, weight: .bold)
            case .medium:
                return UIFont.systemFont(ofSize: newSize, weight: .semibold)
            case .regular:
                return UIFont.systemFont(ofSize: newSize, weight: .regular)
            case .light:
                return UIFont.systemFont(ofSize: newSize, weight: .light)
            }
        } else {
            return UIFont.font(family: .pingFangHK, kind: kind.fontKind, fontSize: size.fontSize, scaleFont: scaleFont)
        }
    }

    class func dinCondensedMultiLanguage(kind: DUMultiLanguageDinCondensedKind = .medium, size: DUMultiLanguageDFontSize, scaleFont: Bool = false) -> UIFont {
        if DUFontLanguageManager.shared.language.isLetterLanguage() {
            let newSize = scaleFont ? (size.fontSize * UIScreen.fontScale) : size.fontSize
            switch kind {
            case .medium:
                return UIFont.font(family: .dinCondensed, kind: .bold, fontSize: newSize, scaleFont: scaleFont)
            }
        } else {
            return UIFont.font(family: .pingFangHK, kind: kind.fontKind, fontSize: size.fontSize, scaleFont: scaleFont)
        }
    }

    class func helveticaNeueMultiLanguage(kind: DUMultiLanguageHelveticaNeueKind = .medium, size: DUMultiLanguageHFontSize, scaleFont: Bool = false) -> UIFont {
        if DUFontLanguageManager.shared.language.isLetterLanguage() {
            let newSize = scaleFont ? (size.fontSize * UIScreen.fontScale) : size.fontSize
            switch kind {
            case .medium:
                return UIFont.font(family: .helveticaNeue, kind: .condensedBold, fontSize: newSize, scaleFont: scaleFont)
            }
        } else {
            return UIFont.font(family: .pingFangHK, kind: .medium, fontSize: size.fontSize, scaleFont: scaleFont)
        }
    }

}
