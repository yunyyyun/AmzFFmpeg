//
//  UIImage+fetch.swift
//  DuCommonResource
//
//  Created by gx on 2020/4/23.
//  Copyright © 2020 gx. All rights reserved.
//

import UIKit

internal class _DuResourceClass: NSObject { }

public extension UIImage {
    convenience init?(du_common_named: String) {
        guard let path = Bundle(for: _DuResourceClass.self).resourcePath?.appending("/DuCommonResources.bundle") else {
            self.init(named: du_common_named)
            return
        }
        
        let bundle = Bundle(path: path)
        self.init(named: du_common_named, in: bundle, compatibleWith: nil)
    }
}
