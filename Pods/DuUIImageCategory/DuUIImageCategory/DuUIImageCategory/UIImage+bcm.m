//
//  UIImage+bcm.m
//  openCV_Demo
//
//  Created by meng yun on 2020/5/18.
//  Copyright © 2020 meng yun. All rights reserved.
//

#import "UIImage+bcm.h"
int d0 = 3*2;
int d1 = 6*2;
@implementation UIImage (Crop)

- (NSString *)base64String {
    NSData *data = UIImagePNGRepresentation(self);
    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    return baseString;
}

+ (UIImage *)imageWithBase64String: (NSString *)base64String {
    NSData *imageData = [[NSData alloc]initWithBase64EncodedString: base64String options:(NSDataBase64DecodingIgnoreUnknownCharacters)];
    UIImage *image = [UIImage imageWithData: imageData];
    return image;
}

- (UIImage *)cropImageWith: (CGRect) box{
    CGImageRef sourceImageRef = [self CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, box);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    CGImageRelease(newImageRef);
    return newImage;
}

- (CGRect) minPortraitBox{
    CGImageRef inImage = self.CGImage;
    // Create off screen bitmap context to draw the image into. Format ARGB is 4 bytes for each pixel: Alpa, Red, Green, Blue
    CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
    if (cgctx == NULL) {
        return CGRectMake(0, 0, 10, 10); /* error */
    }

    size_t w = CGImageGetWidth(inImage);
    size_t h = CGImageGetHeight(inImage);
    d0 = (int)w/32;
    d1 = (int)h/16;
    CGRect rect = {{0,0},{w,h}};

    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(cgctx, rect, inImage);

    // Now we can get a pointer to the image data associated with the bitmap
    // context.
    unsigned char* data = CGBitmapContextGetData (cgctx);

    int minX=0;
    int maxX=(int)w;
    int minY=0;
    int maxY=(int)h;
    if (data != NULL) {
        //offset locates the pixel in the data from x,y.
        //4 for 4 bytes of data per pixel, w is width of one row of data.
//        int offset = 4*((w*round(point.y))+round(point.x));
//        int alpha =  data[offset];
//        int red = data[offset+1];
//        int green = data[offset+2];
//        int blue = data[offset+3];
//        //NSLog(@"offset: %i colors: RGB A %i %i %i  %i",offset,red,green,blue,alpha);
//        color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
        minX = [self getMinX: data width: w height: h];
        minY = [self getMinY: data width: w height: h];
        
        maxX = [self getMaxX: data width: w height: h];
        maxY = [self getMaxY: data width: w height: h];
        NSLog(@"minXY is %d-%d", minX, minY);
    }

    int xx = minX;
    int yy = minY;
    int ww = maxX-minX;
    int hh = maxY-minY;
    // When finished, release the context
    CGContextRelease(cgctx);
    // Free image data memory for the context
    if (data) { free(data); }
    return CGRectMake(xx, yy, ww, hh);
}

- (int) getMinX:(unsigned char*)data width: (CGFloat)w height: (CGFloat)h {
    for (int x=1; x<w; x+=d0){
        if ([self testXWith:data pointX:x width:w height:h]) {
            return x-d0>0 ? x-d0 : 0;
        }
    }
    return 0;
}

- (int) getMinY:(unsigned char*)data width: (CGFloat)w height: (CGFloat)h {
    for (int y=1; y<h; y+=d0){
        if ([self testYWith: data pointY:y width:w height:h]) {
            return y-d0>0 ? y-d0 : 0;
        }
    }
    return 0;
}

- (int) getMaxX:(unsigned char*)data width: (CGFloat)w height: (CGFloat)h {
    for (int x=w-1; x>0; x-=d0){
        if ([self testXWith:data pointX:x width:w height:h]) {
            return x+d0<w ? x+d0 : w;
        }
    }
    return w;
}

- (int) getMaxY:(unsigned char*)data width: (CGFloat)w height: (CGFloat)h {
    for (int y=h-1; y>0; y-=d0){
        if ([self testYWith:data pointY:y width:w height:h]) {
            return y+d0<h ? y+d0 : h;
        }
    }
    return w;
}

// 竖线检测
- (BOOL) testXWith:(unsigned char*)data pointX: (int)p_x width: (int)w height: (int)h {
    for (int i = 0; i<h; i+=d1) {
        int offset = 4*((w*round(i))+round(p_x));
        int alpha =  data[offset];
//        int red = data[offset+1];
//        int green = data[offset+2];
//        int blue = data[offset+3];
        // NSLog(@"curColor_testXWith_is(%d_%d) %d-%d-%d-%d", p_x, i, red, green, blue, alpha);
        if (alpha>10) {
            return true;
        }
    }
    return false;
}

// 横线检测
- (BOOL) testYWith:(unsigned char*)data pointY: (CGFloat)p_y width: (CGFloat)w height: (CGFloat)h {
    for (int i = 0; i<w; i+=d1) {
        int offset = 4*((w*round(p_y))+round(i));
        int alpha =  data[offset];
//        int red = data[offset+1];
//        int green = data[offset+2];
//        int blue = data[offset+3];
        // NSLog(@"curColor_testYWith_is(%d) %d-%d-%d-%d", i, red, green, blue, alpha);
        if (alpha>10) {
            return true;
        }
    }
    return false;
}
 
- (CGContextRef) createARGBBitmapContextFromImage:(CGImageRef) inImage {
        
        CGContextRef    context = NULL;
        CGColorSpaceRef colorSpace;
        void *          bitmapData;
        int             bitmapByteCount;
        int             bitmapBytesPerRow;
        
        // Get image width, height. We'll use the entire image.
        size_t pixelsWide = CGImageGetWidth(inImage);
        size_t pixelsHigh = CGImageGetHeight(inImage);
        
        // Declare the number of bytes per row. Each pixel in the bitmap in this
        // example is represented by 4 bytes; 8 bits each of red, green, blue, and
        // alpha.
        bitmapBytesPerRow   = (int)(pixelsWide * 4);
        bitmapByteCount     = (int)(bitmapBytesPerRow * pixelsHigh);
        
        // Use the generic RGB color space.
        colorSpace = CGColorSpaceCreateDeviceRGB();
 
        if (colorSpace == NULL)
        {
                fprintf(stderr, "Error allocating color space\n");
                return NULL;
        }
        
        // Allocate memory for image data. This is the destination in memory
        // where any drawing to the bitmap context will be rendered.
        bitmapData = malloc( bitmapByteCount );
        if (bitmapData == NULL)
        {
                fprintf (stderr, "Memory not allocated!");
                CGColorSpaceRelease( colorSpace );
                return NULL;
        }
        
        // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
        // per component. Regardless of what the source image format is
        // (CMYK, Grayscale, and so on) it will be converted over to the format
        // specified here by CGBitmapContextCreate.
        context = CGBitmapContextCreate (bitmapData,
                                                                         pixelsWide,
                                                                         pixelsHigh,
                                                                         8,      // bits per component
                                                                         bitmapBytesPerRow,
                                                                         colorSpace,
                                                                         kCGImageAlphaPremultipliedFirst);
        if (context == NULL)
        {
                free (bitmapData);
                fprintf (stderr, "Context not created!");
        }
        
        // Make sure and release colorspace before returning
        CGColorSpaceRelease( colorSpace );
        
        return context;
}
@end
