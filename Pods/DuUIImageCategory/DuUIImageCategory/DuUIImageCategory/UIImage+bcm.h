//
//  UIImage+bcm.h
//  openCV_Demo
//
//  Created by meng yun on 2020/5/18.
//  Copyright © 2020 meng yun. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Crop)

- (NSString *)base64String;

+ (UIImage *)imageWithBase64String: (NSString *)base64String;

- (CGRect) minPortraitBox;

- (UIImage *)cropImageWith: (CGRect)box;

@end

NS_ASSUME_NONNULL_END
