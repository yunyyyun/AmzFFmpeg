//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator
import CTNetworkingSwift

public extension CTMediator {
    func DuLoginModule_present(sourceViewController: UIViewController? = nil, success: (()->Void)? = nil, fail: (()->Void)? = nil, cancel: (()->Void)? = nil) {

        var params: [AnyHashable: Any] = [:]
        if let success = success {
            params["success"] = success
        }
        if let fail = fail {
            params["fail"] = fail
        }
        if let cancel = cancel {
            params["cancel"] = cancel
        }
        if let sourceViewController = sourceViewController {
            params["sourceViewController"] = sourceViewController
        }

        //先尝试FakeLoginModule，AppEngineModule，最后是DuLoginModule
        #if DEBUG
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuFakeLoginModule"
        if presentViewController(params) == true {
            return
        }
        #endif
        
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuAppEngineModule"
        if presentViewController(params) == true {
            return
        }
        
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuLoginModule"
        if presentViewController(params) == true {
            return
        }
    }
    
    func DuLoginModule_presentVerifyCodeLogin(success: (()->Void)? = nil, fail: (()->Void)? = nil, cancel: (()->Void)? = nil) {
        var params: [AnyHashable: Any] = [:]
        if let success = success {
            params["success"] = success
        }
        if let fail = fail {
            params["fail"] = fail
        }
        if let cancel = cancel {
            params["cancel"] = cancel
        }

        // 先尝试DUApp下的target，再尝试DuFakeLoginModule下的target，最后尝试DuLoginModule下的target
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DUApp"
        if showVerifyLoginViewController(params) == true {
            return
        }
        
        #if DEBUG
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuFakeLoginModule"
        if showVerifyLoginViewController(params) == true {
            return
        }
        #endif
        
        params[kCTMediatorParamsKeySwiftTargetModuleName] = "DuLoginModule"
        if showVerifyLoginViewController(params) == true {
            return
        }
    }
    
    private func showVerifyLoginViewController(_ params:[AnyHashable:Any]) -> Bool {
        if let result = self.performTarget("DuLoginModule", action: "showVerifyCodeLogin", params: params, shouldCacheTarget: true) as? Bool {
            return result
        }
        return false
    }
    
    private func presentViewController(_ params:[AnyHashable:Any]) -> Bool {
        if let result = self.performTarget("DuLoginModule", action: "login", params: params, shouldCacheTarget: true) as? Bool {
            return result
        }
        return false
    }
}
