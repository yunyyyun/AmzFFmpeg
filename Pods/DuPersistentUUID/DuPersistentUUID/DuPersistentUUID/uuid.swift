//
//  uuid.swift
//  DuPersistentUUID
//
//  Created by casa on 2020/3/3.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import KeychainAccess
import AdSupport
import FCUUID

extension UUID {
    public static var persistentDeviceUUID: String {
        let asManager = ASIdentifierManager.shared()
        if asManager.isAdvertisingTrackingEnabled {
            var identifier: String? = asManager.safeAdvertisingIdentifier_duplicated?.uuidString
            if identifier == "00000000-0000-0000-0000-000000000000" {
                identifier = "UUID\(FCUUID.uuidForDevice() ?? "")"
            }
            
            return identifier ?? FCUUID.uuidForDevice()
        } else {
            return FCUUID.uuidForDevice()
        }
    }
}

private extension ASIdentifierManager {
    // https://bugs.swift.org/browse/SR-6143
    var safeAdvertisingIdentifier_duplicated: UUID? {
        return perform(#selector(getter: ASIdentifierManager.advertisingIdentifier))?.takeUnretainedValue() as? UUID
    }
}
