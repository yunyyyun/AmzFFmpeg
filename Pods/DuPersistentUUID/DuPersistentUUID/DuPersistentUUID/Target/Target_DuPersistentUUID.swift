//
//  Target_DuPersistentUUIDDemo.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit

@objc class Target_DuPersistentUUID: NSObject {
    @objc func Action_deviceUUID(_ params:[AnyHashable:Any]) -> String {
        return UUID.persistentDeviceUUID
    }
}
