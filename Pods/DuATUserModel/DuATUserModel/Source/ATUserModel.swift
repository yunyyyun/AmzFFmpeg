//
//  ATUserModel.swift
//  DuATUserModel
//
//  Created by 景鹏博 on 2020/6/3.
//  Copyright © 2020 景鹏博. All rights reserved.
//

import UIKit
import HandyJSON
import YYModel

// MARK: - User
public protocol UserType {
    var userId: String { get }
    var userName: String { get }
    var icon: String { get }
    var idiograph: String { get }
    var mobile: String { get }
    var sex: Int { get }
    var token: String? { get }
    var vip: Int { get }
    var balance: Int { get }
    var specialList: String { get }
    var isAdmin: Int { get }
    var isMerchant: Int { get }
}


public protocol DUTrendAuthorType {
    var icon: String {get}
    var userName: String {get}
    var vIcon: String {get}
    var sex: Int {get}
    var userId: String {get}
    var formerName: String {get}
    var liveInfo: DUUserLiveInfoModel? {get}
}



/// ⚠️该模型中新增属性只能是可选类型，不能添加非可选类型
@objcMembers
open class ATUsersModel: NSObject, HandyJSON, Codable, UserType, DUTrendAuthorType {
    
    /// 本地使用的临时存储vIcon的属性,支持协议
    public var liveVicon = ""
    
    public var vIcon: String {
        return group.first?.logo ?? ""
    }
    
    public var formerName: String {
        return publishTrendUserName
    }
    
    required override public init() {}

    public var userId: String = ""
    public var userName: String = ""
    public var publishTrendUserName: String = ""
    public var icon: String = ""
    public var token: String? = ""
    public var specialList: String = ""
    public var sex: Int = 0
    public var idiograph: String = ""
    public var banned: Int = 0
    public var formatTime: String = ""
    public var mobile: String = ""
    public var vip: Int = 0
    public var code: String = ""
    public var group: [ATUsersGroupModel] = []
    public var amount: Int = 0
    public var amountSign: String = ""
    public var isIdentifyExpert: Int = 0
    public var isQuestionExpert: Int = 0
    public var recommendReason: String = ""
    public var account: String = ""
    public var accountName: String = ""
    public var accountType: Int = 0
    public var isOlder: Int = 0
    public var isAdmin: Int = 0
    public var isBindMobile: Int = 0
    public var isMerchant: Int = 0
    public var isReadProtocol: Int = 0
    public var joinDays: Int = 0
    public var isCertify: Int = 0
    public var isCommunityAgreements = 0 // 是否已经阅读版规
    public var balance: Int = 0
    
    public var kolLevel: Int = 0   // 用户的观众等级
    public var userLevel: Int = 0   // 用户的主播等级
    public var extraLevel: [DuChatLevelModel]? // 其它等级的数组
    // 客服跳转URL
    public var customerUrl: String = ""
    // public var isRegister: Int = 0
    
    /// 4.35.0 用户直播状态信息
    public var liveInfo: DUUserLiveInfoModel?
    
    /// 4.38 用户头像挂件
    public var avatarPendant: DUUserAvatarPendant?
    
    public var avatarFrame: String?
    
    // NOTE: 非接口自定义字段，options方式，以便热更新解析
    public var isSetLoginPassword: Bool? // 是否设置了登录密码
    public var isSetWithdrawPassword: Bool? // 是否设置了提现密码
}

public class DUUserAvatarPendant: NSObject, HandyJSON, Codable {
    required override public init() {}
    public var url: String? //相框的地址
    public var isNew: Bool? //是否上新的头像框
    public var userHasNew: Bool? //用户是否拥有新的头像框
}


public class DUUserLiveInfoModel: NSObject, HandyJSON, Codable {
    required override public init() {}
    public var liveStatus: Int?
    public var roomId: Int?
    public var isActivity: Int?
}


public class ATUsersGroupModel: NSObject, HandyJSON, Codable {
    required override public init() {}
    
    public var groupsId: Int = 0
    public var name: String = ""
    public var logo: String = ""
    public var orderBy: Int = 0
}

public class DuChatLevelModel: NSObject, HandyJSON, Codable, YYModel {
    required override public init() {}
    
    @objc public var type: Int = 0
    @objc public var level: Int = 0
    @objc public var name: String = ""
    @objc public var image: String = ""
}

public extension ATUsersModel {
    static func modelContainerPropertyGenericClass() ->[String : AnyObject]? {
        return ["extraLevel": DuChatLevelModel.self]
    }
}

public extension ATUsersModel {
    
    @objc static dynamic let cacheKey = "ATUsersModel.cacheKey"
    
    @objc static dynamic let key = "ATUsersModel.key"
    
    @objc static dynamic let totalKey = "ATUsersModel.totalKey"
    
    @objc static dynamic let assetsKey = "ATUsersModel.assetsKey"
    
    @objc static dynamic let unionListKey = "ATUsersModel.unionListKey"
    
    @objc static dynamic let trendsKey = "ATUsersModel.trendsKey"
    
    @objc static dynamic let mineKey = "ATUsersModel.mineKey"
    
    @objc static dynamic let sellerKey = "ATUsersModel.sellerKey"
    
    @objc static dynamic let isOpenedPurchaseKey = "ATUsersModel.isOpenedPurchaseKey"
    
    @objc static dynamic let isOpenedPurchasePushKey = "ATUsersModel.isOpenedPurchaseKey"
    
    @objc static dynamic let isFirstOpenProductDetailKey = "ATUsersModel.isFirstOpenProductDetailKey"
    
    @objc static dynamic let isGuideProductDetailCollectKey = "ATUsersModel.isGuideProductDetailCollectKey"
    
    @objc static dynamic let homeShowGuidePageKey = "ATUsersModel.homeShowGuidePageKey"
    
    @objc static dynamic let isPurchasedKey = "ATUsersModel.isPurchasedKey"
    
    @objc static dynamic let cutRemindOpenPushKey = "ATUsersModel.cutRemindOpenPushKey"
    
    @objc static dynamic let isRegisterKey = "ATUsersModel.isRegisterKey"
    
    @objc static dynamic let isOutfitGuideShowedKey = "ATUsersModel.isOutfitGuideKeyShowed"
    
    @objc static dynamic let isNewSelectionGuideShownKey = "ATUsersModel.isNewSelectionGuideShownKey"
}


public class ATMyTotalModel: NSObject, HandyJSON, Codable {
    required override public init() {}
    
    public var followNum: Int = 0
    public var fansNum: Int = 0
    public var trendsNum: Int = 0
    var collectNum: Int = 0
    public var favNum: Int = 0
    public var tagNum: Int = 0
    public var remindNum: Int = 0
    public var identifyNum: Int = 0
    public var forumNum: Int = 0
    public var goodsNum: Int = 0
    public var lightNum: Int = 0
    public var trendsFavNum: Int = 0
    public var postsFavNum: Int = 0
    public var newsFavNum: Int = 0
    public var wantNum: Int = 0
    public var possessNum: Int = 0
    public var labelGoodsNum: Int = 0
    public var clockInNum: Int = 0
    public var amount: Int = 0
    public var liveNum: Int = 0
    public var questionNum: Int = 0
    public var tradeNum: Int = 0
    public var buyNum: Int = 0
    public var soldNum: Int = 0
    public var couponNum: Int = 0
    public var collectProductNum: Int = 0
    public var redPacketBalance: Int = 0
    
}
