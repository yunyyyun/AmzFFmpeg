//
//  DuRemoveUserFollowAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuRemoveFollowUserAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {
        /// 关注/取消关注的用户id列表
        public var userId: String?
        public init() {}
    }
}

extension DuRemoveFollowUserAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuRemoveFollowUserAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "sns/v1/user/follow-remove"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        guard let userID = params?["userId"] as? [String] else { return params }
        guard let aParams = params else { return nil }

        var result = aParams
        result["userId"] = userID
        return result
    }
}
