//
//  DuClientInitAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-03-03~17:03:43.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift
import CTMediator
import DuContext_Extension

public class DuClientInitAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
}

extension DuClientInitAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        if let jwtToken = apiManager.response?.response?.allHeaderFields["x-auth-token"] as? String {
            CTMediator.sharedInstance().DuContext_set(key: .XAuthToken, value: jwtToken)
        }
        if let data = apiManager.fetchAsDictionary() {
            if let data = data["data"] as? [AnyHashable: Any] {
                if let serverTimestamp = data["serverTimestamp"] as? Int64, serverTimestamp >= 0 {
                    CTMediator.sharedInstance().DuContext_Device_SetServerTimeStamp(serverTimeStamp: serverTimestamp)
                }
                CT().DuContext_Networking_SetJavaBaseURL(data["javaHost"])
                CT().DuContext_Networking_SetPHPBaseURL(data["serverUrl"])
                CT().DuContext_App_SetPostsTitles(titles: data["postsTitle"])
            }
        }
        return true
    }
}

extension DuClientInitAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuClientInitAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "client/init"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
}
