//
//  DuNewbiddingSellerConfirmAPIManager.swift
//  DuAPIManagers
//  出价确认
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuNewbiddingSellerConfirmAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var sellerBiddingTypeId: Int? // 出价类型
        public var price: Int? //价格
        public var skuId: Int?
        public var stockNo: String? // 库存编号(寄售必传)
        public var billNoList: [String]? ///发货编号(批量必传)
        public var billNo: String? // 发货编号(寄售必传)
        public var merchantType: Int? // 商户类型
        public var sellerBiddingNo: String? // 出价No（重新出价需要）
        public var buyerBiddingNo: String? // 求购No（立即变现需要）
        public init() {}
    }
}

extension DuNewbiddingSellerConfirmAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuNewbiddingSellerConfirmAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/newbidding/seller/confirm"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
