//
//  DuBoutiqueFilterAPIManager.swift
//  DuAPIManagers
//
//  Created by jiguanjie on 2020/10/10.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import Alamofire
import CTNetworkingSwift

public class DuBoutiqueFilterAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var property: String?
        public var categoryId: String?
        public var brandId: String?
        public var lowestPrice: String?
        public var highestPrice: String?
        public var fitId: String?
        public var seriesId: String?
        public var title: String?
        public init() {}
    }
}

extension DuBoutiqueFilterAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuBoutiqueFilterAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/commodity/ice/boutique-recommend/screen"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
