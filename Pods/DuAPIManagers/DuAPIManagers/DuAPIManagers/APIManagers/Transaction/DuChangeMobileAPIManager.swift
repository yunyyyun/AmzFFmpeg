//
//  DuChangeMobileAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
// 修改手机号

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuChangeMobileAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    /*
     countryCode    string
     非必须
     国家码
     mobile    string
     非必须
     手机号
     code    string
     非必须
     验证码
     */
    public struct Param: CTNetworkingParamConvertible {
        public var countryCode: String?
        public var mobile: String?
        public var code: String?
        public init() {}
    }
}

extension DuChangeMobileAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuChangeMobileAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/user_info/change/changeMobile"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
