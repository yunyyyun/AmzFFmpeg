//
//  DuBoutiqueRecommendAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuBoutiqueRecommendAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        /// 推荐ID
        public var recommendId: Int?
        /// 推荐主图中的spuId
        public var spuIds: [Int] = []
    
        public var propertyValueId: Int = 0
        /// 半自动主题筛选项
        public var aggregation: [String: Any] = [
            "aggregation": true,
            "filters": [:],
            "sortMode": 1,
            "sortType": 0
        ]
        
        public init() {}
    }
    
    @objc dynamic public var isLastPage:Bool = false
    
    @objc dynamic public var isFirstPage:Bool = true
    
    @objc dynamic public var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
        
    @objc public dynamic var pageSize:Int = 20
    /// 兜底过滤出价spuId
    dynamic var lastSpuId: Int?
    /// 真实页数，用于兜底过滤出价场景
    dynamic var realPageNum: Int?
    
    @objc dynamic var lastId: String?
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        lastId = nil
        lastSpuId = nil
        realPageNum = nil
        super.loadData()
    }
}

extension DuBoutiqueRecommendAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        isFirstPage = false
        super.loadData()
    }
}

extension DuBoutiqueRecommendAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuBoutiqueRecommendAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/commodity/ice/boutique-recommend/detail"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        
        resultParams["lastId"] = lastId ?? ""
        resultParams["lastSpuId"] = lastSpuId ?? 0
        resultParams["realPageNum"] = realPageNum ?? 0

        return resultParams
    }
}

extension DuBoutiqueRecommendAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {

         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        let result = apiManager.fetchAsDictionary()
        guard let data = result?["data"] as? [AnyHashable:Any] else { return false }

        self.lastId = data["lastId"] as? String
        self.realPageNum = data["realPageNum"] as? Int
        self.lastSpuId = data["lastSpuId"] as? Int
        self.isLastPage = self.lastId == "" || self.lastId == nil
        return true
    }
}
