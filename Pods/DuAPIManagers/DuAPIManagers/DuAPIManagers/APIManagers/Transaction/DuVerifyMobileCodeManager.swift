//
//  DuVerifyMobileCodeManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuVerifyMobileCodeManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    // 验证码类型 0.注册(暂无) 1.登录(暂无) 2.找回密码(暂无) 3.绑定手机号(暂无) 4.商户认证(暂无) 5.绑定支付宝 6.现金账户提现 7.M站 8.换绑手机号-旧手机
    public struct Param: CTNetworkingParamConvertible {
        // public var userName:String?
        // public var password:String?
        public var type: Int?
        public var countryCode: Int?
        public var code: Int?
        public var mobile: String?
        public init() {}
    }
}

extension DuVerifyMobileCodeManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuVerifyMobileCodeManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "users/verifyMobileCode"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
