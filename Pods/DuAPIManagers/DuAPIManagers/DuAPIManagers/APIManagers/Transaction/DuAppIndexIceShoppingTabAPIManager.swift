//
//  DuAppIndexIceShoppingTabAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuAppIndexIceShoppingTabAPIManager: CTNetworkingAPIManager {
    
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param: CTNetworkingParamConvertible {
        public var limit: Int = 20
        public var tabId: Int = 0
        public var abTest: [[String: Any]] = []
        public init() {}
    }

    @objc public dynamic var isLastPage: Bool = false

    @objc public dynamic var isFirstPage: Bool = true

    @objc public dynamic var currentPageNumber: Int = 0
    public var totalCount: Int?

    @objc public dynamic var pageSize: Int = 20

    @objc public dynamic var lastId: String?

    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DuAppIndexIceShoppingTabAPIManager: CTNetworkingAPIManagerPagable {

    @objc public dynamic func loadNextPage() {
        self.isFirstPage = false
        super.loadData()
    }
}

extension DuAppIndexIceShoppingTabAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuAppIndexIceShoppingTabAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/index/ice/shopping-tab"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["limit"] = pageSize
        resultParams["lastId"] = lastId
        return resultParams
    }
}

extension DuAppIndexIceShoppingTabAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {

         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        let result = apiManager.fetchAsDictionary()
        guard let data = result?["data"] as? [AnyHashable:Any] else { return false }

        self.lastId = data["lastId"] as? String
        self.isLastPage = self.lastId == "" || self.lastId == nil
        return true
    }
}
