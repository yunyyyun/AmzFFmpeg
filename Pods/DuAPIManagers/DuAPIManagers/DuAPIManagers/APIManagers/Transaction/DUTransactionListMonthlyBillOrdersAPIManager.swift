//
//  DUTransactionListMonthlyBillOrdersAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DUTransactionListMonthlyBillOrdersAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var sellerUserId:String?
        public var lastId:Int?
        public var year:String?
        public var month:String?
        public var stmtChannel:Int?
        public var stmtStatus:Int?

        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 20
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DUTransactionListMonthlyBillOrdersAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DUTransactionListMonthlyBillOrdersAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DUTransactionListMonthlyBillOrdersAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/finance-data-gateway-biz/finance/dataGateway/listBillOrders"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
         resultParams["pageSize"] = pageSize
        // resultParams["pageNum"] = currentPageNumber+1
        return resultParams
    }
}

extension DUTransactionListMonthlyBillOrdersAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {

         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
//        #warning("需要解析翻页参数")
//        // let result = apiManager.fetchAsDictionary()
//        // guard let data = result?["data"] as? [AnyHashable:Any] else { return false }
//        // guard let total = data["total"] as? Int else { return false }
//        // guard let totalPage = data["pages"] as? Int else { return false }
//
//        // totalCount = total
//        // currentPageNumber += 1
//        // isFirstPage = currentPageNumber == 1
//        // isLastPage = currentPageNumber == totalPage

        return true
    }
}
