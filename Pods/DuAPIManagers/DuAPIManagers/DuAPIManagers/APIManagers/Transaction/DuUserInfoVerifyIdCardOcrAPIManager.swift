//
//  DuUserInfoVerifyIdCardOcrAPIManager.swift
//  DuAPIManagers
//  身份证OCR
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuUserInfoVerifyIdCardOcrAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        /// 身份证正面(base64)
        public var frontImg: String?
        /// 身份证反面(base64)
        public var backImg: String?
        /// OCR类型：1、 卖家入驻 2、得物分期授信
        public var ocrType: String?
        public init() {}
    }
}

extension DuUserInfoVerifyIdCardOcrAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuUserInfoVerifyIdCardOcrAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/user_info/verify/idCardOcr"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
