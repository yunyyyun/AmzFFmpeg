//
//  DuAddressJsUpdateAPIManager.swift
//  DuAPIManagers
//
//  Created by 小华's script on 2020-02-28~17:02:10.
//  Copyright © 2020 小华. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuAddressJsUpdateAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var billNo: String?
        public var address: String?
        public var mobile: String?
        public var name: String?
        public var province: String?
        public var city: String?
        public var district: String?
        public var addressId: Int?

        public init() {}
    }
}

extension DuAddressJsUpdateAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuAddressJsUpdateAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/consign/js/updateAddressInfo"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
