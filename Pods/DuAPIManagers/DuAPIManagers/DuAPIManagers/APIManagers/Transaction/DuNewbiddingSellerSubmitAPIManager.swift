//
//  DuNewbiddingSellerSubmitAPIManager.swift
//  DuAPIManagers
//  提交出价
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuNewbiddingSellerSubmitAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var sellerBiddingTypeId: Int? // 出价类型
        public var skuId: Int?
        public var price: Int?
        public var quantity: Int?
        public var merchantType: Int?
        public var sellerBiddingNo: String?
        public var oldQuantity: Int?
        public var buyerBiddingNo: String?
        public var stockNo: String?
        public var billNoList: [String]?
        public var billNo: String?
        public var isAgreeGuaranteeOption: Bool?
        public var requestId: String?
        public var isCheckPrice: Bool? // 是否移除最高，最低价格校验，
        public init() {}
    }
}

extension DuNewbiddingSellerSubmitAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuNewbiddingSellerSubmitAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/newbidding/seller/submit"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
