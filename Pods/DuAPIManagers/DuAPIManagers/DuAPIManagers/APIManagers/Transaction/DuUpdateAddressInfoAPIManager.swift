//
//  DuUpdateAddressInfoAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuUpdateAddressInfoAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param : CTNetworkingParamConvertible {
        public var billNo: String?
        public var address: String?
        public var mobile: String?
        public var name: String?
        public var province: String?
        public var city: String?
        public var district: String?
        public var addressId: Int?
        public init() {}
    }
}

extension DuUpdateAddressInfoAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuUpdateAddressInfoAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/deposit-interfaces/js/bill/updateAddressInfo"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
