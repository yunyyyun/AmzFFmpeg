//
//  DuBountyApplySmsResendAPIManager.swift
//  DuAPIManagers
//  老客短信验证码重发
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuBountyApplySmsResendAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public init() {}
    }
}

extension DuBountyApplySmsResendAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuBountyApplySmsResendAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/bounty/apply/sms/resend"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
