//
//  DuAddressEditApiManager.swift
//  DuAPIManagers
//
//  Created by 小华's script on 2020-02-28~17:02:10.
//  Copyright © 2020 小华. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuAddressEditApiManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var isChangeMobile: Int?
        public var typeId: Int?
        public var userAddressId: Int?
        public var address: String = ""
        public var district: String = ""
        public var city: String = ""
        public var province: String = ""
        public var mobile: String = ""
        public var name: String = ""

        public init() {}
    }
}

extension DuAddressEditApiManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuAddressEditApiManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/user/ice/user/editAddress"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
