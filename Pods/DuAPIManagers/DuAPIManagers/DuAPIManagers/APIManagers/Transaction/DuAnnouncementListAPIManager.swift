//
//  DuAnnouncementListAPIManager.swift
//  DuAPIManagers
//
//  Created by 小华 script on 2020-02-28~17:02:10.
//  Copyright © 2020 小华. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuAnnouncementListAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var contentType: Int?
        public var limit: Int = 20
        public init() {}
    }

    @objc public dynamic var isLastPage: Bool = false

    @objc public dynamic var isFirstPage: Bool = true

    @objc public dynamic var currentPageNumber: Int = 0
    public var totalCount: Int?

    @objc public dynamic var pageSize: Int = 20

    @objc public dynamic var lastId: String = ""

    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        lastId = ""
        super.loadData()
    }
}

extension DuAnnouncementListAPIManager: CTNetworkingAPIManagerPagable {

    @objc public dynamic func loadNextPage() {
        isFirstPage = false
        super.loadData()
    }
}

extension DuAnnouncementListAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuAnnouncementListAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/merchant-side/ice/announcement/list"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["lastId"] = lastId

        return resultParams
    }
}

extension DuAnnouncementListAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        if isLoading {
            return false
        }

        if isLastPage {
            return false
        }

        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        let result = apiManager.fetchAsDictionary()
        guard let data = result?["data"] as? [AnyHashable: Any] else { return false }
        if let id = data["lastId"] as? Int {
            self.lastId = "\(id)"
        }
        if self.lastId == "" {
            isLastPage = true
        }
        return true
    }
}
