//
//  DcDressUpRecommendAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DcDressUpRecommendAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
   // productId       产品id
   // trendId         动态id
   // tagId           全部；最新；买家开箱；全身穿搭；达人同款；视频
   // contentForm     1=图文 2=视频
   // lastId          页码
   // limit           每次请求数量
   // replyNum        评论数量
   // hotReplyNum     热评数量
   // replyChildNum   子评数量
    
    public struct Param : CTNetworkingParamConvertible {
        
        public var productId: String?
        public var trendId: String?
        public var tagId: String?
        public var contentForm: String?
        public var lastId: String?
        /// 前个页面的动态所在page
        public var pageNum: String?
        public var limit: String?
        public var replyNum: String?
        public var hotReplyNum: String?
        public var replyChildNum: String?
        
        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 6
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DcDressUpRecommendAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DcDressUpRecommendAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DcDressUpRecommendAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "sns-rec/v1/dress-up/detail/feed"
    }

    public var requestType: HTTPMethod {
        return .get
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["limit"] = pageSize
        resultParams["page"] = currentPageNumber+1
        return resultParams
    }
}

extension DcDressUpRecommendAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        
        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        currentPageNumber += 1
        isFirstPage = currentPageNumber == 1
        return true
    }
}
