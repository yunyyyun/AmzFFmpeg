//
//  DuMemeAddAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuMemeAddAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public var successBlock: (() -> ())?
    
    public struct Param : CTNetworkingParamConvertible {
        public var imageUrl:String?
        public var mediaType:String?    // gif/img
        public init() {}
    }
}

extension DuMemeAddAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuMemeAddAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }
    
    @objc public dynamic var methodName: String {
        return "sns-itr/v1/custom-emojis/add"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
