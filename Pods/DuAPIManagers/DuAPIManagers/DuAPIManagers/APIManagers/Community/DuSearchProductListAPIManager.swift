//
//  DuSearchProductListAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuSearchProductListAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
         public var ignore_ids: String = ""
         public var title: String = ""
        public init() {}
    }

    @objc public dynamic var isLastPage: Bool = false

    @objc public dynamic var isFirstPage: Bool = true

    @objc public dynamic var currentPageNumber: Int = 0
    public var totalCount: Int?

    @objc public dynamic var pageSize: Int = 20

    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DuSearchProductListAPIManager: CTNetworkingAPIManagerPagable {

    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DuSearchProductListAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuSearchProductListAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "sns/v1/search/product-list"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
         resultParams["limit"] = pageSize
         resultParams["page"] = currentPageNumber
        return resultParams
    }
}

extension DuSearchProductListAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        let result = apiManager.fetchAsDictionary()
        guard let data = result?["data"] as? [AnyHashable: Any] else { return false }
        if let id = data["page"] as? Int {
            self.currentPageNumber = id
        }
        if self.currentPageNumber == 0 {
            isLastPage = true
        }
        if let total = data["total"] as? Int {
            self.totalCount = total
        }
        return true
    }
}
