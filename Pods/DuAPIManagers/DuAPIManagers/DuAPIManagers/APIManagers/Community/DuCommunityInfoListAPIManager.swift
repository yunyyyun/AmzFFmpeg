//
//  DuCommunityInfoListAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuCommunityInfoListAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var limit: String?
        public var categoryId: String?
        public var lastId: Int = 0
        public init() {}
    }

    @objc public dynamic var isLastPage: Bool = false

    @objc public dynamic var isFirstPage: Bool = true

    @objc public dynamic var currentPageNumber: Int = 0
    public var totalCount: Int?

    @objc public dynamic var pageSize: Int = 20

    @objc public dynamic var lastId: Int = 0

    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        lastId = 0
        super.loadData()
    }
}

extension DuCommunityInfoListAPIManager: CTNetworkingAPIManagerPagable {

    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DuCommunityInfoListAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuCommunityInfoListAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "sns-feed/v1/feed/information-flow"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["limit"] = 20
        resultParams["lastId"] = self.lastId
        return resultParams
    }
}

extension DuCommunityInfoListAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        let result = apiManager.fetchAsDictionary()
        guard let code = result?["code"] as? Int, code == 200 else { return false }
        if let data = result?["data"] as? [AnyHashable: Any] {

            if let lastId = data["lastId"] as? Int {
                self.lastId = lastId
            }

            if lastId == 0 {
                isLastPage = true
            }
        }
        return true
    }
}
