//
//  DUCommunityTrendDetailAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DUCommunityTrendDetailAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    @objc public dynamic var trendId: String? = ""

    public struct Param: CTNetworkingParamConvertible {
        public var trendId: String? = ""
        public init() {}
    }
}

extension DUCommunityTrendDetailAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DUCommunityTrendDetailAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "sns/v2/content/trend-detail"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["contentId"] = params?["trendId"]
        return resultParams
    }
}
