//
//  DuCommunityUserTrendListDetailAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuCommunityUserTrendListDetailAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var tab: Int? // 0:动态 1:喜欢
        public var type: Int? // 0:全部 1:视频 2:专栏 3:资讯 8.得物说
        public var userId: String?
        public var limit: Int?
        public var lastId: String? //addTime
        public var num: Int?
        public var hotNum: Int?
        public var childNum: Int?
        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 20
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DuCommunityUserTrendListDetailAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DuCommunityUserTrendListDetailAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuCommunityUserTrendListDetailAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "sns/v2/content/user-trend-list-detail"
    }

    public var requestType: HTTPMethod {
        return .get
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "PHP_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

extension DuCommunityUserTrendListDetailAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        currentPageNumber += 1
        isFirstPage = currentPageNumber == 1
        return true
    }
}
