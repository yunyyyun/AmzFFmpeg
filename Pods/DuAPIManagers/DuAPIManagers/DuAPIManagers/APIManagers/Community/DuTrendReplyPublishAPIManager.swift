//
//  DuTrendReplyPublishAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

/// 动态详情页面发送评论APIManager
public class DuTrendReplyPublishAPIManager: CTNetworkingAPIManager {

	/// API版本
	/// API存在接口的升级，所以存在接口的多版本。
	public enum Version {
		/// 2020-08-18 之前的版本
		case v1
		/// 2020-08-18 之后的版本。动态详情页面改版。时间在 2020-08-18 4.52
		case v2
	}

	public var version: Version = .v1

	public init(version: DuTrendReplyPublishAPIManager.Version = .v1) {
		super.init()
		self.version = version
		child = self
		validator = self
	}

	public struct Param: CTNetworkingParamConvertible {
		public var trendReplyId: Int?
		public var trendId: Int?
		public var images: String?
        public var memeImages: String?
		public var content: String?
		public var atUserIds: [Any]?
		public var pid: Int?

        // v2接口参数
        public var contentId: Int?
        public var contentType: Int?
        public var replyId: Int?

		public init() {}
	}
}

extension DuTrendReplyPublishAPIManager: CTNetworkingAPIManagerValidator {
	public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
		return .correct
	}

	public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
		return .correct
	}
}

extension DuTrendReplyPublishAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
		return false
	}

    @objc public dynamic var isAPINeedLoginInfo: Bool {
		return true
	}

    @objc public dynamic var methodName: String {
		switch version {
			case .v1:
				return "sns/v1/interact/trend-reply-publish"
			case .v2:
				return "sns/v2/interact/reply-publish"
		}
	}

	public var requestType: HTTPMethod {
		return .post
	}

    @objc public dynamic var serviceIdentifier: String {
		return "PHP_FORM"
	}

    @objc public dynamic var loginServiceIdentifier: String {
		return "DuLoginService"
	}

    @objc public dynamic var moduleName: String {
		return "DuAPIManagers"
	}

	public func transformParams(_ params: Parameters?) -> Parameters? {
		return params
	}
}
