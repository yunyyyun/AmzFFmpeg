//
//  DuCommunityBrandCategoryFeedApi.swift
//  DuAPIManagers
//
//  Created by HG on 2020/11/5.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuCommunityBrandCategoryFeedApi : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var tabId: String = ""
        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 20
    
    @objc public dynamic var lastId: String = ""
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DuCommunityBrandCategoryFeedApi: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DuCommunityBrandCategoryFeedApi: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuCommunityBrandCategoryFeedApi: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "sns-rec/v1/recommend/category/feed"
    }

    public var requestType: HTTPMethod {
        return .get
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
         resultParams["limit"] = pageSize
         resultParams["lastId"] = lastId
        return resultParams
    }
}

extension DuCommunityBrandCategoryFeedApi: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        guard let result = apiManager.fetchAsDictionary() else { return false }
        guard let data = result["data"] as? [AnyHashable: Any] else { return false }
        if let lastId = data["lastId"] as? Int {
            isLastPage = lastId == 0
            self.lastId = "\(lastId)"
        }
        return true
    }
}
