//
//  DuSNSUserTalentAreaRecommendUsers.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//  首次推荐用户列表

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuFirstRecommendUserAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
}

extension DuFirstRecommendUserAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuFirstRecommendUserAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "sns/v1/user/recommend-follow-user-list"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}
