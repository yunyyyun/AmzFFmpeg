//
//  DUCommunityFullReplyListAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DUCommunityFullReplyListAPIManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        public var contentId: String?
        public var contentType: Int?
        public var lastId: String?
        public var anchorReplyId: String?
        public var scene: String?
        
        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 20
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DUCommunityFullReplyListAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DUCommunityFullReplyListAPIManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DUCommunityFullReplyListAPIManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return true
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "sns/v2/interact/full-reply-list"
    }

    public var requestType: HTTPMethod {
        return .get
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "PHP_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

extension DUCommunityFullReplyListAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {

        // if isLoading {
        //     return false
        // }

        // if isLastPage {
        //     return false
        // }

        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        // let result = apiManager.fetchAsDictionary()
        // guard let data = result?["data"] as? [AnyHashable:Any] else { return false }
        // guard let total = data["total"] as? Int else { return false }
        // guard let totalPage = data["pages"] as? Int else { return false }

        // totalCount = total
         currentPageNumber += 1
         isFirstPage = currentPageNumber == 1
//         isLastPage = currentPageNumber == totalPage

        return true
    }
}
