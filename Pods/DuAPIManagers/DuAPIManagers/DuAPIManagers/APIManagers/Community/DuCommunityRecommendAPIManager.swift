//
//  DuCommunityRecommendAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuCommunityRecommendAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {

        public var lastId: String?

        public init() {}
    }
    
    @objc public dynamic var isLastPage:Bool = false
    
    @objc public dynamic var isFirstPage:Bool = true
    
    @objc public dynamic var currentPageNumber:Int = 0
    public var totalCount:Int? = nil
    
    @objc public dynamic var pageSize:Int = 20
    
    @objc public dynamic var lastId: String = ""
    
    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        currentPageNumber = 0
        totalCount = nil
        super.loadData()
    }
}

extension DuCommunityRecommendAPIManager: CTNetworkingAPIManagerPagable {
    
    @objc public dynamic func loadNextPage() {
        super.loadData()
    }
}

extension DuCommunityRecommendAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuCommunityRecommendAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "sns/v2/feed/recommend"
//        return "sns-feed/v1/feed/index-tab/recommend"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
        resultParams["lastId"] = lastId
        return resultParams
    }
}

extension DuCommunityRecommendAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }
    
    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        guard let result = apiManager.fetchAsDictionary() else { return false }
        guard let data = result["data"] as? [AnyHashable: Any] else { return false }
        if let lastId = data["lastId"] as? String {
            isLastPage = lastId == "0" || lastId == ""
            self.lastId = lastId
        }
        return true
    }
}

