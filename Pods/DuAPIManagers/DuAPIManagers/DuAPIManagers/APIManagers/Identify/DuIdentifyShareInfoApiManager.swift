//
//  DuIdentifyShareInfoApiManager.swift
//  DuAPIManagers
//
//  Created by 赵海亭 on 2020/9/3.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import Alamofire
import CTNetworkingSwift

public class DuIdentifyShareInfoApiManager : CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        // public var userName:String?
        // public var password:String?
        public init() {}    
    }
}

extension DuIdentifyShareInfoApiManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if manager.response?.response?.statusCode == 200 {
            return .correct
        } else {
            return .noContent
        }
    }
}

extension DuIdentifyShareInfoApiManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "sns-ar/v1/3d-model/share-info"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

