//
//  DuHomePageLayoutAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuPageLayoutAPIManager: CTNetworkingAPIManager {

    @objc public dynamic var backupJSONFileURL: URL?

    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var test: String = "a" // a or b
        public var page: String = "" // page name
        public var info: [String: Any] = [:] // tab页：[tabId:12345]
        public var app_Version: String?
        public var v_dsl: String?
        public init() {}
    }

    @objc private dynamic var page: String?
}

extension DuPageLayoutAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        guard let pageName = page else { return .missingInfomation }

        if let fetchedData = manager.fetchAsDictionary(), let data = fetchedData["data"] as? [String: Any], let _ = data["components"] as? [Any] {
            UserDefaults.standard.set(manager.fetchAsData(), forKey: "cached-layout-data-\(pageName)")
            return .correct
        }
        if let cachedData = UserDefaults.standard.data(forKey: "cached-layout-data-\(pageName)") {
            setupCachedResponse(cachedData: cachedData)
            return .correct
        }
        if let backupURL = backupJSONFileURL, let backupData = try? Data(contentsOf: backupURL) {
            setupCachedResponse(cachedData: backupData)
            return .correct
        }
        return .missingInfomation
    }
}

extension DuPageLayoutAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/growth-app/home/layout"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        guard let fetchedParams = params else { return nil }
        if let _page = fetchedParams["page"] as? String {
            page = _page
        }

        var finalParams: [String: Any] = [:]
        for (key, value) in fetchedParams {
            if key == "info" {
                guard let info = fetchedParams["info"] as? [String: Any] else {continue}
                for (infoKey, infoValue) in info {
                    finalParams[infoKey] = infoValue
                }
                continue
            }
            finalParams[key] = value
        }
        return finalParams
    }
}

extension DuPageLayoutAPIManager: CTNetworkingBaseAPIManagerInterceptor {

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
    }

    @objc public dynamic func beforePerformFail(_ apiManager: CTNetworkingAPIManager) -> Bool {
        guard let pageName = page else { return true }
        if let cachedData = UserDefaults.standard.data(forKey: "cached-layout-data-\(pageName)") {
            setupCachedResponse(cachedData: cachedData)
            success()
            return false
        }
        if let backupURL = backupJSONFileURL, let backupData = try? Data(contentsOf: backupURL) {
            setupCachedResponse(cachedData: backupData)
            success()
            return false
        }
        return true
    }
}
