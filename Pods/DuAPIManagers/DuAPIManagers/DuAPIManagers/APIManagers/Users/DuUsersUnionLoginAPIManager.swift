//
//  DuUsersUnionLoginAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift
import DuContext_Extension
import CTMediator
import DuLoginModule_Extension
import DuAPIManagers_Extension
import DuAPISignature

public class DuUsersUnionLoginAPIManager: CTNetworkingAPIManager {

    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var userName: String?
        public var password: String?
        public var countryCode: String?
        public var code: String?            // 验证码
        public var accessToken: String?     // 一键登录SDK获取的token; UMSocialResponse.accessToken
        public var openId: String?          // UMSocialResponse.usid
        public var refreshToken: String?    // UMSocialResponse.refreshToken

        public init() {}
    }

    public enum LoginType: String {
        case qq, weibo, weixin, hupu
        case pwd // 用户名密码
        case code // 验证码登录
        case mobile // 一键登录
    }

    public var loginType: LoginType?
}

extension DuUsersUnionLoginAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        guard let data = manager.fetchAsDictionary() else { return .noContent }
        if let status = data["status"] as? Int {
            if status == 704 {
                return .missingInfomation
            }
            if status == 789 {
                CTMediator.sharedInstance().DuLoginModule_presentVerifyCodeLogin()
                return .correct
            }
        }
        return .correct
    }
}

extension DuUsersUnionLoginAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var serviceIdentifier: String {
        // 4.55.0 登录接口修改由PHP改为JAVA
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        // 4.55.0 登录接口路径修改 旧的users/unionLogin
        return "api/v1/app/user_core/users/unionLogin"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        guard var result = params else { return nil }
        result["type"] = loginType?.rawValue
        result["sourcePage"] = fetchSourcePage()
        if let password = result["password"] {
            let md5Password = "\(password)du".md5()
            result["password"] = md5Password
        }
        return result
    }
}

private extension DuUsersUnionLoginAPIManager {

    @objc dynamic func fetchSourcePage() -> String {
        let pasteboard = UIPasteboard.general
        var text = pasteboard.string
        if nil == text {
            text = UserDefaults.standard.string(forKey: "DUPasteBoardText")
        }

        guard let pasteboardText = text else { return "" }

        if pasteboardText.hasPrefix("#dweb"), pasteboardText.hasSuffix("#") {
            return pasteboardText[5..<pasteboardText.count - 1]
        } else {
            return ""
        }
    }
}

private extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
}

extension DuUsersUnionLoginAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        // do nothing
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        if let data = fetchAsDictionary()?["data"] as? [AnyHashable: Any] {
            CTMediator.sharedInstance().DuAPIManagers_didReceiveLoginAPIResponse(responseInfo: data)
//            if let userInfo = data["userInfo"] as? [AnyHashable:Any] {
//                if let userId = userInfo["userId"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .UserID, value: userId, shouldWriteToDisk: true)
//                }
//
//                if let username = userInfo["userName"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .UserName, value: username, shouldWriteToDisk: true)
//                }
//
//                if let userIcon = userInfo["icon"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .UserIcon, value: userIcon, shouldWriteToDisk: true)
//                }
//
//                if let sex = userInfo["sex"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .Sex, value: sex, shouldWriteToDisk: true)
//                }
//
//                if let idioGraph = userInfo["idiograph"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .IdioGraph, value: idioGraph, shouldWriteToDisk: true)
//                }
//
//                if let formatTime = userInfo["formatTime"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .FormatTime, value: formatTime, shouldWriteToDisk: true)
//                }
//
//                if let banned = userInfo["banned"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .Banned, value: banned, shouldWriteToDisk: true)
//                }
//
//                if let vip = userInfo["vip"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .VIP, value: vip, shouldWriteToDisk: true)
//                }
//
//                if let code = userInfo["code"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .Code, value: code, shouldWriteToDisk: true)
//                }
//
//                if let amount = userInfo["amount"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .Amount, value: amount, shouldWriteToDisk: true)
//                }
//
//                if let amountSign = userInfo["amountSign"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .AmountSign, value: amountSign, shouldWriteToDisk: true)
//                }
//
//                if let isOlder = userInfo["isOlder"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsOlder, value: isOlder, shouldWriteToDisk: true)
//                }
//
//                if let isQuestionExpert = userInfo["isQuestionExpert"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsQuestionExpert, value: isQuestionExpert, shouldWriteToDisk: true)
//                }
//
//                if let isAdmin = userInfo["isAdmin"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsAdmin, value: isAdmin, shouldWriteToDisk: true)
//                }
//
//                if let isBindMobile = userInfo["isBindMobile"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsBindMobile, value: isBindMobile, shouldWriteToDisk: true)
//                }
//
//                if let isComplete = userInfo["isComplete"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsComplete, value: isComplete, shouldWriteToDisk: true)
//                }
//
//                if let joinDays = userInfo["joinDays"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .JoinDays, value: joinDays, shouldWriteToDisk: true)
//                }
//
//                if let isCommunityAgreements = userInfo["isCommunityAgreements"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsCommunityAgreements, value: isCommunityAgreements, shouldWriteToDisk: true)
//                }
//
//                if let countryCode = userInfo["countryCode"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .CountryCode, value: countryCode, shouldWriteToDisk: true)
//                }
//
//                if let specialList = userInfo["specialList"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .SpecialList, value: specialList, shouldWriteToDisk: true)
//                }
//
//                if let isMerchant = userInfo["isMerchant"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsMerchant, value: isMerchant, shouldWriteToDisk: true)
//                }
//
//                if let isReadProtocol = userInfo["isReadProtocol"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsReadProtocol, value: isReadProtocol, shouldWriteToDisk: true)
//                }
//
//                if let isCertify = userInfo["isCertify"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsReadProtocol, value: isCertify, shouldWriteToDisk: true)
//                }
//
//                if let isRegister = userInfo["isRegister"] as? Int {
//                    CTMediator.sharedInstance().DuContext_set(key: .IsRegister, value: isRegister, shouldWriteToDisk: true)
//                }
//            }
//
//            if let loginInfo = data["loginInfo"] as? [AnyHashable:Any] {
//                if let loginToken = loginInfo["loginToken"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .LoginToken, value: loginToken, shouldWriteToDisk: true)
//                }
//            }
//
//            if let openImUser = data["openimUser"] as? [AnyHashable:Any] {
//                if let userId = openImUser["uid"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .OpenImUserId, value: userId, shouldWriteToDisk: true)
//                }
//
//                if let password = openImUser["pwd"] as? String {
//                    CTMediator.sharedInstance().DuContext_set(key: .OpenImPassword, value: password, shouldWriteToDisk: true)
//                }
//            }
        }
        return true
    }
}
