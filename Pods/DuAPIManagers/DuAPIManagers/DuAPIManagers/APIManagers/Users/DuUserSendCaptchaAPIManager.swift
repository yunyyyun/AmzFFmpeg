//
//  DuUserSendCaptchaAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuUserSendCaptchaAPIManager: CTNetworkingAPIManager {

    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var countryCode: String?
        public var mobile: String?
        public var riskTypeId: Int = 1
        public init() {}
    }

    public enum DUVerificationCodeScene: Int {
        case register = 0            //注册
        case login = 1               //登录
        case findPassword = 2        //找回密码
        case bindMobilePhone = 3     //绑定手机号
        case vendorCertification = 4 //商户认证
        case bindAlipay = 5          //绑定支付宝
        case accountToCash = 6       //现金账户提现
        case changeMobilePhone = 8   //换绑手机号
        case accountFindPassword = 9 //账号中的找回登录密码，无需传手机号
    }

    public var type: DUVerificationCodeScene?
}

extension DuUserSendCaptchaAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        guard let data = manager.fetchAsDictionary() else {return .noContent}

        if let error = data["error"] as? Bool, error == true {
            return .missingInfomation
        }

        return .correct
    }
}

extension DuUserSendCaptchaAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/user/ice/user/sendCaptcha"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var result = params
        result?["typeId"] = type?.rawValue
        return result
    }
}
