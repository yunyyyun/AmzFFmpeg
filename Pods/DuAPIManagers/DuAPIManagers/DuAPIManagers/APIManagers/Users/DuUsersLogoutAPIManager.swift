//
//  DuUsersLogoutAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift
import CTMediator
import DuContext_Extension

public class DuUsersLogoutAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public var callback:(() -> Void)?
}

extension DuUsersLogoutAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        let mediator = CTMediator.sharedInstance()
        mediator.DuContext_set(key: .LoginToken, value: "")
        return .correct
    }
}

extension DuUsersLogoutAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "users/logout"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_FORM"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return [
            "loginToken": CTMediator.sharedInstance().DuContext_User_LoginToken()
        ]
    }
}

extension DuUsersLogoutAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        // do nothing
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        CTMediator.sharedInstance().DuContext_User_CleanAll()
        if let callback = callback {
            callback()
        }
        return true
    }
}
