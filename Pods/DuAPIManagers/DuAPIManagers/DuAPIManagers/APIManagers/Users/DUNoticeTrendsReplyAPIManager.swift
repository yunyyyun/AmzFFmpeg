//
//  DUNoticeTrendsReplyAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DUNoticeTrendsReplyAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var typeId: Int = 0
        public var replyLightTextVer: Int = 0
        public init() {}
    }

    @objc public dynamic var isLastPage: Bool = false

    @objc public dynamic var isFirstPage: Bool = true

    @objc public dynamic var lastId: String = ""
    public var totalCount: Int?

    @objc public dynamic var pageSize: Int = 20

    @objc public dynamic var currentPageNumber: Int = 20

    @objc public override dynamic func loadData() {
        isLastPage = false
        isFirstPage = true
        lastId = ""
        totalCount = nil
        super.loadData()
    }
}

extension DUNoticeTrendsReplyAPIManager: CTNetworkingAPIManagerPagable {

    @objc public dynamic func loadNextPage() {
        super.loadData()
        isFirstPage = false
    }
}

extension DUNoticeTrendsReplyAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DUNoticeTrendsReplyAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return true
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return true
    }

    @objc public dynamic var methodName: String {
        return "notice/trendsReply"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "PHP_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        var resultParams = params ?? [:]
         resultParams["limit"] = pageSize
         resultParams["lastId"] = lastId
        return resultParams
    }
}

extension DUNoticeTrendsReplyAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        return
    }

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
//        #warning("根据情况选择请求策略，默认任何场景都会发起请求")

         if isLoading {
             return false
         }

         if isLastPage {
             return false
         }

        return true
    }

    @objc public dynamic func beforePerformSuccess(_ apiManager: CTNetworkingAPIManager) -> Bool {
        //        #warning("需要解析翻页参数")
        let result = apiManager.fetchAsDictionary()
        guard let data = result?["data"] as? [AnyHashable: Any] else { return false }
        guard let lastId = data["lastId"] as? String else { return false }

        self.lastId = lastId
        isLastPage = lastId == ""

        return true
    }
}
