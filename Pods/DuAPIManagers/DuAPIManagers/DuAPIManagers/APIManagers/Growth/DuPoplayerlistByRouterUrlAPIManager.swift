//
//  DuPoplayerlistByRouterUrlAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuPoplayerlistByRouterUrlAPIManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }

    public struct Param: CTNetworkingParamConvertible {
        // public var userName:String?
        public var routeUrl: String?
        public var h5Flag: Int?
        public var showPageUrl: String?
        public init() {}
    }
}

extension DuPoplayerlistByRouterUrlAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuPoplayerlistByRouterUrlAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/growth-app/pop/listByRouteUrl"
    }

    public var requestType: HTTPMethod {
        return .post
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

extension DuPoplayerlistByRouterUrlAPIManager: CTNetworkingBaseAPIManagerInterceptor {
    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
        
    }
    
    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return false
    }
}
