//
//  DuHomePageLayoutAPIManager.swift
//  DuAPIManagers
//
//  Created by casa's script on 2020-02-28~17:02:10.
//  Copyright © 2020 casa. All rights reserved.
//

import UIKit
import Alamofire
import CTNetworkingSwift

public class DuHomePageLayoutAPIManager: CTNetworkingAPIManager {

    @objc public dynamic var backupJSONFileURL: URL?

    public override init() {
        super.init()
        child = self
        validator = self
        interceptor = self
    }

    public struct Param: CTNetworkingParamConvertible {
        public var test: String = "a" // a or b
        public init() {}
    }
}

extension DuHomePageLayoutAPIManager: CTNetworkingAPIManagerValidator {
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }

    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        if let fetchedData = manager.fetchAsDictionary(), let data = fetchedData["data"] as? [String: Any], let components = data["components"] as? [Any], components.count > 0 {
            UserDefaults.standard.set(manager.fetchAsData(), forKey: "cached-DuHomePageLayoutAPIManager")
            return .correct
        }
        if let cachedData = UserDefaults.standard.data(forKey: "cached-DuHomePageLayoutAPIManager") {
            setupCachedResponse(cachedData: cachedData)
            return .correct
        }
        if let backupURL = backupJSONFileURL, let backupData = try? Data(contentsOf: backupURL) {
            setupCachedResponse(cachedData: backupData)
            return .correct
        }
        return .missingInfomation
    }
}

extension DuHomePageLayoutAPIManager: CTNetworkingAPIManagerChild {

    @objc public dynamic var isPagable: Bool {
        return false
    }

    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }

    @objc public dynamic var methodName: String {
        return "api/v1/app/growth-app/home/layout"
    }

    public var requestType: HTTPMethod {
        return .get
    }

    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }

    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }

    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }

    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

extension DuHomePageLayoutAPIManager: CTNetworkingBaseAPIManagerInterceptor {

    public func shouldCallAPI(_ apiManager: CTNetworkingAPIManager, params: Parameters?) -> Bool {
        return true
    }

    public func afterAPICalling(_ apiManager: CTNetworkingAPIManager, params: Parameters?) {
    }

    @objc public dynamic func beforePerformFail(_ apiManager: CTNetworkingAPIManager) -> Bool {
        if let cachedData = UserDefaults.standard.data(forKey: "cached-DuHomePageLayoutAPIManager") {
            setupCachedResponse(cachedData: cachedData)
            success()
            return false
        }
        if let backupURL = backupJSONFileURL, let backupData = try? Data(contentsOf: backupURL) {
            setupCachedResponse(cachedData: backupData)
            success()
            return false
        }
        return true
    }
}
