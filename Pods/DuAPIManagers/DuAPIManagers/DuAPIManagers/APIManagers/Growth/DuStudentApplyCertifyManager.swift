//
//  DuStudentApplyCertifyManager.swift
//  DuAPIManagers
//
//  Created by 赵海亭 on 2020/10/23.
//  Copyright © 2020 casa. All rights reserved.
//
//  提交学生认证结果

import Foundation
import Alamofire
import CTNetworkingSwift

public class DuStudentApplyCertifyManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        /// 学校名称
        public var schoolName: String = ""
        /// 学历: 1 “高中”、2 “专科”、3 “本科”、4 “研究生”
        public var education: Int = 0
        /// 学制:2 “2年”、3 “3年”、4 “4年”、5 “5年”
        public var schoolYear: Int = 0
        /// 入学年份
        public var enrollmentDate: String = ""
        /// 学生证明正面图片URL
        public var studentCertificateFrontUrl: String = ""
        /// 学生证明反面图片URL
        public var studentCertificateBackUrl: String = ""
        public init() {}
    }
}

extension DuStudentApplyCertifyManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuStudentApplyCertifyManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/user_info/student/applyStudentCertify"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

