//
//  DuStudentGetSchoolInfoManager.swift
//  DuAPIManagers
//
//  Created by 赵海亭 on 2020/10/23.
//  Copyright © 2020 casa. All rights reserved.
//
//  获取学校库信息

import Foundation
import Alamofire
import CTNetworkingSwift

public class DuStudentGetSchoolInfoManager: CTNetworkingAPIManager {
    public override init() {
        super.init()
        child = self
        validator = self
    }
    
    public struct Param : CTNetworkingParamConvertible {
        /// 元数据类型 1 省 2 市 3 学校
        public var type: Int = 0
        /// 数据ID
        public var dataId: Int = 0
        public init() {}
    }
}

extension DuStudentGetSchoolInfoManager: CTNetworkingAPIManagerValidator{
    public func isCorrect(manager: CTNetworkingAPIManager, params: Parameters?) -> CTNetworkingErrorType.Params {
        return .correct
    }
    
    public func isCorrect(manager: CTNetworkingAPIManager) -> CTNetworkingErrorType.Response {
        return .correct
    }
}

extension DuStudentGetSchoolInfoManager: CTNetworkingAPIManagerChild {
    
    @objc public dynamic var isPagable: Bool {
        return false
    }
    
    @objc public dynamic var isAPINeedLoginInfo: Bool {
        return false
    }
    
    @objc public dynamic var methodName: String {
        return "api/v1/app/user_info/student/getSchoolInfo"
    }

    public var requestType: HTTPMethod {
        return .post
    }
    
    @objc public dynamic var serviceIdentifier: String {
        return "JAVA_JSON"
    }
    
    @objc public dynamic var loginServiceIdentifier: String {
        return "DuLoginService"
    }
    
    @objc public dynamic var logHandleIdentifier: String {
        return "LogMessage"
    }
    
    @objc public dynamic var moduleName: String {
        return "DuAPIManagers"
    }
    
    public func transformParams(_ params: Parameters?) -> Parameters? {
        return params
    }
}

