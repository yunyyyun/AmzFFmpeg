//
//  Environments.enum.swift
//  DuAPIManagers
//
//  Created by casa on 2020/3/3.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import DuContext_Extension
import CTMediator

public enum DuApiEnvironment {
    case release
    case test(Int)
    case dev(Int)
    case preRelease
    case testOld(Int)
    case devOld(Int)
    case wcs
    case mock
}

extension DuApiEnvironment {
    static public func fetchList() -> [String: DuApiEnvironment] {
        return [
            "release": .release,
            "pre release": .preRelease,
            "wcs": .wcs,
            "t0": .test(0),
            "t1": .test(1),
            "t2": .test(2),
            "t4": .test(4),
            "t99": .test(99),
            "d1": .dev(1),
            "d2": .dev(2),
            "mock": .mock
        ]
    }

    public func description() -> String {
        switch self {
        case .release:
            return "release"
        case .preRelease:
            return "pre release"
        case .test(let index):
            return "t\(index)"
        case .dev(let index):
            return "d\(index)"
        case .wcs:
            return "wcs"
        case .mock:
            return "mock"
        default:
            return ""
        }
    }

    static public func currentEnv() -> DuApiEnvironment {
        guard let desc = CTMediator.sharedInstance().DuContext_fetchAny(key: .ApiEnv) as? String else { return .release }
        let list = fetchList()
        return list[desc] ?? DuApiEnvironment.release
    }
}

extension DuApiEnvironment {
    func fetchPhpBaseUrl() -> String {
        switch self {
        case .release:
            return CTMediator.sharedInstance().DuContext_Networking_PHPBaseURL()
        case .preRelease:
            return "http://pre-m.dewu.com"
        case .wcs:
            return "http://cstst-public-web.poizon.com"
        case .test(let index):
            switch index {
            case 0:
                return "http://t0-m.dewu.com"
            case 1:
                return "http://t1-m.dewu.com"
            case 2:
                return "http://t2-m.dewu.com"
            case 4:
                return "http://t4-public-web.poizon.com"
            case 99:
                return "http://t99-m.dewu.com"
            default:
                return ""
            }
        case .dev(let index):
            switch index {
            case 1:
                return "http://d1-m.dewu.com"
            case 2:
                return "http://d2-m.dewu.com"
            default:
                return ""
            }
        case .mock:
            return "http://mock.poizon.com/mock/23"
        default:
            return ""
        }
    }

    func fetchJavaBaseUrl() -> String {
        switch self {
        case .release:
            return CTMediator.sharedInstance().DuContext_Networking_JavaBaseURL()
        case .preRelease:
            return "https://pre-app.dewu.com"
        case .wcs:
            return "http://cs-tst-public-gw.poizon.com"
        case .test(let index):
            switch index {
            case 0:
                return "http://t0-app.dewu.com"
            case 1:
                return "http://t1-app.dewu.com"
            case 2:
                return "http://t2-app.dewu.com"
            case 4:
                return "http://t4-public-gw.poizon.com"
            case 99:
                return "http://t99-app.dewu.com"
            default:
                return ""
            }
        case .dev(let index):
            switch index {
            case 1:
                return "http://d1-app.dewu.com"
            case 2:
                return "http://d2-app.dewu.com"
            default:
                return ""
            }
        case .mock:
            return "http://mock.poizon.com/mock/23"
        default:
            return ""
        }
    }
}
