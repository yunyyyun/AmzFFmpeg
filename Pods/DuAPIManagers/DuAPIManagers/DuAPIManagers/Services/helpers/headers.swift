//
//  headers.swift
//  DuAPIManagers
//
//  Created by casa on 2020/3/5.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import Alamofire
import DuContext_Extension
import CTMediator

enum ContentType {
    case json, form, none
}

func generateHeaders(contentType: ContentType) -> (HTTPHeaders, [String: String]) {

    let mediator = CTMediator.sharedInstance()
    var commonHeaders: [String: String] = [: ]
    commonHeaders["mode"] = mediator.DuContext_App_Mode()
    commonHeaders["platform"] = mediator.DuContext_Device_Platform()
    commonHeaders["uuid"] = mediator.DuContext_Device_UUID()
    commonHeaders["v"] = mediator.DuContext_App_Version()
    commonHeaders["token"] = mediator.DuContext_App_Token()
    commonHeaders["shumeiid"] = mediator.DuContext_App_ShuMeiID()
    commonHeaders["isProxy"] = mediator.DuContext_Device_IsProxyString()
    commonHeaders["emu"] = mediator.DuContext_Device_IsSimulatorString()
    commonHeaders["isRoot"] = mediator.DuContext_Device_isBrokenString()
    commonHeaders["timestamp"] = mediator.DuContext_Device_Timestamp()
    commonHeaders["appId"] = mediator.DuContext_App_Id()
    commonHeaders["loginToken"] = mediator.DuContext_User_LoginToken()
    commonHeaders["X-Auth-Token"] = mediator.DuContext_User_XAuthToken()
    commonHeaders["cookieToken"] = mediator.DuContext_Networking_CookieToken()
    commonHeaders["brand"] = "Apple"

    let filteredHeaders = commonHeaders.filter { !"\($0.value)".isEmpty }

    var httpHeaders = HTTPHeaders(filteredHeaders)

    // system
    switch contentType {
    case .json:
        httpHeaders.add(name: "Content-Type", value: "application/json")
    case .form:
        httpHeaders.add(name: "Content-Type", value: "application/x-www-form-urlencoded; charset=utf-8")
    default: break
    }
    httpHeaders.add(HTTPHeader.defaultUserAgent)
    httpHeaders.add(HTTPHeader.defaultAcceptEncoding)
    httpHeaders.add(HTTPHeader.defaultAcceptLanguage)

    return (httpHeaders, filteredHeaders)
}
