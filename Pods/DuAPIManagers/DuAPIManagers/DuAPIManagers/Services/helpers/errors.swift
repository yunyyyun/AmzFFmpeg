//
//  errors.swift
//  DuAPIManagers
//
//  Created by casa on 2020/4/26.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import CTNetworkingSwift
import CTMediator
import DuContext_Extension
import DuAPIManagers_Extension
import Alamofire
import SwiftyJSON
import DuGT_Extension
import DuLogger

func CommonErrorHandler(apiManager: CTNetworkingAPIManager) -> Bool {
    if let headers = apiManager.response?.response?.allHeaderFields {
        if let xAuthToken = headers["x-auth-token"] as? String {
            CTMediator.sharedInstance().DuContext_set(key: .XAuthToken, value: xAuthToken, shouldWriteToDisk: true)
        }
        if let xAuthToken = headers["X-Auth-Token"] as? String {
            CTMediator.sharedInstance().DuContext_set(key: .XAuthToken, value: xAuthToken, shouldWriteToDisk: true)
        }
        if let cookie = headers["Set-Cookie"] as? String {
            let setCookiesArray = cookie.split(separator: ";").filter({ $0.hasPrefix("duToken") })
            if let duc = setCookiesArray.first?.dropFirst(8), let cookieToken = String(duc).removingPercentEncoding, cookieToken.count > 10 {
                CTMediator.sharedInstance().DuContext_set(key: .CookieToken, value: cookieToken, shouldWriteToDisk: true)
            }
        }
        if let cookie = headers["set-cookie"] as? String {
            let setCookiesArray = cookie.split(separator: ";").filter({ $0.hasPrefix("duToken") })
            if let duc = setCookiesArray.first?.dropFirst(8), let cookieToken = String(duc).removingPercentEncoding, cookieToken.count > 10 {
                CTMediator.sharedInstance().DuContext_set(key: .CookieToken, value: cookieToken, shouldWriteToDisk: true)
            }
        }
    }
    
    if apiManager.response?.response?.statusCode == 485,
       let result = apiManager.response?.result,
       case let .success(resultData) = result,
       let data = resultData {
        validateResponseIfNeed(response: data, apiManager: apiManager)
        return false
    }

    _ = CTMediator.sharedInstance().DuAPIManagers_commonErrorHandler(response: apiManager.response) // 这个函数的返回永远都是true，以后应该改掉

    if let status = apiManager.fetchAsDictionary()?["status"] as? Int, status != 200 {
        apiManager.fail()
        return false
    }

    return true
}

func validateResponseIfNeed(response: Data, apiManager: CTNetworkingAPIManager) {
    guard let json = try? JSON(data: response) else { return }
    
    guard let challenge = json["data"]["challenge"].string,
          let gt = json["data"]["gt"].string,
          let serverStatus = json["data"]["serverStatus"].int
    else {
        apiManager.fail()
        return
    }
    guard let req = apiManager.request else {
        apiManager.fail()
        return
    }
    
    DispatchQueue.main.async {
        CTMediator.sharedInstance().DuGT_startValidate(challenge: challenge, gt: gt, serverStatus: serverStatus) { (validate) in
            if validate {
                AF.request(req).response { (response) in
                    DispatchQueue.main.async {
                        handleResponse(response: response, apiManager: apiManager)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    apiManager.fail()
                }
            }
        }
    }
}

private func handleResponse(response: AFDataResponse<Data?>, apiManager: CTNetworkingAPIManager) {
    apiManager.isLoading = false
    apiManager.response = response
    apiManager.interceptor?.didReceiveResponse(apiManager)
    guard let child = apiManager.child else { return }
    
    let networkService: CTNetworkingService?
    switch child.serviceIdentifier {
    case "JAVA_JSON":
        networkService = DuJavaJsonService.shared
    case "JAVA_FORM":
        networkService = DuJavaFormService.shared
    case "PHP_FORM":
        networkService = DuPhpFormService.shared
    case "PHP_JSON":
        networkService = DuPhpJsonService.shared
    default:
        return
    }
    
    guard let service = networkService else { return }
    
    guard service.handleCommonError(apiManager) else { return }

    if response.error == nil {
        if apiManager.validator?.isCorrect(manager: apiManager) != CTNetworkingErrorType.Response.correct {
            apiManager.fail()
        } else {
            apiManager.success()
        }
    } else {
        apiManager.fail()
    }
}
