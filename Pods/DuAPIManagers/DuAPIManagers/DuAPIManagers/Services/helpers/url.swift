//
//  url.swift
//  DuAPIManagers
//
//  Created by casa on 2020/6/19.
//  Copyright © 2020 casa. All rights reserved.
//

import Alamofire

func generateURLParamString(params: Parameters?) -> String {
    var result = ""
    if let urlParams = params {
        for (key, value) in urlParams {
            result.append("&\(key)=\(value)")
        }
    }
    return result
}

func mergeParams(_ leftParams: Parameters?, _ rightParams: Parameters?) -> Parameters {
    var result: Parameters = [:]

    if let params = leftParams {
        for (key, value) in params {
            result[key] = value
        }
    }

    if let params = rightParams {
        for (key, value) in params {
            result[key] = value
        }
    }

    return result
}
