//
//  DuLoginService.swift
//  DuAPIManagers
//
//  Created by casa on 2020/3/18.
//  Copyright © 2020 casa. All rights reserved.
//

import CTNetworkingSwift
import DuContext_Extension
import CTMediator
import DuLoginModule_Extension
import DuLogger

class DuLoginService: CTNetworkingLoginService {
    static let shared = DuLoginService()
    var storedAPIManager: [CTNetworkingAPIManager] = []

    func isCurrentLoggedIn() -> Bool {
        let isGuest = CTMediator.sharedInstance().DuContext_User_IsGuest()
        return !isGuest
    }

    func doLoginProcess(success:(() -> Void)?, fail:(() -> Void)?, cancel:(() -> Void)?, apiManager: CTNetworkingAPIManager) {
        if CTMediator.sharedInstance().DuContext_App_IsLoginPagePresenting() {
            storedAPIManager.append(apiManager)
        } else {
            CTMediator.sharedInstance().DuContext_set(key: .IsLoginPagePresenting, value: true)
            CTMediator.sharedInstance().DuLoginModule_present(success: success, fail: fail, cancel: cancel)
        }
    }

    func loginSuccessOperation(apiManager: CTNetworkingAPIManager) {
        CTMediator.sharedInstance().DuContext_set(key: .IsLoginPagePresenting, value: false)
        if let shouldLoadData = apiManager.delegate?.shouldContinueRequestAfterLogin(apiManager), shouldLoadData == true { apiManager.loadData() }
        storedAPIManager.forEach { (apiManager) in
            if let shouldLoadData = apiManager.delegate?.shouldContinueRequestAfterLogin(apiManager), shouldLoadData == true { apiManager.loadData() }
        }
        storedAPIManager.removeAll()
    }

    func loginFailOperation(apiManager: CTNetworkingAPIManager) {
        CTMediator.sharedInstance().DuContext_set(key: .IsLoginPagePresenting, value: false)
        apiManager.delegate?.requestDidFailed(apiManager)
        storedAPIManager.forEach { (apiManager) in
            apiManager.delegate?.requestDidFailed(apiManager)
        }
        storedAPIManager.removeAll()
    }

    func loginCancelOperation(apiManager: CTNetworkingAPIManager) {
        CTMediator.sharedInstance().DuContext_set(key: .IsLoginPagePresenting, value: false)
        // do nothing
        Logger.default.debug(user: .casa, "cancel")
    }
}
