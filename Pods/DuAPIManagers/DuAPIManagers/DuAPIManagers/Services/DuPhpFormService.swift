//
//  DuPhpFormService.swift
//  DuAPIManagers
//
//  Created by casa on 2020/2/28.
//  Copyright © 2020 casa. All rights reserved.
//

import CTNetworkingSwift
import Alamofire
import DuContext_Extension
import CTMediator
import DuAPISignature

class DuPhpFormService: CTNetworkingService {
    static let shared = DuPhpFormService()
    
    lazy var session: Session = {
        return Session.default
    }()

    func request(params: Parameters?, extraURLParams: Parameters?, methodName: String, requestType: HTTPMethod) -> URLRequest? {
        switch requestType {
        case .get:
            return generateGETRequest(params, methodName)
        case .post:
            return generatePOSTRequest(mergeParams(params, extraURLParams), extraURLParams, methodName)
        default:
            return nil
        }
    }

    func handleCommonError(_ apiManager: CTNetworkingAPIManager) -> Bool {
        return CommonErrorHandler(apiManager: apiManager)
    }
}

extension DuPhpFormService {
    private func generateGETRequest(_ params: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .none)

        let formatedParams = formatParmaters(params: params, headers: commonHeaders)

        var queryList: [String] = []
        formatedParams.forEach { (key: String, value: Any) in
            guard let urlEncodedKey = key.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            let _value = "\(value)"
            guard let urlEncodedValue = _value.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            queryList.append("\(urlEncodedKey)=\(urlEncodedValue)")
        }
        let queryString = queryList.joined(separator: "&")

        let urlString = "\(DuApiEnvironment.currentEnv().fetchPhpBaseUrl())/\(methodName)?\(queryString)"
        return try? URLRequest(url: urlString, method: .get, headers: headers)
    }

    private func generatePOSTRequest(_ params: Parameters?, _ extraURLParams: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .form)

        let formatedParams = formatParmaters(params: params, headers: commonHeaders)
        guard let signature = formatedParams["newSign"] as? String else { return nil }
        let appendURLParamString = generateURLParamString(params: extraURLParams)
        let urlString = "\(DuApiEnvironment.currentEnv().fetchPhpBaseUrl())/\(methodName)?newSign=\(signature)\(appendURLParamString)"

        var queryList: [String] = []
        for (key, value) in formatedParams {
            guard let urlEncodedKey = key.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {continue}
            let _value = "\(value)"
            guard let urlEncodedValue = _value.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {continue}
            if urlEncodedKey == "newSign" { continue }

            if let valueList = value as? [Any] {
                for valueItem in valueList {
                    queryList.append("\(urlEncodedKey)[]=\(valueItem)")
                }
            } else {
                queryList.append("\(urlEncodedKey)=\(urlEncodedValue)")
            }
        }

        var request = try? URLRequest(url: urlString, method: .post, headers: headers)
        request?.httpBody = queryList.joined(separator: "&").data(using: .utf8)

        return request
    }
}
