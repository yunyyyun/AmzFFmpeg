//
//  DuPhpJsonService.swift
//  DuAPIManagers
//
//  Created by casa on 2020/2/28.
//  Copyright © 2020 casa. All rights reserved.
//

import CTNetworkingSwift
import Alamofire
import CTMediator
import DuContext_Extension
import DuAPISignature

class DuPhpJsonService: CTNetworkingService {
    static let shared = DuPhpJsonService()
    
    lazy var session: Session = {
        return Session.default
    }()

    func request(params: Parameters?, extraURLParams: Parameters?, methodName: String, requestType: HTTPMethod) -> URLRequest? {
        switch requestType {
        case .get:
            return generateGETRequest(params, methodName)
        case .post:
            return generatePOSTRequest(mergeParams(params, extraURLParams), extraURLParams, methodName)
        default:
            return nil
        }
    }

    func handleCommonError(_ apiManager: CTNetworkingAPIManager) -> Bool {
        return CommonErrorHandler(apiManager: apiManager)
    }
}

extension DuPhpJsonService {
    private func generateGETRequest(_ params: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .none)

        let formatedParams = formatParmaters(params: params, headers: commonHeaders)

        var queryList: [String] = []
        formatedParams.forEach { (key: String, value: Any) in
            guard let urlEncodedKey = key.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            let _value = "\(value)"
            guard let urlEncodedValue = _value.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            queryList.append("\(urlEncodedKey)=\(urlEncodedValue)")
        }
        let queryString = queryList.joined(separator: "&")
        let urlString = "\(DuApiEnvironment.currentEnv().fetchPhpBaseUrl())/\(methodName)?\(queryString)"
        return try? URLRequest(url: urlString, method: .get, headers: headers)
    }

    private func generatePOSTRequest(_ params: Parameters?, _ extraURLParams: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .json)

        let formatedParams = formatParmaters(params: params, headers: commonHeaders)
        guard let signature = formatedParams["newSign"] as? String else { return nil }

        let appendURLParamString = generateURLParamString(params: extraURLParams)
        let urlString = "\(DuApiEnvironment.currentEnv().fetchPhpBaseUrl())/\(methodName)?newSign=\(signature)\(appendURLParamString)"

        var request = try? URLRequest(url: urlString, method: .post, headers: headers)
        request?.httpBody = try? JSONSerialization.data(withJSONObject: formatedParams, options: [])

        return request
    }
}
