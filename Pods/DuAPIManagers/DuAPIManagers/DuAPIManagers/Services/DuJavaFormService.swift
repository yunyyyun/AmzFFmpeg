//
//  DuJavaFormService.swift
//  DuAPIManagers
//
//  Created by casa on 2020/2/28.
//  Copyright © 2020 casa. All rights reserved.
//

import CTNetworkingSwift
import Alamofire
import CTMediator
import DuContext_Extension
import DuAPIManagers_Extension
import DuAPISignature

class DuJavaFormService: CTNetworkingService {
    static let shared = DuJavaFormService()
    
    lazy var session: Session = {
        return Session.default
    }()

    func request(params: Parameters?, extraURLParams: Parameters?, methodName: String, requestType: HTTPMethod) -> URLRequest? {
        switch requestType {
        case .post:
            return generatePOSTRequest(mergeParams(params, extraURLParams), extraURLParams, methodName)
        case .get:
            return generateGETRequest(params, methodName)
        default: return nil
        }
    }

    func handleCommonError(_ apiManager: CTNetworkingAPIManager) -> Bool {
        return CommonErrorHandler(apiManager: apiManager)
    }
}

extension DuJavaFormService {
    private func generatePOSTRequest(_ params: Parameters?, _ extraURLParams: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .form)
        let formatedParams = formatParmaters(params: params, headers: commonHeaders)
        guard let signature = formatedParams["newSign"] as? String else { return nil }

        let appendURLParamsString = generateURLParamString(params: extraURLParams)
        let urlString = "\(DuApiEnvironment.currentEnv().fetchJavaBaseUrl())/\(methodName)?newSign=\(signature)\(appendURLParamsString)"

        var queryList: [String] = []
        for (key, value) in formatedParams {
            guard let urlEncodedKey = key.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {continue}
            let _value = "\(value)"
            guard let urlEncodedValue = _value.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {continue}
            if urlEncodedKey == "newSign" { continue }
            queryList.append("\(urlEncodedKey)=\(urlEncodedValue)")
        }

        var request = try? URLRequest(url: urlString, method: .post, headers: headers)
        request?.httpBody = queryList.joined(separator: "&").data(using: .utf8)

        return request
    }

    private func generateGETRequest(_ params: Parameters?, _ methodName: String) -> URLRequest? {
        let params = params ?? [:]
        let (headers, commonHeaders) = generateHeaders(contentType: .none)
        let formatedParams = formatParmaters(params: params, headers: commonHeaders)

        var queryList: [String] = []
        formatedParams.forEach { (key: String, value: Any) in
            guard let urlEncodedKey = key.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            let _value = "\(value)"
            guard let urlEncodedValue = _value.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) else {return}
            queryList.append("\(urlEncodedKey)=\(urlEncodedValue)")
        }
        queryList.append("categoryId=1")
        let queryString = queryList.joined(separator: "&")

        let urlString = "\(DuApiEnvironment.currentEnv().fetchJavaBaseUrl())/\(methodName)?\(queryString)"
        return try? URLRequest(url: urlString, method: .get, headers: headers)
    }
}
