//
//  Target_DuLoginService.swift
//  DuAPIManagers
//
//  Created by casa on 2020/3/18.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation

@objc class Target_DuLoginService: NSObject {
    @objc func Action_DuLoginService(_ params: NSDictionary) -> Any {
        return DuLoginService.shared
    }
}
