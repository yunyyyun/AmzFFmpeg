//
//  Target_DuAPIManagers.swift
//  DuAPIManagers
//
//  Created by casa on 2020/3/18.
//  Copyright © 2020 casa. All rights reserved.
//

import CTNetworkingSwift
import Alamofire
import UIKit
import CTMediator
import DuContext_Extension
import DULeanCloudSDK_Extension
import DuModuleExtensions
import DuEventTracking_Extension

@objc class Target_DuAPIManagers: NSObject {
    @objc func Action_loginAPIManager(_ params: [AnyHashable: Any]) -> CTNetworkingAPIManager? {
        guard let paramSource = params["paramSource"] as? CTNetworkingAPIManagerParamSource else { return nil }
        guard let delegate = params["delegate"] as? CTNetworkingAPIManagerCallbackDelegate else { return nil }

        let loginAPIManager = DuUsersUnionLoginAPIManager()
        loginAPIManager.delegate = delegate
        loginAPIManager.paramSource = paramSource
        return loginAPIManager
    }

    @objc func Action_currentPHPBaseURL(_ params: [AnyHashable: Any]) -> String {
        return DuApiEnvironment.currentEnv().fetchPhpBaseUrl()
    }

    @objc func Action_currentJAVABaseURL(_ params: [AnyHashable: Any]) -> String {
        return DuApiEnvironment.currentEnv().fetchJavaBaseUrl()
    }

    @objc func Action_prepareForPasswordLogin(_ params: [AnyHashable: Any]) {
        guard let loginAPIManager = params["loginAPIManager"] as? DuUsersUnionLoginAPIManager else { return }
        loginAPIManager.loginType = .pwd
    }

    @objc func Action_prepareForVerifyCodeLogin(_ params: [AnyHashable: Any]) {
        guard let loginAPIManager = params["loginAPIManager"] as? DuUsersUnionLoginAPIManager else { return }
        loginAPIManager.loginType = .code
    }

    @objc func Action_loginAPIParams(_ params: [AnyHashable: Any]) -> Parameters? {
        guard let phoneNumber = params["phoneNumber"] as? String else { return nil }
        guard let password = params["password"] as? String else { return nil }
        guard let loginAPIManager = params["loginAPIManager"] as? DuUsersUnionLoginAPIManager else { return nil }

        switch loginAPIManager.loginType {
        case .pwd:
            var apiParams = DuUsersUnionLoginAPIManager.Param()
            apiParams.countryCode = "86"
            apiParams.userName = phoneNumber
            apiParams.password = password
            return apiParams.toParameters()
        case .code:
            var apiParams = DuUsersUnionLoginAPIManager.Param()
            apiParams.countryCode = "86"
            apiParams.userName = phoneNumber
            apiParams.code = password
            return apiParams.toParameters()
        default:
            return nil
        }
    }

    @objc func Action_sendVerifyCodeAPIManager(_ params: [AnyHashable: Any]) -> CTNetworkingAPIManager? {
        guard let paramSource = params["paramSource"] as? CTNetworkingAPIManagerParamSource else { return nil }
        guard let delegate = params["delegate"] as? CTNetworkingAPIManagerCallbackDelegate else { return nil }

        let sendVerifyCodeAPIManager = DuUserSendCaptchaAPIManager()
        sendVerifyCodeAPIManager.delegate = delegate
        sendVerifyCodeAPIManager.paramSource = paramSource
        return sendVerifyCodeAPIManager
    }

    @objc func Action_sendVerifyCodeAPIParams(_ params: [AnyHashable: Any]) -> Parameters? {
        guard let phoneNumber = params["phoneNumber"] as? String else { return nil }

        var params = DuUserSendCaptchaAPIManager.Param()
        params.countryCode = "86"
        params.mobile = phoneNumber
        return params.toParameters()
    }

    @objc func Action_prepareForSendLoginVerifyCode(_ params: [AnyHashable: Any]) {
        guard let sendVerifyCodeAPIManager = params["sendVerifyCodeAPIManager"] as? DuUserSendCaptchaAPIManager else { return }
        sendVerifyCodeAPIManager.type = .login
    }

    @objc func Action_changeAPIEnv(_ params: [AnyHashable: Any]) {

        let apiEnvSelectController = UIAlertController(title: "修改API环境", message: nil, preferredStyle: .alert)

        let envList = DuApiEnvironment.fetchList()
        for item in envList {
            let alertAction = UIAlertAction(title: item.value.description(), style: .default) { _ in
                CT().DuContext_set(key: .JavaBaseURL, value: "", shouldWriteToDisk: true)
                CT().DuContext_set(key: .PHPBaseURL, value: "", shouldWriteToDisk: true)
                CT().DuContext_set(key: .ApiEnv, value: item.value.description(), shouldWriteToDisk: true)
                CT().DULeanCloudSDK_didChangeAPIEnv()
                if let callback = params["callback"] as? (String) -> Void {
                    callback(item.value.description())
                }
            }
            apiEnvSelectController.addAction(alertAction)
        }
        CT().present(apiEnvSelectController, animated: true) {}
    }

    @objc func Action_currentEnvDescription(_ params: [AnyHashable: Any]) -> String {
        return DuApiEnvironment.currentEnv().description()
    }

    @objc lazy dynamic var logoutAPIManager: DuUsersLogoutAPIManager = {
        let apiManager = DuUsersLogoutAPIManager()
        return apiManager
    }()

    @objc func Action_logout(_ params: [AnyHashable: Any]) {
        CTMediator.sharedInstance().DuContext_User_CleanAll()
        CTMediator.sharedInstance().DULeanCloudSDK_logout()
        if let callback = params["callback"] as? (() -> Void) {
            callback()
        }
    }

    @objc func Action_didReceiveLoginAPIResponse(_ params: [AnyHashable: Any]) {
        if let data = params["responseInfo"] as? [AnyHashable: Any] {
            if let userInfo = data["userInfo"] as? [AnyHashable: Any] {
                if let userId = userInfo["userId"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .UserID, value: userId, shouldWriteToDisk: true)
                }

                if let username = userInfo["userName"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .UserName, value: username, shouldWriteToDisk: true)
                }

                if let userIcon = userInfo["icon"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .UserIcon, value: userIcon, shouldWriteToDisk: true)
                }

                if let sex = userInfo["sex"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .Sex, value: sex, shouldWriteToDisk: true)
                }

                if let idioGraph = userInfo["idiograph"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .IdioGraph, value: idioGraph, shouldWriteToDisk: true)
                }

                if let formatTime = userInfo["formatTime"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .FormatTime, value: formatTime, shouldWriteToDisk: true)
                }

                if let banned = userInfo["banned"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .Banned, value: banned, shouldWriteToDisk: true)
                }

                if let vip = userInfo["vip"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .VIP, value: vip, shouldWriteToDisk: true)
                }

                if let code = userInfo["code"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .Code, value: code, shouldWriteToDisk: true)
                }

                if let amount = userInfo["amount"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .Amount, value: amount, shouldWriteToDisk: true)
                }

                if let amountSign = userInfo["amountSign"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .AmountSign, value: amountSign, shouldWriteToDisk: true)
                }

                if let isOlder = userInfo["isOlder"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsOlder, value: isOlder, shouldWriteToDisk: true)
                }

                if let isQuestionExpert = userInfo["isQuestionExpert"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsQuestionExpert, value: isQuestionExpert, shouldWriteToDisk: true)
                }

                if let isAdmin = userInfo["isAdmin"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsAdmin, value: isAdmin, shouldWriteToDisk: true)
                }

                if let isBindMobile = userInfo["isBindMobile"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsBindMobile, value: isBindMobile, shouldWriteToDisk: true)
                }

                if let isComplete = userInfo["isComplete"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .IsComplete, value: isComplete, shouldWriteToDisk: true)
                }

                if let joinDays = userInfo["joinDays"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .JoinDays, value: joinDays, shouldWriteToDisk: true)
                }

                if let isCommunityAgreements = userInfo["isCommunityAgreements"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsCommunityAgreements, value: isCommunityAgreements, shouldWriteToDisk: true)
                }

                if let countryCode = userInfo["countryCode"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .CountryCode, value: countryCode, shouldWriteToDisk: true)
                }

                if let specialList = userInfo["specialList"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .SpecialList, value: specialList, shouldWriteToDisk: true)
                }

                if let isMerchant = userInfo["isMerchant"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsMerchant, value: isMerchant, shouldWriteToDisk: true)
                }

                if let isReadProtocol = userInfo["isReadProtocol"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .IsReadProtocol, value: isReadProtocol, shouldWriteToDisk: true)
                }

                if let isCertify = userInfo["isCertify"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsReadProtocol, value: isCertify, shouldWriteToDisk: true)
                }

                if let isRegister = userInfo["isRegister"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsRegister, value: isRegister, shouldWriteToDisk: true)
                }

                if let token = userInfo["token"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .Token, value: token, shouldWriteToDisk: true)
                }

                if let mobile = userInfo["mobile"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .Mobile, value: mobile, shouldWriteToDisk: true)
                }

                if let isIdentifyExpert = userInfo["isIdentifyExpert"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsIdentifyExpert, value: isIdentifyExpert, shouldWriteToDisk: true)
                }

                if let isSetWithdrawPassword = userInfo["isSetWithdrawPassword"] as? Bool {
                    CTMediator.sharedInstance().DuContext_set(key: .IsSetWithdrawPassword, value: isSetWithdrawPassword, shouldWriteToDisk: true)
                }

                if let publishTrendUserName = userInfo["publishTrendUserName"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .PublishTrendUserName, value: publishTrendUserName, shouldWriteToDisk: true)
                }

                if let customerUrl = userInfo["customerUrl"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .CustomerURL, value: customerUrl, shouldWriteToDisk: true)
                }

                if let accountType = userInfo["accountType"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .AccountType, value: accountType, shouldWriteToDisk: true)
                }

                if let balance = userInfo["balance"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .Balance, value: balance, shouldWriteToDisk: true)
                }

                if let isCommunityAgreements = userInfo["isCommunityAgreements"] as? Int {
                    CTMediator.sharedInstance().DuContext_set(key: .IsCommunityAgreements, value: isCommunityAgreements, shouldWriteToDisk: true)
                }

                if let account = userInfo["account"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .Account, value: account, shouldWriteToDisk: true)
                }

                if let recommendReason = userInfo["recommendReason"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .RecommendReason, value: recommendReason, shouldWriteToDisk: true)
                }

                if let accountName = userInfo["accountName"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .AccountName, value: accountName, shouldWriteToDisk: true)
                }

                if let group = userInfo["group"] as? [[AnyHashable: Any]], let groupInfo = group.first, let vipLogo = groupInfo["logo"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .VIPLogo, value: vipLogo)
                }
            }

            if let loginInfo = data["loginInfo"] as? [AnyHashable: Any] {
                if let loginToken = loginInfo["loginToken"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .LoginToken, value: loginToken, shouldWriteToDisk: true)
                }
            }

            if let openImUser = data["openimUser"] as? [AnyHashable: Any] {
                if let userId = openImUser["uid"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .OpenImUserId, value: userId, shouldWriteToDisk: true)
                }

                if let password = openImUser["pwd"] as? String {
                    CTMediator.sharedInstance().DuContext_set(key: .OpenImPassword, value: password, shouldWriteToDisk: true)
                }
            }
        }
        CTMediator.sharedInstance().DULeanCloudSDK_didLogin()
    }

    @objc func Action_didChangeAPIEnv(_ params: [AnyHashable: Any]) {
        guard let envString = params["env"] as? String else {
            CTMediator.sharedInstance().DuContext_set(key: .ApiEnv, value: DuApiEnvironment.currentEnv().description(), shouldWriteToDisk: true)
            CTMediator.sharedInstance().DULeanCloudSDK_didChangeAPIEnv()
            CTMediator.sharedInstance().DuFileUploader_environmentChanged()
            CTMediator.sharedInstance().DuEventTracking_environmentChanged()
            return
        }

        var env = DuApiEnvironment.currentEnv()
        if envString == "Tdewu_0" {
            env = DuApiEnvironment.test(0)
        }
        if envString == "Tdewu_1" {
            env = DuApiEnvironment.test(1)
        }
        if envString == "Tdewu_2" {
            env = DuApiEnvironment.test(2)
        }
        if envString == "Tdewu_3" {
            env = DuApiEnvironment.test(3)
        }
        if envString == "Tdewu_4" {
            env = DuApiEnvironment.test(4)
        }
        if envString == "Ddewu_0" {
            env = DuApiEnvironment.dev(0)
        }
        if envString == "Ddewu_1" {
            env = DuApiEnvironment.dev(1)
        }
        if envString == "Ddewu_2" {
            env = DuApiEnvironment.dev(2)
        }
        if envString == "Ddewu_3" {
            env = DuApiEnvironment.dev(3)
        }
        if envString == "Ddewu_4" {
            env = DuApiEnvironment.dev(4)
        }
        if envString == "Ddewu_5" {
            env = DuApiEnvironment.dev(5)
        }
        if envString == "wcs" {
            env = DuApiEnvironment.wcs
        }
        if envString == "release" {
            env = DuApiEnvironment.release
        }
        if envString == "preRelease" {
            env = DuApiEnvironment.preRelease
        }

        CTMediator.sharedInstance().DuContext_set(key: .ApiEnv, value: env.description(), shouldWriteToDisk: true)
        CTMediator.sharedInstance().DULeanCloudSDK_didChangeAPIEnv()
    }
}
