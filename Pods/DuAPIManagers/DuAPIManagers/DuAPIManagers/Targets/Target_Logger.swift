//
//  Target_Logger.swift
//  DuAPIManagers
//
//  Created by 景鹏博 on 2020/11/19.
//  Copyright © 2020 casa. All rights reserved.
//

import Foundation
import DuLogger

@objc class Target_LogMessage: NSObject {
    @objc dynamic func Action_LogMessage(_ params: NSDictionary) -> Bool {
        guard let message = params["message"] as? String else { return false }
        Logger.default.verbose(user: .casa, message)
        return true
    }
}
