//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

fileprivate let ModuleName = "DuEventTracking"

public extension CTMediator {
    
    func DuEventTracking_start(launchOptions: [AnyHashable: Any]?, projectName: String, callbackEvent: @escaping (_ projectName: String, _ event: String, _ properties: [AnyHashable: Any]?, _ duration: Double?) -> Void) {
        var params: [AnyHashable: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        if let launchOptions = launchOptions {
            params["launchOptions"] = launchOptions
        }
        params["projectName"] = projectName
        params["callBackEvent"] = callbackEvent
        performTarget(ModuleName, action: "start", params: params, shouldCacheTarget: true)
    }
    
    func DuEventTracking_track(event: String, properties: [AnyHashable: Any]?, duration: Double?) {
        var params: [AnyHashable: Any] = [
            "event": event,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        if let properties = properties {
            params["properties"] = properties
        }
        if let duration = duration {
            params["duration"] = duration
        }
        performTarget(ModuleName, action: "track", params: params, shouldCacheTarget: true)
    }
    
    func DuEventTracking_login(userId: String) {
        let params: [AnyHashable: Any] = [
            "userId": userId,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "login", params: params, shouldCacheTarget: true)
    }
    
    func DuEventTracking_handle(url: URL) -> Bool {
        let params: [AnyHashable: Any] = [
            "url": url,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        if let result = performTarget(ModuleName, action: "handleURL", params: params, shouldCacheTarget: true) as? Bool {
            return result
        } else {
            return false
        }
    }
    
    func DuEventTracking_environmentChanged() {
        let params: [AnyHashable: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "environmentChanged", params: params, shouldCacheTarget: true)
    }

}
