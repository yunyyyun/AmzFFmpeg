//
//  CTMediator+DuShuMeiSDK.m
//  DuShuMeiSDK
//
//  Created by casa on 2020/3/9.
//  Copyright © 2020 casa. All rights reserved.
//

#import "CTMediator+DuShuMeiSDK.h"
#import <objc/runtime.h>

@implementation CTMediator (DuShuMeiSDK)

static const void *DuShuMeiSDK_Id = @"DuShuMeiSDK_Id";

- (NSString *)DuShuMeiSDK_test
{
    NSString *result = (NSString *)[self performTarget:@"DuShuMeiSDK" action:@"test" params:nil shouldCacheTarget:NO];
    return result;
}

- (void)DuShuMeiSDK_prepare
{
    [self performTarget:@"DuShuMeiSDK" action:@"prepare" params:nil shouldCacheTarget:NO];
}

- (NSString *)DuShuMeiSDK_fetchId
{
    NSString *result = objc_getAssociatedObject(self, &DuShuMeiSDK_Id);
    if (result == nil || [result isEqualToString: @""]) {
        result = (NSString *)[self performTarget:@"DuShuMeiSDK" action:@"fetchId" params:nil shouldCacheTarget:NO];
        objc_setAssociatedObject(self, &DuShuMeiSDK_Id, result, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return result;
}

@end
