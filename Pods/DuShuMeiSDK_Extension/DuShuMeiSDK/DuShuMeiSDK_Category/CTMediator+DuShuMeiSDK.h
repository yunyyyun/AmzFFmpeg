//
//  CTMediator+DuShuMeiSDK.h
//  DuShuMeiSDK
//
//  Created by casa's script 
//  Copyright © 2020 casa. All rights reserved.
//

#import <CTMediator/CTMediator.h>

NS_ASSUME_NONNULL_BEGIN

@interface CTMediator (DuShuMeiSDK)

- (NSString *)DuShuMeiSDK_test;
- (void)DuShuMeiSDK_prepare;
- (NSString *)DuShuMeiSDK_fetchId;
    
@end

NS_ASSUME_NONNULL_END
