//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

private let ModuleName = "DuPersistentUUID"

public extension CTMediator {
    func DuPersistentUUID_device() -> String {
        let params: [AnyHashable: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        if let result = self.performTarget("DuPersistentUUID", action: "deviceUUID", params: params, shouldCacheTarget: false) as? String {
            return result
        }
        return ""
    }
}
