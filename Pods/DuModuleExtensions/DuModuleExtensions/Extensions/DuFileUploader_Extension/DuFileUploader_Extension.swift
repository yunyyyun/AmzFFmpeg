//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

private let ModuleName = "DuFileUploader"

public extension CTMediator {

    /// 环境改变
    func DuFileUploader_environmentChanged() {
        let params: [AnyHashable: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "environmentChanged", params: params, shouldCacheTarget: false)
    }
}
