//
//  UIFont+Localize.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/7/3.
//  Copyright © 2020 susan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public extension Reactive where Base: UILabel {
    /// Bindable sink for `UIFont` property.
    var font: Binder<UIFont> {
        return Binder(self.base) { element, value in
            element.font = value
        }
    }
}
