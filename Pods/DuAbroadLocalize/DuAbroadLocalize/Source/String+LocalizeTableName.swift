//
//  String+LocalizeTableName.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/6/10.
//  Copyright © 2020 susan. All rights reserved.
//

import Foundation
import RxCocoa

public extension String {
    func i18n(using tableName: String?) -> String {
        return localized(using: tableName)
    }
    
    func i18nRx(using tableName: String?) -> Driver<String> {
        return DuAbroadLocalize.shared.rxLocalized(self, using: tableName)
    }
    
    func i18nFormat(format argument: String, using tableName: String?) -> String {
        return localizedFormat(arguments: argument, using: tableName)
    }
    
    func i18nFormat(format argument: CVarArg..., using tableName: String?) -> String {
        return localizedFormat(arguments: argument, using: tableName)
    }
}
