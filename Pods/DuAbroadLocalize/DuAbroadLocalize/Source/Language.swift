//
//  Language.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/6/10.
//  Copyright © 2020 susan. All rights reserved.
//

import UIKit

public enum Region: String, Codable {
    case hongkong = "HK"
    case japan = "JP"
//    case Korea = "KR" // 目前还没上线 后续上线再放开
    
    func allLanguages() -> [Language] {
        switch self {
        case .hongkong:
            return [.English, .繁体中文]
        case .japan:
            return [.Japan]
        }
    }
    
    func defaultLanguage() -> Language {
        switch self {
        case .hongkong:
            return .繁体中文
        case .japan:
            return .Japan
        }
    }
    
    public var currentLanguages: Language {
        if let current = Language(rawValue: Locale.current.languageCode ?? "en"), allLanguages().contains(current) {
            return current
        }
        return defaultLanguage()
    }
}

public enum Language: String {
    case English = "en"
    case 繁体中文 = "zh-HK"
    case Japan = "ja"
    
    var languageCode: String {
        switch self {
        case .English:
            return "en"
        case .繁体中文:
            return "zh-HK"
        case .Japan:
            return "ja"
        }
    }
    
    var serverLanguageCode: String {
        switch self {
        case .English:
            return "en"
        case .繁体中文:
            return "zh-TW"
        case .Japan:
            return "ja"
        }
    }
}
