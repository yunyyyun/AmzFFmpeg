//
//  DuAbroadLocalize.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/6/10.
//  Copyright © 2020 susan. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

let regionKey = "region_key"

open class DuAbroadLocalize: NSObject {
    
    @objc public static dynamic let shared = DuAbroadLocalize()
    private let disposeBag = DisposeBag()
    private let rxCurrent: BehaviorRelay<Region> = BehaviorRelay(value: .hongkong)

    public var current: Region {
        if let regionCode = UserDefaults.standard.value(forKey: regionKey) as? String,
           let region = Region(rawValue: regionCode) {
            return region
        }
        return .hongkong
    }
    
    @objc public dynamic func serverLanguage() -> String {
        return current.currentLanguages.serverLanguageCode
    }
    
    public static func setCurrentRegion(region: Region) {
        UserDefaults.standard.set(region.rawValue, forKey: regionKey)
        DuAbroadLocalize.shared.rxCurrent.accept(region)
        
    }
    
    @objc public static dynamic func setCurrentRegion(regionCode: String?) {
        guard let code = regionCode,
            let region = Region(rawValue: code) else {return}
        UserDefaults.standard.set(region.rawValue, forKey: regionKey)
        DuAbroadLocalize.shared.rxCurrent.accept(region)
    }
    
    public func rxLocalized(_ string: String, using tableName: String?) -> Driver<String> {
        return rxCurrent.map { (_) -> String in
            string.i18n(using: tableName)
        }.asDriver(onErrorJustReturn: "")
    }
    
    public func rxFont(font: UIFont) -> Driver<UIFont> {
        return rxCurrent.map { (region) -> UIFont in
            let language = region.currentLanguages
            return font
        }.asDriver(onErrorJustReturn: font)
    }
    
    /// hotfix translation with contents
    /// - Parameter translations: contents
    public func updateResource(_ translations: [[String: String]]) {
        
        let bundleName = "Localize.bundle"
        let fileManager = FileManager.default
        let cachePath = NSHomeDirectory() + "/Library/Caches"
        let cacheBundlePath = cachePath + "/\(bundleName)"
        let modules = ["Commodity", "Common", "Favorites", "Identify", "Personal", "User"]
        if fileManager.fileExists(atPath: cacheBundlePath) {
            let projPath = cacheBundlePath + "/\(self.current.currentLanguages.languageCode).lproj"
            // 记录是否有更新资源，否则会有过多写文件
            var isUpdate = false
            
            modules.forEach { (module) in
                let stringFilePath = "\(projPath)/\(module).strings"
                
                if let fileDic = NSDictionary.init(contentsOfFile: stringFilePath) as? [String: String] {
                    var tempDic = fileDic
                    translations.filter({return $0["module"] == module})
                        .forEach { (dic) in
                            tempDic[dic["key"]!] = dic["value"]!
                            if !isUpdate {isUpdate = true}
                    }
                    if isUpdate {
                        do {
                        try fileManager.removeItem(atPath: stringFilePath)
                        fileManager.createFile(atPath: stringFilePath, contents: nil, attributes: nil)
                        let fileHanlde = FileHandle.init(forUpdatingAtPath: stringFilePath)
                            DispatchQueue.global().async {
                                for (key, value) in tempDic {
                                    do {
                                        if #available(iOS 13.0, *) {
                                        //                                     try fileHanlde?.seekToEnd()
                                        } else {
                                        //                                     fileHanlde?.seekToEndOfFile()
                                        }
                                        fileHanlde?.write("\n\"\(key)\" = \"\(value)\";".data(using: .utf8)!)
                                    } catch  {
                                        print("error-")
                                    }
                                }
                            }
                        } catch  {
                          print("error-")
                        }
                    }
                    isUpdate = false
                }
                
            }
            
        }
    }
}
