//
//  String+Localized.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/6/10.
//  Copyright © 2020 susan. All rights reserved.
//

import Foundation

/// bundle & tableName friendly extension
public extension String {
    
    /**
     Swift 2 friendly localization syntax, replaces NSLocalizedString.
     
     - parameter tableName: The receiver’s string table to search. If tableName is `nil`
     or is an empty string, the method attempts to use `Localizable.strings`.
     
     - parameter bundle: The receiver’s bundle to search. If bundle is `nil`,
     the method attempts to use main bundle.
     
     - returns: The localized string.
     */
    func localized(using tableName: String?) -> String {
        
        if let _ = UserDefaults.standard.value(forKey: "translationVersion") as? String {

            let bundleName = "Localize.bundle"
            let fileManager = FileManager.default
            let cachePath = NSHomeDirectory() + "/Library/Caches"
            let mainBundle = Bundle.main.resourcePath?.appending("/\(bundleName)")
            let cacheBundlePath = cachePath + "/\(bundleName)"
            
            if !fileManager.fileExists(atPath: cacheBundlePath) {
                if let _ = try? fileManager.copyItem(atPath: mainBundle!, toPath: cacheBundlePath) {
                    if let dic = Bundle.main.infoDictionary,
                        let version = dic["CFBundleShortVersionString"] as? String{
                            UserDefaults.standard.set(version, forKey: "Localize_version")
                    }
                    print("拷贝成功--")
                } else {
                    let current = Bundle(for: DuAbroadLocalize.self)
                    if let url = current.url(forResource: "Localize", withExtension: "bundle"),
                        let localizedBundle = Bundle(url: url),
                        let path = localizedBundle.path(forResource: DuAbroadLocalize.shared.current.currentLanguages.languageCode, ofType: "lproj"),
                        let bundle = Bundle(path: path)  {
                        return bundle.localizedString(forKey: self, value: nil, table: tableName)
                    }
                }
            }
            if let dic = Bundle.main.infoDictionary,
                let version = dic["CFBundleShortVersionString"] as? String,
                let cacheVersion = UserDefaults.standard.object(forKey: "Localize_version") as? String,
            version != cacheVersion{
                if let _ = try? fileManager.copyItem(atPath: mainBundle!, toPath: cacheBundlePath) {
                    if let dic = Bundle.main.infoDictionary {
                        if let version = dic["CFBundleShortVersionString"] as? String{
                            UserDefaults.standard.set(version, forKey: "Localize_version")
                        }
                    }
                    print("拷贝成功--")
                } else {
                    let current = Bundle(for: DuAbroadLocalize.self)
                    if let url = current.url(forResource: "Localize", withExtension: "bundle"),
                        let localizedBundle = Bundle(url: url),
                        let path = localizedBundle.path(forResource: DuAbroadLocalize.shared.current.currentLanguages.languageCode, ofType: "lproj"),
                        let bundle = Bundle(path: path)  {
                        return bundle.localizedString(forKey: self, value: nil, table: tableName)
                    }
                }
            }
            if let localizedBundle = Bundle(path: cacheBundlePath),
               let path = localizedBundle.path(forResource: DuAbroadLocalize.shared.current.currentLanguages.languageCode, ofType: "lproj"),
               let bundle = Bundle(path: path) {
               return bundle.localizedString(forKey: self, value: nil, table: tableName)
            }
        } else {
            let current = Bundle(for: DuAbroadLocalize.self)
            if let url = current.url(forResource: "Localize", withExtension: "bundle"),
                let localizedBundle = Bundle(url: url),
                let path = localizedBundle.path(forResource: DuAbroadLocalize.shared.current.currentLanguages.languageCode, ofType: "lproj"),
                let bundle = Bundle(path: path)  {
                return bundle.localizedString(forKey: self, value: nil, table: tableName)
            }
        }
        
        return self
    }
    
    /**
     Swift 2 friendly localization syntax with format arguments, replaces String(format:NSLocalizedString).
     
     - parameter arguments: arguments values for temlpate (substituted according to the user’s default locale).
     
     - parameter tableName: The receiver’s string table to search. If tableName is `nil`
     or is an empty string, the method attempts to use `Localizable.strings`.
     
     - returns: The formatted localized string with arguments.
     */
    func localizedFormat(arguments: CVarArg..., using tableName: String?) -> String {
        return String(format: localized(using: tableName), arguments: arguments)
    }
    
    /**
     Swift 2 friendly plural localization syntax with a format argument.
     
     - parameter argument: Argument to determine pluralisation.
     
     - parameter tableName: The receiver’s string table to search. If tableName is `nil`
     or is an empty string, the method attempts to use `Localizable.strings`.
     
     - parameter bundle: The receiver’s bundle to search. If bundle is `nil`,
     the method attempts to use main bundle.
     
     - returns: Pluralized localized string.
     */
    func localizedPlural(argument: CVarArg, using tableName: String?, in bundle: Bundle?) -> String {
        return NSString.localizedStringWithFormat(localized(using: tableName) as NSString, argument) as String
    }
    
}

