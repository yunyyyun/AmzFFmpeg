//
//  Int+LocalizeFormat.swift
//  DuAbroadLocalize
//
//  Created by susan on 2020/7/3.
//  Copyright © 2020 susan. All rights reserved.
//

import Foundation

public extension Int {
    
    var roundingMode: NumberFormatter.RoundingMode {
        switch DuAbroadLocalize.shared.current {
        case .japan:
            return .halfUp
        default:
            return .halfUp
        }
    }
    
    var percentNumber: NSDecimalNumber {
        let toPercent = NSDecimalNumber(value: self).dividing(by: NSDecimalNumber(value: 10000))
        return toPercent
    }
    
    /// 服务端以分为单位（PS：日本除外，日本以元为单位）
    var serviceValue: Int {
        switch DuAbroadLocalize.shared.current {
        case .japan:
            return self
        default:
            return self * 100
        }
    }
    
    var priceValue: NSNumber {
        switch DuAbroadLocalize.shared.current {
        case .japan:
            return NSNumber(value: self)
        default:
            return NSDecimalNumber(value: self).dividing(by: NSDecimalNumber(value: 100))
        }
    }
    
    ///把服务器返回的金额转换成带币种的string
    /// - Parameter fraction: 小数点后显示几位（日本固定不显示小数点，设置也不会生效）
    /// - Returns:带币种的金额（用于展示）
    func currencyPrice(fraction: Int = 2, showDash: Bool = true, containWhitespace isContained: Bool = true) -> String {
        let format = NumberFormatter()
        let space = isContained ? " " : ""
        format.currencySymbol = "Price.Unit".i18n(using: "Commodity") + space
        format.numberStyle = .currency
        // 日本不出现小数
        switch DuAbroadLocalize.shared.current {
        case .japan:
            format.maximumFractionDigits = 0
            format.minimumFractionDigits = 0
        default:
            format.maximumFractionDigits = fraction
            format.minimumFractionDigits = fraction
        }
        format.roundingMode = roundingMode
        return formatString(format: format, showDash: showDash)
    }
    
    ///把服务器返回的金额转换成不带币种的string
    /// - Parameter fraction: 小数点后显示几位（日本固定不显示小数点，设置也不会生效）
    /// - Returns:不带币种的金额（用于展示）
    func unsignedPrice(fraction: Int = 2, showDash: Bool = true) -> String {
        let format = NumberFormatter()
        format.currencySymbol = ""
        format.numberStyle = .currency
        // 日本不出现小数
        switch DuAbroadLocalize.shared.current {
        case .japan:
            format.maximumFractionDigits = 0
            format.minimumFractionDigits = 0
        default:
            format.maximumFractionDigits = fraction
            format.minimumFractionDigits = fraction
        }
        format.roundingMode = roundingMode
        return formatString(format: format, showDash: showDash)
    }
    
    func formatString(format: NumberFormatter, showDash: Bool = true) -> String {
        let defaultValue = showDash ? "--" : "0"
        guard self > 0 else {
            return "\("Price.Unit".i18n(using: "Commodity"))\(defaultValue)"
        }
        return format.string(from: priceValue) ?? "\("Price.Unit".i18n(using: "Commodity"))\(defaultValue)"
    }
    
    /// 计算百分比金额
    /// - Parameter percent: 百分比（850代表8.5%）
    /// - Returns: 计算后四舍五入的值
    func calculate(with percent: Int) -> Int {
        let numberValue = NSDecimalNumber(value: serviceValue)
        let behavior = NSDecimalNumberHandler(roundingMode: .plain, scale: 0, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        let result = numberValue.multiplying(by: percent.percentNumber, withBehavior: behavior)
        return result.intValue
    }
}
