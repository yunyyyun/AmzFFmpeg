//
//  signature.swift
//  DuAPISignature
//
//  Created by casa on 2020/9/2.
//  Copyright © 2020 casa. All rights reserved.
//

import Alamofire
import CryptoSwift
import DuLogger

internal enum DUParams: Int, CaseIterable {
    case de356 = 0
    case db1cb = 1
    case du6i8 = 2
    case dju2a = 3
    case di0ad = 4
    case d9u9a = 5
    case dv9b4 = 6
    case dle3o = 7
    case d9e78 = 8
    case d9b72 = 9

    func stringValue() -> String {
        switch self {
        case .de356:
            return "e356"
        case .db1cb:
            return "b1cb"
        case .du6i8:
            return "u6i8"
        case .dju2a:
            return "ju2a"
        case .di0ad:
            return "i0ad"
        case .d9u9a:
            return "9u9a"
        case .dv9b4:
            return "v9b4"
        case .dle3o:
            return "le3o"
        case .d9e78:
            return "9e78"
        case .d9b72:
            return "9b72"
        }
    }

    func newParams(_ data: String, value: String, type: Padding, params: [String: Any]) -> String? {
        var kB = [UInt8]()
        let paramsCount = (params.count + data.count) % 10
        guard let paramsType = DUParams(rawValue: paramsCount) else {
            return nil
        }
        for type in DUParams.allCases {
            if  paramsType != self {
                if type.rawValue < 4 {
                    kB.append(contentsOf: type.stringValue().bytes)
                }
            } else {
                if type.rawValue < 2 || type.rawValue > 7 {
                    kB.append(contentsOf: type.stringValue().bytes)
                }
            }
        }
        do {
            kB = kB.map { $0 - 1 }
            let aes = try AES(key: kB, blockMode: ECB(), padding: type)
            let ciphertext = try aes.encrypt(data.bytes).toBase64()
            return ciphertext
        } catch {
            return nil
        }
    }

    func newJSONParams(_ data: String, value: String, type: Padding, params: [String: Any]) -> String? {
        var kB = [UInt8]()
        let paramsCount = (params.count + data.count) % 10
        guard let paramsType = DUParams(rawValue: paramsCount) else {
            return nil
        }
        for type in DUParams.allCases {
            if  paramsType != self {
                if type.rawValue < 4 {
                    kB.append(contentsOf: type.stringValue().bytes)
                }
            } else {
                if type.rawValue < 2 || type.rawValue > 7 {
                    kB.append(contentsOf: type.stringValue().bytes)
                }
            }
        }
        do {
            kB = kB.map({ (value) -> UInt8 in
                return value - 1
            })
            let aes = try AES(key: "b46afc89e13025d7".bytes, blockMode: ECB(), padding: type)
            let ciphertext = try aes.encrypt(data.bytes).toBase64()
            return ciphertext
        } catch {
            return nil
        }
    }
}

internal var privateKey: String {
    return "3542e676b4c80983f6131cdfe577ac9b"
}

func ignoreHeaders(_ headers: [String: String]) -> [String: String] {
    return headers.filter({ false
        || $0.0 == "v"
        || $0.0 == "loginToken"
        || $0.0 == "platform"
        || $0.0 == "timestamp"
        || $0.0 == "uuid"
    })
}

/// 合并格式化请求参数，添加公共参数，签名
///
/// - Parameters:
///   - params: 请求参数
///   - headers: 公共参数
/// - Returns: 签名后的参数
public func formatParmaters(params: [String: Any], headers: [String: String]) -> [String: Any] {
    //    let headers = ignoreHeaders(headers)
    var p = params.filter { (kv) -> Bool in
        switch kv.value {
        case let arr as [Any]:
            return !arr.isEmpty
        default:
            return true
        }
    }
    p.merge(headers) { (current, _) -> Any in
        return current
    }

    p = p.mapValues({ (val) -> Any in
        if let v = val as? String {
            return v.replacingOccurrences(of: "\\/", with: "/")
        }
        return val
    })

    let signParams = p.mapValues({ (val) -> String in
        if val is [String: Any] {
            return ""
        }
        switch val {
        case let arrayParams as [Any]:
            if arrayParams.isEmpty { return "" }

            do {
                let js = try JSONSerialization.data(withJSONObject: val)
                return String(data: js, encoding: .utf8) ?? ""
            } catch {
                return ""
            }
        case let v as String:
            return v
        case let f as CGFloat:
            let number = NSNumber(value: Double(f))
            return number.stringValue
        case let i as Int64:
            let number = NSNumber(value: i)
            return number.stringValue
        case let d as Double:
            let number = NSNumber(value: d)
            return number.stringValue
        case let f as Float:
            let number = NSNumber(value: f)
            return number.stringValue
        case let num as NSNumber:
            return num.stringValue
        default:
            return "\(val)"
        }
    })

    let keys = signParams.keys.filter { !["pv", "appId"].contains($0) }.sorted()
    let sign_Str = keys.reduce("") { (res, key) -> String in
        let val = signParams[key] ?? ""
        return res.appending("\(key)\(val)")
    }
    #if DEBUG
    Logger.default.debug(user: .casa, "未签名前的参数拼接的字符串 : ".appending(sign_Str))
    #endif
    p["sign"] = "\(sign_Str)\(privateKey)".md5()

    let paramsCount = (signParams.count + sign_Str.count) % 10
    if let type = DUParams(rawValue: paramsCount) {
        let newP = type.newParams(sign_Str, value: "v9b48d67e130i0ad", type: .pkcs5, params: signParams)
        p["newSign"] = newP?.md5()
    }
    return p
}

public func formatParmaters_json(params: [String: Any], headers: [String: String]) -> [String: Any] {
    var p = params.filter { (kv) -> Bool in
        switch kv.value {
        case let arr as [Any]:
            return !arr.isEmpty
        default:
            return true
        }
    }

    p.merge(headers) { (current, _) -> Any in
        return current
    }

    p = p.mapValues({ (val) -> Any in
        if let v = val as? String {
            return v.replacingOccurrences(of: "\\/", with: "/")
        }
        return val
    })

    var sign_Str = ""
    var signParams = [String: Any]()
    (signParams, sign_Str) = bodySign(params: p)
    #if DEBUG
    Logger.default.debug(user: .casa, "未签名前的参数拼接的字符串 : ".appending(sign_Str))
    #endif

    let paramsCount = (signParams.count + sign_Str.count) % 10
    if let type = DUParams(rawValue: paramsCount) {
        let newP = type.newParams(sign_Str, value: "v9b48d67e130i0ad", type: .pkcs5, params: signParams)
        p["newSign"] = newP?.md5()
    }
    return p
}

private func bodySign(params: [String: Any]) -> ([String: Any], String) {
    let signParams = params.mapValues({ (val) -> String in
        switch val {
        case let arrayParams as [Any]:
            return arrayParams.map(convertSignValue).joined(separator: ",")
        default:
            return convertSignValue(val: val)
        }
    })
    let keys = params.keys.filter { !["pv", "appId"].contains($0) }.sorted()
    let sign_Str = keys.reduce("") { (res, key) -> String in
        if let val = signParams[key] {
            let newResult = res.appending(key).appending(val)
            return newResult
        } else {
            return res
        }
    }
    return (signParams, sign_Str)
}

private func convertSignValue(val: Any) -> String {
    switch val {
    case let v as String:
        return v
    case let f as CGFloat:
        let number = NSNumber(value: Double(f))
        return number.stringValue
    case let i as Int64:
        let number = NSNumber(value: i)
        return number.stringValue
    case let d as Double:
        let number = NSNumber(value: d)
        return number.stringValue
    case let f as Float:
        let number = NSNumber(value: f)
        return number.stringValue
    case let b as Bool:
        return "\(b)"
    case let num as NSNumber:
        return num.stringValue
    default:
        if let jsonData = try? JSONSerialization.data(withJSONObject: val, options: []), let jsonString = String(data: jsonData, encoding: .utf8) {
            let replacedSlashJsonString = jsonString.replacingOccurrences(of: "\\/", with: "/")
            return replacedSlashJsonString
        } else {
            return ""
        }
    }
}
