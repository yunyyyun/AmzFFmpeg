//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

let ModuleName = "DuAliLogService"

public enum DuAliLogReportAction: Int {
    case access = 0    // 访问
    case exposure = 1  // 曝光
    case click = 2     // 点击
    case end = 3       // 结束
}

public enum DuAliLogProject: String {
    case du
    case poizon
    case duApm
    case duApmBusiness
    case rnCrash
    case dunc
    case other
    case abtest
    case dw_widget
}

public enum DuLogChannel: String {
    case appStore
    case testFlight
    case pgyer
}

public protocol DuAliLogRawProtocol {
    var page: String { get }
    var block: String { get }
    var event: String { get }
    var action: DuAliLogReportAction { get }
    var position: String { get }
    var duration: String { get }
    var data: [String: Any]? { get }
}

public struct LogConstants { }

public extension CTMediator {
    func DuAliLogService_setup() {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setup", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_setupUserId() {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setupUserId", params: params, shouldCacheTarget: false)
    }
    
    func DuAliLogService_setupAppKey(_ key: String) {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName,
            "key": key
        ]
        performTarget(ModuleName, action: "setupAppKey", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_setAliYunLog(isOpenAPP: Bool, attributes: [AnyHashable: Any]) {
        let params: [String: Any] = [
            "isOpenAPP": isOpenAPP,
            "attributes": attributes,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setAliYunLog", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_addProject(_ project: DuAliLogProject) {
        let params: [String: Any] = [
            "project": project,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "addProject", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_report(page: String,
                                action: DuAliLogReportAction,
                                block: String = "",
                                event: String = "",
                                position: String = "",
                                duration: String = "",
                                data: Any? = nil,
                                immediately: Bool = false) {
        let params: [String: Any] = [
            "page": page,
            "action": action,
            "block": block,
            "event": event,
            "position": position,
            "duration": duration,
            "data": data ?? "",
            "immediately": immediately,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "report", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_report<T: DuAliLogRawProtocol>(_ log: T) {
        let params: [String: Any] = [
            "page": log.page,
            "action": log.action,
            "block": log.block,
            "event": log.event,
            "position": log.position,
            "duration": log.duration,
            "data": log.data ?? "",
            "immediately": false,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "report", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_report(customData: [String: String], project: DuAliLogProject, immediately: Bool = false) {
        let params: [String: Any] = [
            "data": customData,
            "project": project,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "reportCustomData", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_rush() {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "rush", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_setIsShowLogWindow(_ isShowLogWindow: Bool) {
        let params: [String: Any] = [
            "isShowLogWindow": isShowLogWindow,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setIsShowLogWindow", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_getIsShowLogWindow() -> Bool {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]

        let isShowLogWindow = performTarget(ModuleName, action: "getIsShowLogWindow", params: params, shouldCacheTarget: false) as? Bool
        return isShowLogWindow ?? false
    }

    func DuAliLogService_getLogWindow() -> UIView {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]

        let logWindow = performTarget(ModuleName, action: "getLogWindow", params: params, shouldCacheTarget: false) as? UIView
        return logWindow ?? UIView()
    }

    func DuAliLogService_setSessionId(_ sessionId: String) {
        let params: [String: Any] = [
            "sessionId": sessionId,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setSessionId", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_getSessionId() -> String {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]

        let sessionId = performTarget(ModuleName, action: "getSessionId", params: params, shouldCacheTarget: false) as? String
        return sessionId ?? ""
    }

    func DuAliLogService_setRangersLog(_ block: @escaping (_ page: String?, _ action: Int, _ block: String?, _ event: String?, _ data: Any?) -> Void) {
        let params: [String: Any] = [
            "block": block,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]

        performTarget(ModuleName, action: "setRangersLog", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_reportForRangers(page: String,
                                          action: DuAliLogReportAction,
                                          block: String = "",
                                          event: String = "",
                                          data: Any? = nil) {
        let params: [String: Any] = [
            "page": page,
            "action": action,
            "block": block,
            "event": event,
            "data": data ?? "",
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "reportForRangers", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_registerForRangers() {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "registerForRangers", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_setupUserIdForRangers() {
        let params: [String: Any] = [
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "setupUserIdForRangers", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_getABTestForRangers(keyName: String, keyValue: Any) -> Any {
        let params: [String: Any] = [
            "keyName": keyName,
            "keyValue": keyValue,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]

        return performTarget(ModuleName, action: "getABTestForRangers", params: params, shouldCacheTarget: false) as Any
    }

    func DuAliLogService_reportForLogDisplayWindow(project: String, event: String, properties: [AnyHashable: Any]?, duration: Double?) {
        let params: [String: Any] = [
            "project": project,
            "event": event,
            "properties": properties ?? [:],
            "duration": duration ?? 0,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        performTarget(ModuleName, action: "reportForLogDisplayWindow", params: params, shouldCacheTarget: false)
    }

    func DuAliLogService_isLogDisplayWindowType(view: UIView?) -> Bool {
        guard let someView = view else { return false }
        let params: [String: Any] = [
            "view": someView,
            kCTMediatorParamsKeySwiftTargetModuleName: ModuleName
        ]
        let isLogDisplayWindowType = performTarget(ModuleName, action: "isLogDisplayWindowType", params: params, shouldCacheTarget: false) as? Bool
        return isLogDisplayWindowType ?? false
    }
}
