//
//  DuExposureLogService_Extension.swift
//  DuAliLogService_Extension
//
//  Created by Todd Cheng on 2020/7/15.
//  Copyright © 2020 DeWu. All rights reserved.
//

import RxCocoa
import RxSwift
import CTMediator

public extension CTMediator {
    /*
     社区列表曝光埋点规则：
     
     1. 第一次加载完成/下拉刷新，曝光当前展示的第一屏。
     2. APP进入二级页面/APP进入后台后，返回列表，重新曝光当前屏幕展示的卡片。
     3. 列表滚动停止时，曝光当前屏未曝光的数据。
     4. 未曝光的定义：重新进入/第一次进入 当前屏幕的卡片。
     */

    /// 获取需要曝光的事件
    ///
    /// - Parameters:
    ///   - headerRefresh: 下拉刷新的通知,刷新新数据后传入事件
    ///   - willEndScroll: scrollView.rx.willEndScroll 传入这个API
    ///   - realAppear: vc.rx.realAppear() 调用这个方法. 传入api
    ///   - didDisappear: didDisappear 的通知.
    ///   - getRows: 调用闭包获取当前所有展示的元素
    ///   - isNeedEmptyArray: 是否需要空数组数据的回调
    /// - Returns: 需要曝光的元素序列
    func exposureEvent<T: Equatable>(headerRefresh: Observable<Void>,
                                     willEndScroll: Observable<Void>,
                                     realAppear: Observable<Void>,
                                     didDisappear: Observable<Void>,
                                     isDataLoaded: Bool = true,
                                     isInCurrentPage: Bool = true,
                                     isNeedBackgroundExposure: Bool = true,
                                     isNeedEmptyArray: Bool = false,
                                     indexPathsForVisibleRows getRows: @escaping () -> ([T]?)) -> Observable<[T]> {

        return Observable<[T]>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            /// 是否在当前页，解决App去到二级页面时,压入后台又回到前台进行曝光
            var _isInCurrentPage: Bool = isInCurrentPage

            /// 是否需要曝光
            var _needExposure: Bool = true

            /// 是否加载成功数据
            var _isDataLoaded: Bool = isDataLoaded

            /// 最终使用这个属性进行判断是否需要曝光
            var isNeedExposure: Bool {
                return _isInCurrentPage && _needExposure && _isDataLoaded
            }

            // 离开当前页面
            func leaveCurrentPage() {
                _isInCurrentPage = false
            }

            // 回到当前页面
            func comeCurrentPage() {
                _isInCurrentPage = true
            }

            // 停止曝光
            func stopExposure() {
                _needExposure = false
            }

            // 开始曝光
            func startExposure() {
                _needExposure = true
            }

            // 数据加载完成
            func dataLoaded() {
                _isDataLoaded = true
            }

            // 创建一个 indexPath(model) 的列表. 用于存储上次曝光的 index(model)
            var lastExposure: [T] = []

            // APP将要活跃的通知事件
            let didBecome = NotificationCenter.default
                .rx.notification(UIApplication.didBecomeActiveNotification)
                .mapEmptyType
                .share()
                .filter {isNeedBackgroundExposure}

            // APP将要去后台的通知
            let willResign = NotificationCenter.default
                .rx.notification(UIApplication.willResignActiveNotification)
                .mapEmptyType
                .share()
                .filter {isNeedBackgroundExposure}

            // 更新上次曝光的下标(model)
            func updateLastExposure(_ indexPaths: [T]?) {
                guard let indexPaths = indexPaths else { return }
                // 记录曝光
                lastExposure = indexPaths
            }

            // 传入当前界面的下标,去重后 发送曝光的下标
            func exposure(_ indexPaths: [T]?) {
                guard let indexPaths = indexPaths else { return }
                // 判断是否需要曝光
                guard isNeedExposure else { return }
                // 去重
                let exposureArr = indexPaths.filter {!lastExposure.contains($0)}
                if exposureArr.count > 0 {
                    // 曝光
                    ob.onNext(exposureArr)
                    // 记录曝光
                    updateLastExposure(indexPaths)
                } else if exposureArr.count == 0 && isNeedEmptyArray == true {
                    ob.onNext([])
                }
            }

            // 当 App 进入 二级页面/进入后台后, 停止进行曝光
            Observable
                .merge(willResign, didDisappear.map(leaveCurrentPage))
                .subscribe(onNext: stopExposure)
                .disposed(by: bag)

            // didBecome： 当app 回到前台
            // realAppear： 回到当前页
            // headerRefresh: 第一次加载完成/下拉刷新
            // 曝光当前展示的第一屏。且不去重。
            Observable
                .merge(didBecome, realAppear.map(comeCurrentPage), headerRefresh.map(dataLoaded))
                .subscribe(onNext: { _ in
                    // 开始曝光
                    startExposure()
                    // 清除之前曝光的数据,这几个触发的曝光事件不去重
                    updateLastExposure([])
                    // 曝光当前数据
                    exposure(getRows())
                })
                .disposed(by: bag)

            // 列表滚动停止时，曝光当前屏未曝光的数据。
            willEndScroll
                .asObservable()
                .map(getRows)
                .subscribe(onNext: exposure)
                .disposed(by: bag)

            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }
    
    
    /*
     社区列表曝光结束埋点规则：
     
     1. 上次曝光的数据，本次不进行曝光，则上报曝光结束以及曝光时长
     */
    
    /// 获取需要曝光的事件
    ///
    /// - Parameters:
    ///   - headerRefresh: 下拉刷新的通知,刷新新数据后传入事件
    ///   - willEndScroll: scrollView.rx.willEndScroll 传入这个API
    ///   - realAppear: vc.rx.realAppear() 调用这个方法. 传入api
    ///   - didDisappear: didDisappear 的通知.
    ///   - getRows: 调用闭包获取当前所有展示的元素
    /// - Returns: 需要曝光的元素序列
    func exposureDurationEvent<T: Equatable>(headerRefresh: Observable<Void>,
                                     willEndScroll: Observable<Void>,
                                     realAppear: Observable<Void>,
                                     didDisappear: Observable<Void>,
                                     isDataLoaded: Bool = true,
                                     isInCurrentPage: Bool = true,
                                     isNeedBackgroundExposure: Bool = true,
                                     indexPathsForVisibleRows getRows: @escaping () -> ([T]?)) -> Observable<[(T, Double)]> {
        
        
        return Observable<[(T, Double)]>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            /// 是否在当前页，解决App去到二级页面时,压入后台又回到前台进行曝光
            var _isInCurrentPage: Bool = isInCurrentPage

            /// 是否需要曝光
            var _needExposure: Bool = true

            /// 是否加载成功数据
            var _isDataLoaded: Bool = isDataLoaded

            /// 最终使用这个属性进行判断是否需要曝光
            var isNeedExposure: Bool {
                return _isInCurrentPage && _needExposure && _isDataLoaded
            }

            // 离开当前页面
            func leaveCurrentPage() {
                _isInCurrentPage = false
            }

            // 回到当前页面
            func comeCurrentPage() {
                _isInCurrentPage = true
            }

            // 停止曝光
            func stopExposure() {
                _needExposure = false
            }

            // 开始曝光
            func startExposure() {
                _needExposure = true
            }

            // 数据加载完成
            func dataLoaded() {
                _isDataLoaded = true
            }
            
            // 记录曝光的数据以及曝光开始的时间
            var exposuredArray: [(T, TimeInterval)] = []

            // APP将要活跃的通知事件
            let didBecome = NotificationCenter.default
                .rx.notification(UIApplication.didBecomeActiveNotification)
                .mapEmptyType
                .share()
                .filter {isNeedBackgroundExposure}

            // APP将要去后台的通知
            let willResign = NotificationCenter.default
                .rx.notification(UIApplication.willResignActiveNotification)
                .mapEmptyType
                .share()
                .filter {isNeedBackgroundExposure}

            // 更新上次曝光的下标(model)
            func updateLastExposure(_ indexPaths: [T]?) {
                guard let indexPaths = indexPaths else { return }
                // 记录曝光
                indexPaths.forEach { (newItem) in
                    exposuredArray.append((newItem, Date().timeIntervalSince1970))
                }
            }

            // 传入当前界面的下标,去重后 发送曝光的下标
            func exposure(_ indexPaths: [T]?) {
                guard let indexPaths = indexPaths else { return }
                // 判断是否需要曝光
                guard isNeedExposure else { return }
                // 去重
                let needExposureItems = indexPaths.filter {!exposuredArray.map{$0.0}.contains($0)}
                
                // 找出上次曝光没有，这次有的，进行添加。
                let needAddArray = needExposureItems.filter({!exposuredArray.map{$0.0}.contains($0)})
                
                // 记录曝光
                updateLastExposure(needAddArray)
                
                // 找出上次曝光有，这次没有的，进行上报，并计算时间
                let durationArray = exposuredArray.filter({!indexPaths.contains($0.0)})
                
                let durationItems = durationArray.map { oldItem in
                    return (oldItem.0, Date().timeIntervalSince1970 - oldItem.1)
                }
                
                if !durationItems.isEmpty {
                    ob.onNext(durationItems)
                }
                
                // 删除数组内的已经上报了结束的item
                exposuredArray.removeAll { (oldItem, _) -> Bool in
                    return durationArray.contains { (durationItem, _) -> Bool in
                        return durationItem == oldItem
                    }
                }
                
            }

            // 当 App 进入 二级页面/进入后台后, 停止进行曝光
            Observable
                .merge(willResign, didDisappear.map(leaveCurrentPage))
                .subscribe(onNext: {
                    if _isInCurrentPage == false {
                        _isInCurrentPage = true
                        exposure([])
                        _isInCurrentPage = false
                    } else {
                        exposure([])
                    }
                    
                    stopExposure()
                })
                .disposed(by: bag)

            // didBecome： 当app 回到前台
            // realAppear： 回到当前页
            // headerRefresh: 第一次加载完成/下拉刷新
            // 曝光当前展示的第一屏。且不去重。
            Observable
                .merge(didBecome, realAppear.map(comeCurrentPage), headerRefresh.map(dataLoaded))
                .subscribe(onNext: { _ in
                    // 开始曝光
                    startExposure()
                    // 清除之前曝光的数据,这几个触发的曝光事件不去重
                    updateLastExposure([])
                    // 曝光当前数据
                    exposure(getRows())
                })
                .disposed(by: bag)

            // 列表滚动停止时，曝光当前屏未曝光的数据。
            willEndScroll
                .asObservable()
                .map(getRows)
                .subscribe(onNext: exposure)
                .disposed(by: bag)

            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }

    /*访问埋点规则
     1. 进入页面/App进入前台开始计时
     2. 离开页面/App进入后台停止计时
     */

    /// - Parameter startTime: 开始事件，默认为调用函数的事件。
    /// - Parameter willAppear: willAppear事件，不要使用 invoke，kvo等获取。
    /// - Parameter didDisappear: didDisappear事件，不要使用 invoke，kvo等获取。
    func accessEvent(startTime: TimeInterval = Date().timeIntervalSince1970,
                     didAppear: Observable<Void>,
                     didDisappear: Observable<Void>,
                     isInCurrentPage: Bool = true) -> Observable<String> {

        return Observable<String>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            // 设置为可选类型是防止因为某种生命周期的错误调用,导致连续上报多次访问时长埋点
            var startTime: TimeInterval? = startTime

            // 判断是否还在当前页面
            var _isInCurrentPage: Bool = isInCurrentPage

            // 离开当前页面
            func leaveCurrentPage() {
                _isInCurrentPage = false
            }

            // 回到当前页面
            func comeCurrentPage() {
                _isInCurrentPage = true
            }

            // APP将要活跃的通知事件
            let didBecome = NotificationCenter.default
                .rx.notification(UIApplication.didBecomeActiveNotification)
                .mapEmptyType

            // APP将要去后台的通知
            let willResign = NotificationCenter.default
                .rx.notification(UIApplication.willResignActiveNotification)
                .mapEmptyType

            // 对开始时间进行处理
            Observable
                .merge(didBecome.filter {_isInCurrentPage}, didAppear.filter {!_isInCurrentPage}.map(comeCurrentPage))
                .subscribe(onNext: { (_) in
                    // print("进入该视图,判断是否startTime 为空")
                    guard startTime == nil else {return}
                    startTime = Date().timeIntervalSince1970
                }).disposed(by: bag)

            // 当前时间作为结束时间,计算访问时长进行回调
            Observable
                .merge(willResign.filter {_isInCurrentPage}, didDisappear.filter {_isInCurrentPage}.map(leaveCurrentPage))
                .subscribe(onNext: { (_) in
                    // print("退出该视图")
                    guard let time = startTime else {return}
                    let durationTime = String(format: "%.3f", Date().timeIntervalSince1970 - time)
                    ob.onNext(durationTime)
                    startTime = nil
                }).disposed(by: bag)
            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }

    /*交易的访问埋点规则
     1. 离开页面/App进入后台准备上报
     2. 进入页面/App上报访问埋点
     */

    /// - Parameter didAppear: didAppear事件，不要使用 invoke，kvo等获取。
    /// - Parameter didDisappear: didDisappear事件，不要使用 invoke，kvo等获取。
    /// - Parameter isOnWindow: 创建函数的时候，当前页面是否在Window上，true： 直接回调上报埋点。 false：等待下一个didAppear事件
    func transAccessEvent(didAppear: Observable<Void>,
                          didDisappear: Observable<Void>,
                          isOnWindow: Bool) -> Observable<Void> {

        return Observable<Void>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            var _isOnWindow: Bool = isOnWindow

            // APP将要活跃的通知事件
            let didBecome = NotificationCenter.default
                .rx.notification(UIApplication.didBecomeActiveNotification)
                .mapEmptyType

            func onWindow() {
                _isOnWindow = true
            }

            func leaveWindow() {
                _isOnWindow = false
            }

            Observable
                .merge(didAppear.map(onWindow), // 出发didAppear之后，修改isOnwindow为true。
                    didBecome) // 同时监听 didAppear和didBecome事件
                .filter {_isOnWindow} // 有事件产生时，判断是否在window上。 ture：上报， false：忽略
                .subscribe(onNext: { (_) in
                    ob.onNext(()) // 同时监听者，上报埋点
                }).disposed(by: bag)

            // 离开当前页面，修改isOnWindow为false
            Observable
                .merge(didDisappear)
                .subscribe(onNext: { (_) in
                    leaveWindow()
                }).disposed(by: bag)
            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }

    func realAppear(didAppear: Observable<Void>,
                    didDisappear: Observable<Void>,
                    isOnWindow: Bool = true
    ) -> Observable<Void> {

        return Observable<Void>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            var isGotoOtherPage: Bool = !isOnWindow

            func gotoOtherPage() {
                isGotoOtherPage = true
            }

            func backCurrentPage() {
                isGotoOtherPage = false
            }

            // 去到其他页面,记录已经离开
            didDisappear
                .subscribe(onNext: gotoOtherPage)
                .disposed(by: bag)

            // 回到当前页的时候 记录已经在当前页
            didAppear
                .subscribe(onNext: { _ in
                    // 已经在其他页面
                    guard isGotoOtherPage else { return }
                    // App pop 回到当前页面.
                    backCurrentPage()
                    // 发送事件
                    ob.onNext(())
                })
                .disposed(by: bag)

            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }

    func willEndScroll(didEndDragging: Observable<Bool>, didEndDecelerating: Observable<Void>) -> Observable<Void> {
        // 用户手动滚动 直接停止无滑动效果
        let draggingStop = didEndDragging
            .filter { $0 == false }
            .mapEmptyType

        // 将要停止的时候发送事件.
        let didEndDecelerating = didEndDecelerating

        return Observable
            .merge(draggingStop, didEndDecelerating)
    }
}

public extension Reactive where Base: UIScrollView {
    var willEndScroll: Observable<Void> {

        return Observable<Void>.create { (ob) -> Disposable in

            var bag = DisposeBag()

            var isNeed_KVO_Exposure: Bool = true

            func end_KVO_Exposure() {
                isNeed_KVO_Exposure = false
            }

            func start_KVO_Exposure() {
                isNeed_KVO_Exposure = true
            }

            func next() {
                ob.onNext(())
            }

            // 用户手动滚动 直接停止无滑动效果
            self.base.rx.didEndDragging
                .filter { $0 == false }
                .mapEmptyType
                .subscribe(onNext: {
                    start_KVO_Exposure()
                    next()
                })
                .disposed(by: bag)

            // 将要停止的时候发送事件.
            self.base.rx.didEndDecelerating
                .asObservable()
                .subscribe(onNext: {
                    start_KVO_Exposure()
                    next()
                })
                .disposed(by: bag)

            // 手动修改contentOffset时曝光
            self.base.rx.contentOffset
                .asObservable()
                .mapEmptyType
                .filter({isNeed_KVO_Exposure})
                .subscribe(onNext: next)
                .disposed(by: bag)

            self.base.rx.willBeginDragging
                .asObservable()
                .mapEmptyType
                .subscribe(onNext: end_KVO_Exposure)
                .disposed(by: bag)

            return Disposables.create {
                bag = DisposeBag()
            }
        }
    }
}

public extension Observable {
    var mapEmptyType: Observable<Void> {
        return map {_ -> Void in return ()}
    }
}
