//
//  UserEventTrackingConstants.swift
//  DuAppService
//
//  Created by 刘立夫 on 2019/8/9.
//  Copyright © 2019 DuApp. All rights reserved.
//

public struct NewUserLogConstants {
    public static let page_app_register_choose_interest = "100103"
    public static let block_app_register_choose_interest_all = "1"
    public static let event_app_register_choose_interest_all_go = "1"
    public static let event_app_register_choose_interest_all_skip = "2"
    public static let event_app_register_choose_interest_all_interests = "3"

    public static let page_app_login_main = "100104"
    public static let block_app_login_main_all = "1"
    public static let event_app_login_main_all_weixin = "1"
    public static let event_app_login_main_all_weibo = "2"
    public static let event_app_login_main_all_qq = "3"
    public static let event_app_login_main_all_phone = "4"
    public static let event_app_login_main_all_hupu = "5"
    public static let event_app_login_main_all_register = "6"

    public static let page_app_phone_login = "100105"
    public static let block_app_phone_login_all = "1"
    public static let event_app_phone_login_all_password_login = "1"
    public static let event_app_phone_login_all_verification_code_login = "2"
    public static let  event_app_phone_login_all_forget_password = "3"
    public static let event_app_phone_login_all_login = "4"

    public static let page_app_phone_register = "100106"
    public static let block_app_phone_register_all = "1"
    public static let event_app_phone_register_all_register = "1"

    public static let page_app_register_fill_info = "100107"
    public static let block_app_register_fill_info_all = "1"
    public static let event_app_register_fill_info_all_save = "1"
    public static let event_app_register_fill_info_all_skip = "2"

    public static let page_app_signup_popup = "100110"
    public static let block_app_signup_popup_all = "1"
    public static let event_app_signup_popup_all_close = "1"
    public static let event_app_signup_popup_all_signup = "2"

    //注册推荐用户页 4.24.0
    public static let page_app_register_recommend_follow = "100111"
    public static let block_app_register_recommend_follow = "1"
    public static let event_app_register_recommend_follow_user = "1"       //点击用户选择
    public static let event_app_register_recommend_follow_button = "2"       //点击关注并进入app
    public static let event_app_register_recommend_follow_skip = "3"         // 跳过

    public static let page_app_oneClick_login_check = "100121"
    public static let block_app_oneclick_login_all = "1"
    public static let event_app_onestep_login_all_login = "1"
    public static let event_app_onestep_login_all_close = "2"
    public static let event_app_onestep_login_all_code = "3"
    public static let event_app_onestep_login_all_thirdparty = "4"

    public static let page_app_others_login = "100130"
    public static let block_app_others_login_all = "1"
    public static let event_app_others_login_all_login = "1"
    public static let event_app_others_login_all_close = "2"
    public static let event_app_others_login_all_thirdparty = "3"
    public static let event_app_others_login_all_pwd_login_success = "4"
    public static let event_app_others_login_all_code_login_success = "5"

    public static let page_app_thirdparty_bind = "100109"
    public static let block_app_thirdparty_bind_all = "1"
    public static let event_app_thirdparty_blind_all_bind = "1"
}

public struct NewUserETConstants {
    public static let event_new_login_third_party_bind_mobile = "new_loginThirdPartBindMobile"
    public static let event_new_popBindMobile = "new_popBindMobile"
    public static let event_new_phoneLogin = "new_phoneLogin"
    public static let value_codeLogin = "codeLogin"
    public static let event_new_loginPage = "new_loginPage"
    public static let value_phonenumber = "phonenumber"
    public static let value_hupu = "hupu"
    public static let value_wechat = "wechat"
    public static let value_qq = "qq"
    public static let value_weibo = "weibo"
    public static let eventId_login = "login"
    public static let value_loginPageA = "loginPageA"
    public static let value_bindSuccess = "bindSuccess"
    public static let event_new_loginForgetPwd = "new_loginForgetPwd"
}
