//
//  Community.swift
//  DuLogReportService
//
//  Created by WYT on 2020/5/9.
//  Copyright © 2020 Wild. All rights reserved.
//

public struct CommunityLogConstants {
    public static let page_tab_news = "205000"
    public static let block_news_category = "1"
    public static let block_news_content = "2"
    public static let event_news_select_category = "1"
    public static let event_news_content_detail = "1"

    public static let page_trend_detail = "200200"
    public static let block_tab_trend_detail_pic_tag_info = "7"
    public static let block_tab_trend_detail_pic_tag_info_goto_product = "1"
    public static let block_tab_trend_detail_pic_tag_info_ar_tryon = "2"
    public static let block_tab_trend_detail_pic_tag_info_post_trend = "3"
    public static let block_tab_trend_detail_pic_tag_info_related_trend = "4"

    public static let page_trend_detail_apollo = "201100"
    public static let block_pic_detail_pic_tag_info = "3"
    public static let block_pic_detail_pic_tag_info_goto_product = "1"
    public static let block_pic_detail_pic_tag_info_ar_tryon = "2"
    public static let block_pic_detail_pic_tag_info_post_trend = "3"
    public static let block_pic_detail_pic_tag_info_related_trend = "4"
    public static let block_tab_trend_detail_pic_tag_info_top_trend = "5"
    public static let block_pic_detail_pic_tag_info_top_trend = "5"
    public static let block_tab_trend_detail_pic_tag_info_filter_trend = "6"
    public static let block_pic_detail_pic_tag_info_filter_trend = "6"
    public static let block_pic_detail_pic_tag_top_content = "6"
    
    public static let page_trend_brand = "201400"
    public static let block_brand_tab_content_hot_pic_tag_expo = "5"
    public static let block_brand_tab_content_hot_card_tag_expo = "6"
    public static let block_brand_tab_content_hot = "3"
    public static let event_brand_tab_content_hot_pic_tag = "21"
    public static let event_brand_tab_content_hot_detail_pic_tag_info_goto_product = "22"
    public static let event_brand_tab_content_hot_card_tag = "23"

    public static let page_trend_spu = "201500"
    public static let block_trend_spu_content_hot_pic_tag_expo = "3"
    public static let block_trend_spu_content_hot_card_tag_expo = "4"
    public static let block_trend_spu_content_hot = "1"
    public static let event_trend_spu_content_hot_pic_tag = "21"
    public static let event_trend_spu_content_hot_detail_pic_tag_info_goto_product = "22"
    public static let event_trend_spu_content_hot_card_tag = "23"

}

/// 直播使用
public extension CommunityLogConstants {
    static let page_live = "210000"
    static let block_live_all = "1"
    static let event_live_all_proudctlist = "1"
    static let event_live_all_proudctcard = "2"
    static let event_live_all_host_follow = "3" // 直播过程中观众关注主播埋点
    static let event_live_all_share = "4"
    static let event_live_all_sharechannel = "5"
    static let event_live_all_morelive = "6"

    static let page_livereview = "210100"
    static let block_livereview_all = "1"
    static let event_livereview_all_proudctlist = "1"
    static let event_livereview_all_host_follow = "2"
    static let event_livereview_all_expound = "3"

    static let page_liveaggregation = "210200"
    static let block_liveaggregation_all = "1"
    static let event_liveaggregation_all_livecontent = "1"
    static let event_liveaggregation_all_trailercontent = "2"
    static let event_liveaggregation_all_follow = "3"

    static let page_live_addproduct = "210001"
    static let block_live_addproduct = "1"
    static let event_live_addproduct = "1"

    static let page_live_productlist = "210002"
    static let block_live_productlist = "1"
    static let event_live_productlist_expound = "1"
}

public struct CommunityETConstants {
    public static let eventId_new_liveRedPacketRemind = "new_liveRedPacketRemind"
    public static let eventId_new_verticalLive = "new_verticalLive"
    public static let eventId_Live = "live"
    public static let eventId_new_liveList = "new_liveList"
    public static let eventId_new_appLive = "new_appLive"
    public static let eventId_new_live = "new_live"

    public static let value_liveReplay = "liveReplay"
    public static let value_liveRemind = "liveRemind"
    public static let value_followUser = "followUser"
    public static let value_enterReportDetail = "enterReportDetail"
    public static let value_selectedReplay = "selectedReplay"
    public static let value_EnterLiveRoom = "enterLiveRoom"
    public static let value_closeBeauty = "closeBeauty"
    public static let value_switchCamera = "switchCamera"
    public static let value_share = "share"
    public static let value_videoCommment = "videoCommment"
    public static let value_showHeader = "showHeader"
    public static let value_consulterSendMsg = "consulterSendMsg"
    public static let value_addDullarAskquestion = "addDullarAskquestion"
    public static let value_sendRedpacket = "sendRedpacket"
    public static let value_sendRedpacketRecharge = "sendRedpacketRecharge"
    public static let value_rechargeDullarafterask = "rechargeDullarafterask"
    public static let value_enterRoom = "enterRoom"
    public static let value_enterGift = "enterGift"
    public static let value_closeLive = "closeLive"
    public static let value_fullScreen = "fullScreen"
    public static let value_closeFullScreen = "closeFullScreen"
    public static let value_tapAskQuestion = "tapAskQuestion"
    public static let value_tapShare = "tapShare"
    public static let value_chooseTag = "chooseTag"
    public static let value_slipRight = "slipRight"

    public static let page_new_live = "new_live"
}
