//
//  Identi.swift
//  DuLogReportService
//
//  Created by WYT on 2020/5/14.
//  Copyright © 2020 Wild. All rights reserved.
//

public enum IdentifiLogConstants {
    public static let page_news_list = "400101"

    public static let block_news_list_all = "1"
    public static let event_news_list_all_category = "1"

    public static let block_news_list = "1"
    public static let event_news_list_content = "1"

    public static let page_tab_service = "400000"
    public static let block_tab_service_all = "1"
    public static let event_tab_service_all_news_detail = "1"
    public static let event_tab_service_all_adv = "2"
    public static let event_tab_service_all_search = "3"
    public static let event_tab_service_all_identification = "4"
    public static let event_tab_service_all_calendar = "5"
    public static let event_tab_service_all_kol_qa = "6"
    public static let event_tab_service_all_news = "7"
    public static let event_tab_service_all_issue_identification = "8"
    public static let event_tab_service_all_identifier = "9"
    public static let event_tab_service_all_category = "10"
    public static let event_tab_service_all_95_points = "11"
    public static let event_tab_service_all_refresh_points = "12"
    public static let event_tab_service_all_95_points_ensure = "13"
    public static let event_tab_service_all_identify = "15"
    public static let event_tab_service_all_talktitle = "16"
    public static let event_tab_service_all_talkcontent = "17"
    public static let event_tab_service_all_roll = "18"
    public static let event_tab_service_all_ar = "19"
}
