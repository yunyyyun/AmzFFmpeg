//
//  Logger.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/16.
//

import UIKit
import YYModel

public final class Logger: NSObject {
    
    /// log level
    public enum Level: Int {
        case error
        case warning
        case info
        case debug
        case verbose
        
        var description: String {
            switch self {
            case .verbose:
                return "Verbose"
            case .debug:
                return "Debug"
            case .info:
                return "Info"
            case .warning:
                return "Warning"
            case .error:
                return "Error"
            }
        }
    }
    
    //----------------------------
    // 以下五个字段只控制在控制台中的打印
    // 设置后, 只有白名单中的log可以打印,
    // 优先级大于黑名单
    //----------------------------
    
    /// 用户白名单
    public static var enableUser: [LoggerUser] = []
    /// 业务线白名单
    @objc public static dynamic var enableService: [String] = []
    /// module白名单
    @objc public static dynamic var enableModule: [String] = []
    /// 文件白名单
    @objc public static dynamic var enableFile: [String] = []
    
    //--------------------------
    // 设置后, 黑名单中的log不再打印
    //--------------------------
    
    /// 用户黑名单
    public static var disableUser: [LoggerUser] = []
    /// 业务线黑名单
    @objc public static dynamic var disableService: [String] = []
    /// module黑名单
    @objc public static dynamic var disableModule: [String] = []
    /// 文件黑名单
    @objc public static dynamic var disableFile: [String] = []
    
    
    /// 控制台打印日志级别
    public static var consoleEnableLevel: Level = .verbose
    /// 本地文件日志打印级别
    public static var fileEnableLevel: Level = .info
    
    @objc private dynamic var serviceName: String
    
    @objc private static dynamic var cacheDirectory: String = "Library/Caches/com.dewu.Logger/"
    
    /// 某次启动, 唯一ID标识, 每次启动唯一uuid
    @objc private static dynamic let uuid = UUID().uuidString
    /// log 日志基本信息
    @objc private static dynamic var basicInfo: LoggerBasicInfo = LoggerBasicInfo()
    
    init(service: String) {
        self.serviceName = service
        super.init()
    }
    
    @objc public static dynamic func setup(_ basicInfo: LoggerBasicInfo, _ cacheDirectory: String?) {
        if let c = cacheDirectory {
            Logger.cacheDirectory = c
        }
        Logger.basicInfo = basicInfo
        Logger.basicInfo.launch_id = Logger.uuid
        FileLogger.setup(cacheDirectory: Logger.cacheDirectory, basicInfo: basicInfo)
    }
    
    @objc public static dynamic func set(expiredTime: Int) {
        FileLogger.default.set(expiredTime: expiredTime)
    }
    
    /// verbose log
    /// - Parameter content: content
    public func verbose(user: LoggerUser, module: String = #fileID, time: Date = Date(), fileName: String = #file, functionName: String = #function, lineNumber: Int = #line, _ content: Any) {
        log(level: .verbose, service: self.serviceName, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
    }
    
    /// debug log
    /// - Parameter content: content
    public func debug(user: LoggerUser, module: String = #fileID, time: Date = Date(), fileName: String = #file, functionName: String = #function, lineNumber: Int = #line, _ content: Any) {
        log(level: .debug, service: self.serviceName, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
    }
    
    /// info log
    /// - Parameter content: content
    public func info(user: LoggerUser, module: String = #fileID, time: Date = Date(), fileName: String = #file, functionName: String = #function, lineNumber: Int = #line, _ content: Any) {
        log(level: .info, service: self.serviceName, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
    }
    
    /// warn log
    /// - Parameter content: content
    public func warn(user: LoggerUser, module: String = #fileID, time: Date = Date(), fileName: String = #file, functionName: String = #function, lineNumber: Int = #line, _ content: Any) {
        log(level: .warning, service: self.serviceName, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
    }
    
    /// error log
    /// - Parameter content: content
    public func error(user: LoggerUser, module: String = #fileID, time: Date = Date(), fileName: String = #file, functionName: String = #function, lineNumber: Int = #line, _ content: Any) {
        log(level: .error, service: self.serviceName, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
    }
    
    /// 获取可上传log文件路径, 可单独上传某个service或者module, 或者service下的module
    /// 注意: 调用此方法后, 无论上传成功失败, 都一定要调用`uploadLogFileSuccess(_, _)`方法或者`uploadLogFileFailed(_, _)`方法
    /// 调用此方法后, 直到调用`uploadLogFileSuccess(_, _)`方法或者`uploadLogFileFailed(_, _)`方法之前, 将无法再次调用此方法获取日志路径
    /// 此方法会生成.log日志文件, 上传后请自行删除处理
    /// - Parameters:
    ///   - service: 想要上传的log的service, 传""为所有service
    ///   - module: 想要上传的log的module, 传""为所有module
    ///   - completion: 路径列表回调
    public static func getLogPathToUpload(_ service: String, module: String, completion: @escaping ([String]) -> Void) {
        FileLogger.default.getLogPathToUpload(service, module: module, completion: completion)
    }
    
    @objc public static dynamic func uploadLogFileSuccess(_ service: String, _ module: String) {
        FileLogger.default.uploadLogFileSuccess(service, module)
    }
    
    @objc public static dynamic func uploadLogFileFailed(_ service: String, _ module: String) {
        FileLogger.default.uploadLogFileFailed(service, module)
    }
    
    /// 获取可上传log文件路径, 独上传当前service中, 某个module下的log
    /// 注意: 调用此方法后, 无论上传成功失败, 都一定要调用`uploadLogFileSuccess(_)`方法或者`uploadLogFileFailed(_)`方法
    /// 调用此方法后, 直到调用`uploadLogFileSuccess(_)`方法或者`uploadLogFileFailed(_)`方法之前, 将无法再次调用此方法获取日志路径
    /// 此方法会生成.log日志文件, 上传后请自行删除处理
    /// - Parameters:
    ///   - module: 想要上传的log的module, 传""为所有module
    ///   - completion: 路径列表回调
    public func getLogPathToUpload(_ module: String, completion: @escaping ([String]) -> Void) {
        FileLogger.default.getLogPathToUpload(serviceName, module: module, completion: completion)
    }
    
    @objc public dynamic func uploadLogFileSuccess(_ module: String) {
        FileLogger.default.uploadLogFileSuccess(serviceName, module)
    }
    
    @objc public dynamic func uploadLogFileFailed(_ module: String) {
        FileLogger.default.uploadLogFileFailed(serviceName, module)
    }
    
    /// log method
    /// - Parameters:
    ///   - level: level
    ///   - content: msg
    fileprivate func log(level: Level, service: String, module: String, user: LoggerUser, time: Date, fileName: String, functionName: String, lineNumber: Int, _ content: Any) {
        
        let file = NSString(string: fileName).lastPathComponent
        let shortFile = NSString(string: file).deletingPathExtension
        let moduleName = module.components(separatedBy: "/").first ?? ""
        
        #if DEBUG
        if Logger.consoleEnableLevel.rawValue >= level.rawValue {
            let inUserWhiteList = Logger.enableUser.contains(user)
            let inServiceWhiteList = Logger.enableService.contains(service)
            let inModuleWhiteList = Logger.enableModule.contains(moduleName)
            let inFileWhiteList = Logger.enableFile.contains(shortFile)
            
            let inUserBlackList = Logger.disableUser.contains(user)
            let inServiceBlackList = Logger.disableService.contains(service)
            let inModuleBlackList = Logger.disableModule.contains(moduleName)
            let inFileBlackList = Logger.disableFile.contains(shortFile)
            
            let inBlackList = inUserBlackList || inServiceBlackList || inModuleBlackList || inFileBlackList
            let inWhiteList = inUserWhiteList || inFileWhiteList || inServiceWhiteList || inModuleWhiteList
            
            if inBlackList {
                if inWhiteList {
                    ConsoleLogger.log(level: level.description, service: service, module: moduleName, user: user, time: time, fileName: file, functionName: functionName, lineNumber: lineNumber, content)
                }
            }else {
                let userEnable = Logger.enableUser.count == 0 || inUserWhiteList
                let serviceEnable = Logger.enableService.count == 0 || inServiceWhiteList
                let moduleEnable = Logger.enableModule.count == 0 || inModuleWhiteList
                let fileEnable = Logger.enableFile.count == 0 || inFileWhiteList
                
                if userEnable || serviceEnable || moduleEnable || fileEnable {
                    ConsoleLogger.log(level: level.description, service: service, module: moduleName, user: user, time: time, fileName: file, functionName: functionName, lineNumber: lineNumber, content)
                }
            }
        }
        #endif
        if Logger.fileEnableLevel.rawValue >= level.rawValue {
            FileLogger.default.log(level: level.rawValue, service: service, module: moduleName, user: user, time: time, fileName: file, functionName: functionName, lineNumber: lineNumber, content)
        }
    }
    
    /**
     @objc private static dynamic func setupBasicInfo() {
         Logger.basicInfo.app_id = CTMediator.sharedInstance().DuContext_App_Id()
         Logger.basicInfo.app_name = (Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String) ?? ""
         Logger.basicInfo.app_version = CTMediator.sharedInstance().DuContext_App_Version()
         Logger.basicInfo.app_version_code = CTMediator.sharedInstance().DuContext_App_BuildVersion()
         Logger.basicInfo.device_ip = CTMediator.sharedInstance().DuContext_Networking_IpAddress()
         Logger.basicInfo.device_model = CTMediator.sharedInstance().DuContext_Device_ModelName()
         Logger.basicInfo.Prodevice_name = CTMediator.sharedInstance().DuContext_Device_Platform()
         Logger.basicInfo.device_os = "iOS"
         Logger.basicInfo.device_os_version = UIDevice.current.systemVersion
         Logger.basicInfo.device_brand = "Apple"
         Logger.basicInfo.device_uuid = CTMediator.sharedInstance().DuContext_Device_UUID()
         Logger.basicInfo.device_oaid = ""
         Logger.basicInfo.device_imei = CTMediator.sharedInstance().DuContext_App_ShuMeiID()
         Logger.basicInfo.visit_mode = (CTMediator.sharedInstance().DuContext_User_Status() != 0)
         Logger.basicInfo.userid = CTMediator.sharedInstance().DuContext_User_UserIdString()
         Logger.basicInfo.is_root = CTMediator.sharedInstance().DuContext_Device_isBroken()
         Logger.basicInfo.is_simulator = CTMediator.sharedInstance().DuContext_Device_IsSimulator()
         Logger.basicInfo.is_proxy = CTMediator.sharedInstance().DuContext_Device_IsProxy()
         Logger.basicInfo.location = CTMediator.sharedInstance().DuContext_Location_City()
         Logger.basicInfo.lat = CTMediator.sharedInstance().DuContext_Location_Latitude()
         Logger.basicInfo.lng = CTMediator.sharedInstance().DuContext_Location_Longitude()
     }
     */
}
