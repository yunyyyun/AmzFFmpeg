//
//  LoggerDB.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/20.
//

import UIKit
import FMDB
import YYModel

internal class LoggerDB: NSObject {
    
    @objc internal static dynamic let `default` = LoggerDB()
    
    @objc static private dynamic var cacheDirectory: String = "Library/Caches/com.dewu.Logger/"
    /// log 日志基本信息
    @objc internal static dynamic var basicInfo: LoggerBasicInfo = LoggerBasicInfo()
    
    private var dbQueue: FMDatabaseQueue?
    private var asyncQueue: DispatchQueue = DispatchQueue(label: "com.siwuai.LoggerAsyncQueue")
    
    @objc private dynamic var dbDuplicateCache: [String: Int] = [:]
    
    @objc internal dynamic func setup(cacheDirectory: String, basicInfo: LoggerBasicInfo) {
        LoggerDB.basicInfo = basicInfo
        LoggerDB.cacheDirectory = cacheDirectory
        checkAndFetchLoggerDBQueue()
        createLogTable()
        createTagTable(tableName: LoggerDB.moduleTable)
        createTagTable(tableName: LoggerDB.serviceTable)
        createTagIndexTable(indexTable: LoggerDB.logModuleIndexTable, tagTable: LoggerDB.moduleTable)
        createTagIndexTable(indexTable: LoggerDB.logServiceIndexTable, tagTable: LoggerDB.serviceTable)
        createDBInfoTable()
        checkOverdueLog()
    }
    
    @objc internal dynamic func close() {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.close()
        }
    }
    
    internal func insertLog(level: Int, service: String, module: String, user: LoggerUser, time: Date, fileName: String, functionName: String, lineNumber: Int, _ content: Any) {
        
        guard let insertLogContentSQL = getInsertLogContentSQL(level: level, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content) else { return }
        let insertServiceSQL = getInsertTagSQL(LoggerDB.serviceTable, service)
        let insertModuleSQL = getInsertTagSQL(LoggerDB.moduleTable, module)
        
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inTransaction({ (db, rollback) in
                do {
                    // 存log
                    try db.executeUpdate(insertLogContentSQL, values: nil)
                    let logID = db.lastInsertRowId
                    // 存service
                    try db.executeUpdate(insertServiceSQL, values: nil)
                    let serviceResult = try db.executeQuery(String(format: "SELECT id FROM %@ WHERE name = ?;", LoggerDB.serviceTable), values: [service])
                    
                    var serviceID: Int64?
                    while serviceResult.next() {
                        serviceID = serviceResult.longLongInt(forColumn: "id")
                    }
                    serviceResult.close()
                    
                    // 存log module索引
                    if let id = serviceID {
                        let logServiceSQL = self.getInsertLogTagIndexSQL(LoggerDB.logServiceIndexTable, logID, id)
                        try db.executeUpdate(logServiceSQL, values: nil)
                    }
                    
                    // 存module
                    try db.executeUpdate(insertModuleSQL, values: nil)
                    let moduleResult = try db.executeQuery(String(format: "SELECT id FROM %@ WHERE name = ?;", LoggerDB.moduleTable), values: [module])
                    
                    var moduleID: Int64?
                    while moduleResult.next() {
                        moduleID = moduleResult.longLongInt(forColumn: "id")
                    }
                    moduleResult.close()
                    
                    // 存log module索引
                    if let id = moduleID {
                        let logModuleSQL = self.getInsertLogTagIndexSQL(LoggerDB.logModuleIndexTable, logID, id)
                        try db.executeUpdate(logModuleSQL, values: nil)
                    }
                }catch let error {
                    Logger.default.verbose(user: .bo, String(format: "insert log failed, error: %@", error.localizedDescription))
                    rollback.pointee = true
                }
            })
        }
    }
    
    /// 获取log
    /// - Parameters:
    ///   - service: log service name
    ///   - module: log module name
    ///   - completion: String: log content, Bool: is complete flag, Int: last log id
    ///                 if String is empty, Bool is true, Int is zero, shouldn't record last id
    ///                 if String not empty, Bool is false, Int is zero, shouldn't record last id
    ///                 whether String is empty or not empty, Bool is true, Int not zero, please record last id, and when you upload log complete, whether success or failed, incoming last id when you call `updateLogStatusToUploadSuccess(_, _, lastID: )` or `updateLogStatusToUploadFailed(_, _, lastID: )`
    internal func queryLogFrom(_ service: String, _ module: String, completion: @escaping (String, Bool, Int) -> Void) {
        let querySQL = getQueryLogContentSQL(service, module)
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inTransaction({ (db, rollBack) in
                self.queryLogFrom(service, module, querySQL: querySQL, db: db, rollBack: rollBack, lastID: 0, completion: completion)
            })
        }
    }
    
    @objc internal dynamic func updateLogStatusToUploadSuccess(_ service: String, _ module: String, lastID: Int) {
        let sql = getUpdateUploadStatusSQL(service, module, targetStatus: 2, currentStatus: 1, lastID: lastID)
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                do {
                    try db.executeUpdate(sql, values: nil)
                }catch {
                    Logger.default.verbose(user: .bo, "modify log upload status and time failed, error: ".appending(error.localizedDescription))
                }
            })
        }
    }
    
    @objc internal dynamic func updateLogStatusToUploadFailed(_ service: String, _ module: String, lastID: Int) {
        let sql = getUpdateUploadStatusSQL(service, module, targetStatus: 0, currentStatus: 1, lastID: lastID)
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                do {
                    try db.executeUpdate(sql, values: nil)
                }catch {
                    Logger.default.verbose(user: .bo, "modify log upload status and time failed, error: ".appending(error.localizedDescription))
                }
            })
        }
    }
    
    @objc internal dynamic func set(expiredTime: Int) {
        LoggerDB.expiredPeriod = expiredTime * 60 * 60 * 24
    }
    
    private func isExistTable(_ tableName: String, completion: @escaping (Bool) -> Void) {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                let result = try? db.executeQuery("select count(*) as 'count' from sqlite_master where type = 'table' and name = ?", values: [tableName])
                guard let rs = result else { return }
                
                while(rs.next()) {
                    let count = rs.int(forColumn: "count")
                    if count == 0 {
                        completion(false)
                    }else {
                        completion(true)
                    }
                }
                rs.close()
            })
        }
    }
    
    @objc private dynamic func checkOverdueLog() {
        let clearTime = Date().timeIntervalSince1970 - Double(LoggerDB.expiredPeriod)
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                do {
                    try db.executeUpdate(String(format: "DELETE FROM %@ WHERE log_time <= %lf", LoggerDB.logTable, clearTime), values: nil)
                }catch {
                    Logger.default.verbose(user: .bo, "clear log recode failed, error: ".appending(error.localizedDescription))
                }
            })
        }
    }
    
    private func queryLogFrom(_ service: String, _ module: String, querySQL: String, db: FMDatabase, rollBack: UnsafeMutablePointer<ObjCBool>, lastID: Int, completion: (String, Bool, Int) -> Void) {
        do {
            var logContent = ""
            let result = try db.executeQuery(querySQL, values: nil)
            var updateSQL = String(format: "UPDATE %@ SET upload_status = 1, upload_time = %lf WHERE", LoggerDB.logTable, Date().timeIntervalSince1970)
            
            /// 如果当前查询结果已经小于 limit 了, 就说明不用再查询下一次了
            var currentQueryCount = 0
            var tempLastID: Int = lastID
            
            while result.next() {
                let id = Int(result.int(forColumn: "id"))
                // 拼接更新上传状态, 时间字段 sql
                if updateSQL.hasSuffix("WHERE") {
                    updateSQL.append(String(format: " %@.id = %ld", LoggerDB.logTable, id))
                }else {
                    updateSQL.append(String(format: " OR %@.id = %ld", LoggerDB.logTable, id))
                }
                
                logContent.append(String(format: "%@\n", NSDictionary(dictionary: result.resultDictionary ?? [:]).yy_modelToJSONString() ?? ""))
                currentQueryCount += 1
                tempLastID = id
            }
            result.close()
            
            // 更新字段, 记录上传状态为正在上传
            if lastID != tempLastID {
                try db.executeUpdate(updateSQL, values: nil)
            }
            
            if currentQueryCount >= LoggerDB.queryLogMaxLimit {
                completion(logContent, false, 0)
                self.queryLogFrom(service, module, querySQL: querySQL, db: db, rollBack: rollBack, lastID: tempLastID, completion: completion)
            }else {
                completion(logContent, true, tempLastID)
            }
            
        }catch {
            Logger.default.verbose(user: .bo, "query log failed, error: ".appending(error.localizedDescription))
            rollBack.pointee = true
            completion("", true, 0)
        }
    }
    
    @objc private dynamic func createLogTable() {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                if !db.tableExists(LoggerDB.logTable) {
                    let result = db.executeStatements(String(format: "CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, log_time timestamp, file_name text, function_name text, line integer, level integer, content text, app_id text, app_name text, app_version text, app_version_code text, device_ip text, device_model text, prodevice_name text, device_os text, device_os_version text, device_brand text, device_uuid text, device_oaid text, device_imei text, visit_mode boolean, userid text, is_root boolean, is_simulator boolean, is_proxy boolean, location text, lat text, lng text, launch_id text, logger_user text, upload_status integer, upload_time timestamp);", LoggerDB.logTable))
                    if result {
                        Logger.default.verbose(user: .bo, "create log table success")
                    }else {
                        Logger.default.verbose(user: .bo, "create log table failed")
                    }
                }
            })
        }
    }
    
    @objc private dynamic func createTagTable(tableName: String) {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                if !db.tableExists(tableName) {
                    let result = db.executeStatements(String(format: "CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, name text UNIQUE);", tableName))
                    if result {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table success", tableName))
                    }else {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table failed", tableName))
                    }
                }
            })
        }
    }
    
    @objc private dynamic func createTagIndexTable(indexTable: String, tagTable: String) {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inDatabase({ (db) in
                if !db.tableExists(indexTable) {
                    let result = db.executeStatements(String(format: "CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, log_id integer, tag_id integer, FOREIGN KEY (log_id) REFERENCES %@(id), FOREIGN KEY (tag_id) REFERENCES %@(id));", indexTable, LoggerDB.logTable, tagTable))
                    if result {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table success", indexTable))
                    }else {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table failed", indexTable))
                    }
                }
            })
        }
    }
    
    @objc private dynamic func createDBInfoTable() {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            self.dbQueue?.inTransaction({ (db, rollback) in
                if !db.tableExists(LoggerDB.dbInfo) {
                    let result = db.executeStatements(String(format: "CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, create_time timestamp, version text, app_version text, app_build_version text, bundle_id text, table_desc text);", LoggerDB.dbInfo))
                    if result {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table success", LoggerDB.dbInfo))
                        do {
                            // 查询表结构, 记录
                            let result = try db.executeQuery("select * from sqlite_master where type = 'table'", values: nil)
                            var resultArr: [[AnyHashable: Any]] = []
                            while result.next() {
                                resultArr.append(result.resultDictionary ?? [:])
                            }
                            result.close()
                            
                            let resultJsonString = NSArray(array: resultArr).yy_modelToJSONString() ?? ""
                            
                            // 存入DB信息
                            try db.executeUpdate(String(format: "INSERT INTO %@ (id, create_time, version, app_version, app_build_version, bundle_id, table_desc) VALUES (%d, %lf, '%@', '%@', '%@', '%@', '%@')", LoggerDB.dbInfo, 1, Date().timeIntervalSince1970, LoggerDB.dbVersion, LoggerDB.basicInfo.app_version ?? "", LoggerDB.basicInfo.app_version_code ?? "", (Bundle.main.infoDictionary?[kCFBundleIdentifierKey as String] as? String ?? "Unknown"), resultJsonString), values: nil)
                        } catch let error {
                            Logger.default.verbose(user: .bo, "insert db info failed, error: ".appending(error.localizedDescription))
                            rollback.pointee = true
                        }
                    }else {
                        Logger.default.verbose(user: .bo, String(format: "create %@ table failed", LoggerDB.dbInfo))
                        rollback.pointee = true
                    }
                }
            })
        }
    }
    
    @objc private dynamic func checkAndFetchLoggerDBQueue() {
        asyncQueue.async { [weak self] in
            guard let self = self else { return }
            let dbPath = self.getLoggerDBPath()
            Logger.default.verbose(user: .bo, String(format: "log storage path: %@", dbPath))
            self.dbQueue = FMDatabaseQueue(path: dbPath)
        }
    }
    
    @objc private dynamic func getLoggerDBPath() -> String {
        let dbPath = NSHomeDirectory().appending("/").appending(LoggerDB.cacheDirectory)
        
        var isDir: ObjCBool = false
        let exist = FileManager.default.fileExists(atPath: dbPath, isDirectory: &isDir)
        if !exist || !isDir.boolValue {
            do {
                try FileManager.default.createDirectory(atPath: dbPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error {
                Logger.default.verbose(user: .bo, "create db path failed, error: ".appending(error.localizedDescription))
            }
        }
        
        return dbPath.appending(LoggerDB.dbName)
    }
    
    @objc static internal dynamic let dbInfo = "db_info"
    
    @objc static internal dynamic let logTable = "log_content"
    
    @objc static internal dynamic let moduleTable = "module_name"
    
    @objc static internal dynamic let serviceTable = "service_name"
    
    @objc static internal dynamic let logModuleIndexTable = "log_module"
    
    @objc static internal dynamic let logServiceIndexTable = "log_service"
    
    @objc static internal dynamic let dbVersion = "1.0.0"
    
    @objc static internal dynamic let dbName = "DuLog.db"
    
    @objc static internal dynamic var expiredPeriod = 60 * 60 * 24 * 14
    
    @objc static internal dynamic let queryLogMaxLimit = 1000
}

