//
//  ConsoleLogger.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/19.
//

import UIKit

internal class ConsoleLogger: NSObject {
    internal static func log(level: String, service: String, module: String, user: LoggerUser, time: Date, fileName: String, functionName: String, lineNumber: Int, _ content: Any) {
        
        var msgContent = content
        if let c = content as? Array<Any> {
            let nsArr = NSArray(array: c)
            msgContent = nsArr
        }else if let c = content as? Dictionary<AnyHashable, Any> {
            let nsDic = NSDictionary(dictionary: c)
            msgContent = nsDic
        }else if let c = content as? Set<AnyHashable> {
            let nsSet = NSSet(set: c)
            msgContent = nsSet
        }else if let c = content as? NSObject, !(c is String) {
            msgContent = c.yy_modelToJSONObject() ?? content
        }
        
        var msg = String.init(format: "%@ [%@] [%@] [%@]", time.timeString(), user.rawValue, level, service)
        if module.count > 0 {
            msg.append(String(format: " [%@]", module))
        }
        
        msg.append(String(format: " [%@] [%@] [line: %d]:", NSString(string: fileName).lastPathComponent, functionName, lineNumber))
        print(msg, msgContent)
    }
}
