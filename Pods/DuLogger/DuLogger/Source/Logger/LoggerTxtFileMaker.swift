//
//  LoggerTxtFileMaker.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/22.
//

import UIKit

/// 从数据库读取log后生成文件
internal class LoggerTxtFileMaker: NSObject {
    
    @objc static private dynamic var cacheDirectory: String = "Library/Caches/com.dewu.Logger/"
    
    @objc internal dynamic var rollBackSize: UInt64 = 1024 * 1024 * 2
    /// overdue period
    internal var period: TimeInterval = 60 * 60 * 24 * 7
    
    @objc internal dynamic var serect: String?
    
    @objc private dynamic var once: Bool = false
    
    @objc private dynamic var length: UInt64 = 0
    private var fileHandler: FileHandle?
    
    @objc private dynamic var noSpaceLeft: Bool = false
    /// global queue
    private static let queue: DispatchQueue = DispatchQueue.init(label: "com.dewu.loggerTxtFileMaker")
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationResignActive), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    /// call when install plugin
    /// - Parameters:
    ///   - cacheDirectory: cacheDirectory
    @objc internal dynamic func setup(cacheDirectory: String, service: String, module: String) {
        LoggerTxtFileMaker.cacheDirectory = cacheDirectory
        // check file
        checkExistFileAndInit(service: service, module: module)
        // check left space
        do {
            let fileInfo = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())
            guard let size = fileInfo[.systemFreeSize] as? UInt64 else {
                return
            }
            // 4G
            let o: UInt64 = 1024
            let f: UInt64 = 4
            // 4G, will not log to fil
            let min: UInt64 = f * o * o * o
            // 1G, will not log last line
            let minLog: UInt64 = o * o * o
            // flat noSpaceLeft
            if size < min {
                // log last line
                if size > minLog {
                    writeToFile(content: "LoggerTxtFileMaker check, no left space on deive, stop write...")
                }
                invalidFileHandler()
                noSpaceLeft = true
                return
            }
        } catch _ {
            
        }
    }
    
    @objc public dynamic func willUninstall() {
        NotificationCenter.default.removeObserver(self)
        invalidFileHandler()
    }
    
    @objc fileprivate dynamic func invalidFileHandler() {
        guard let fh = fileHandler else { return }
        if #available(iOS 13.0, *) {
            _ = try? fh.synchronize()
            _ = try? fh.close()
        } else {
            fh.synchronizeFile()
            fh.closeFile()
        }
        fileHandler = nil
        length = 0
    }
    
    @objc public dynamic func write(_ content: String, service: String, module: String) {
        // check left space
        guard !noSpaceLeft else { return }
        // check is need roll backs
        checkRollBack(service: service, module: module)
        // write
        writeToFile(content: content)
    }
    
    @objc private dynamic func writeToFile(content: String) {
        // to data
        guard let fh = fileHandler, let data = content.data(using: .utf8) else { return }
        // record length
        length += UInt64(data.count)
        if #available(iOS 13.4, *) {
            do {
                try fh.seekToEnd()
                try fh.write(contentsOf: data)
            } catch {
                
            }
        } else {
            // seek end
            fh.seekToEndOfFile()
            fh.write(data)
        }
    }
    
    
    /// 创建文件工具
    /// - Parameter path: 操作路径
    /// - Returns: 文件工具
    private func getNewFileHandler(path: String) -> FileHandle? {
        return FileHandle.init(forWritingAtPath: path)
    }
    
    @objc public dynamic func __allFilePath(service: String, module: String) -> [String] {
        let fs = FileManager.default
        let dir = self.moduleDir(service: service, module: module)
        return allFileName(service: service, module: module)
            .map({Path.join(path: dir, other: $0)})
            .compactMap { (path) -> (String, Date)? in
                if let ct = (try? fs.attributesOfItem(atPath: path))?[.modificationDate] as? Date  {
                    return (path, ct)
                } else {
                    return nil
                }
        }
        .sorted(by: {$1.1.timeIntervalSince1970 < $0.1.timeIntervalSince1970})
        .compactMap({$0.0})
    }
    
    @objc public dynamic func allFilePath(service: String, module: String) -> [String] {
        LoggerTxtFileMaker.queue.sync {
            rollBack(service: service, module: module)
            return __allFilePath(service: service, module: module)
        }
    }
    
    /// get all cache file async
    /// - Parameter result: result
    public func allFilePathAsync(service: String, module: String, result: @escaping([String])->Void) {
        LoggerTxtFileMaker.queue.async { [weak self] in
            guard let self = self else { return }
            self.rollBack(service: service, module: module)
            result(self.__allFilePath(service: service, module: module))
        }
    }
    
    @objc public dynamic func latestFilePath(service: String, module: String, max: Int) -> [String] {
        return allFilePath(service: service, module: module).prefix(max).map({String($0)})
    }
    
    public func latestFilePathAsync(service: String, module: String, max: Int, result: @escaping([String])->Void) {
        allFilePathAsync(service: service, module: module, result: {result($0.prefix(max).map({String($0)}))})
    }
    
    @objc public dynamic func clear(service: String, module: String) {
        allFilePath(service: service, module: module).forEach({_ = try? FileManager.default.removeItem(atPath: $0)})
    }
    
    /// clear all cache file async
    public func clearAsync(service: String, module: String, done: @escaping ()->Void) {
        allFilePathAsync(service: service, module: module, result: {$0.forEach({_ = try? FileManager.default.removeItem(atPath: $0)}); done();})
    }
    
    @objc private dynamic func checkExistFileAndInit(service: String, module: String) {
        guard !once else { return }
        once = true
        let fs = FileManager.default
        __allFilePath(service: service, module: module).forEach { (path) in
            do {
                // file att
                let at = try fs.attributesOfItem(atPath: path)
                // get create date
                if let ct = at[.creationDate] as? Date {
                    // time stamp
                    let ts = Date().timeIntervalSince(ct)
                    // check is overdue
                    if ts > period {
                        try fs.removeItem(atPath: path)
                    }
                }
            } catch {
                
            }
        }
        // get cache log length
        let logPath = logFile(service: service, module: module)
        do {
            if let size = try fs.attributesOfItem(atPath: logPath)[.size] as? UInt64 {
                length = size
            }
        } catch {
            
        }
        fileHandler = getNewFileHandler(path: logPath)
    }
    
    @objc private dynamic func checkRollBack(service: String, module: String) {
        if length > rollBackSize {
            rollBack(service: service, module: module)
        }
    }
    
    @objc private dynamic func rollBack(service: String, module: String) {
        guard length > 0 else { return }
        // invalid current write file handler
        invalidFileHandler()
        
        let fs = FileManager.default
        let logPath = logFile(service: service, module: module)
        // need roll back
        let nextFile = nextFilePath(service: service, module: module)
        func removeTarget() {
            do {
                try fs.removeItem(atPath: nextFile)
            } catch {
                
            }
        }
        removeTarget()
        do {
            try fs.copyItem(atPath: logPath, toPath: nextFile)
        } catch {
            removeTarget()
            do {
                FileManager.default.createFile(atPath: nextFile, contents: nil, attributes: nil)
                let data = try Data.init(contentsOf: URL(fileURLWithPath: logPath))
                try data.write(to: URL(fileURLWithPath: logPath))
            } catch {
                
            }
        }
        length = 0
        fileHandler = getNewFileHandler(path: logPath)
        guard let fh = fileHandler else { return }
        if #available(iOS 13.0, *) {
            _ = try? fh.truncate(atOffset: 0)
            _ = try? fh.synchronize()
        } else {
            fh.truncateFile(atOffset: 0)
            fh.synchronizeFile()
        }
    }
    
    @objc private dynamic func applicationResignActive() {
        if #available(iOS 13.0, *) {
            _ = try? fileHandler?.synchronize()
        } else {
            fileHandler?.synchronizeFile()
        }
    }
    
    @objc private dynamic var r_base_dir: Bool = false
    
    @objc private dynamic func baseDir() -> String {
        let full = Path.join(path: NSHomeDirectory(), other: LoggerTxtFileMaker.cacheDirectory)
        guard !r_base_dir else { return full }
        r_base_dir = true
        Path.dir(full)
        return full
    }
    
    @objc private dynamic var r_module_dir: Bool = false
    
    @objc private dynamic func moduleDir(service: String, module: String) -> String {
        let full = Path.join(path: baseDir(), other: service, module)
        guard !r_module_dir else { return full }
        r_module_dir = true
        Path.dir(full)
        return full
    }
    
    @objc private dynamic var r_log_file: Bool = false
    
    @objc private dynamic func logFile(service: String, module: String) -> String {
        let full = Path.join(path: moduleDir(service: service, module: module), other: "log")
        guard !r_log_file else { return full }
        r_log_file = true
        if !FileManager.default.fileExists(atPath: full) {
            FileManager.default.createFile(atPath: full, contents: nil, attributes: [FileAttributeKey.protectionKey: FileProtectionType.none ] )
        }
        return full
    }
    
    @objc private dynamic func nextFilePath(service: String, module: String) -> String {
        return Path.join(path: moduleDir(service: service, module: module), other: Date().timeString())
    }
    
    @objc private dynamic func allFileName(service: String, module: String) -> [String] {
        do {
            return try FileManager.default.subpathsOfDirectory(atPath: moduleDir(service: service, module: module)).filter({$0 != "log" && !$0.hasPrefix(".") && !$0.hasPrefix("_")})
        } catch {
            return []
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

private struct Path {
    static func dir(_ path: String) {
        var isDir: ObjCBool = false
        let exist = FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
        if exist && isDir.boolValue {
            return
        }
        if exist {
            _ = try? FileManager.default.removeItem(atPath: path)
        }
        _ = try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
    }
    static func join(path: String, other o: String...) -> String {
        func rl(_ p: String)->String {
            guard p.hasSuffix("/") else { return p }
            return String(p.dropLast())
        }
        func rf(_ p: String)->String {
            guard p.hasPrefix("/") else { return p }
            return String(p.dropFirst())
        }
        return o.filter({!$0.isEmpty}).reduce(path) { rl($0).appendingFormat("/%@", rf($1)) }
    }
}


