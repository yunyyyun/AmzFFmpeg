//
//  LoggerDB+SQL.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/23.
//

import UIKit

internal extension LoggerDB {
    /// 拼接存储log的sql
    func getInsertLogContentSQL(level: Int, user: LoggerUser, time: Date, fileName: String, functionName: String, lineNumber: Int, _ content: Any) -> String? {
        var msgContent: String = ""
        if let c = content as? Array<Any> {
            let nsArr = NSArray(array: c)
            msgContent = nsArr.yy_modelToJSONString() ?? ""
        }else if let c = content as? Dictionary<AnyHashable, Any> {
            let nsDic = NSDictionary(dictionary: c)
            msgContent = nsDic.yy_modelToJSONString() ?? ""
        }else if let c = content as? Set<AnyHashable> {
            let nsSet = NSSet(set: c)
            msgContent = nsSet.yy_modelToJSONString() ?? ""
        }else if let c = content as? NSObject, !(c is String) {
            msgContent = c.yy_modelToJSONString() ?? ""
        }else if let c = content as? String {
            msgContent = c
        }else {
            Logger.default.verbose(user: .bo, "log content isn't a string or can't transform to json string, can't write to disk, content: \(content)")
            return nil
        }
        
        let sql = String(format: "INSERT INTO %@ (log_time, file_name, function_name, line, level, content, app_id, app_name, app_version, app_version_code, device_ip, device_model, prodevice_name, device_os, device_os_version, device_brand, device_uuid, device_oaid, device_imei, visit_mode, userid, is_root, is_simulator, is_proxy, location, lat, lng, launch_id, logger_user, upload_status, upload_time) VALUES (%lf, '%@', '%@', %d, %d, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', %d, '%@', %d, %d, %d, '%@', '%@', '%@', '%@', '%@', 0, 0)", LoggerDB.logTable, time.timeIntervalSince1970, fileName, functionName, lineNumber, level, msgContent, LoggerDB.basicInfo.app_id ?? "", LoggerDB.basicInfo.app_name ?? "", LoggerDB.basicInfo.app_version ?? "", LoggerDB.basicInfo.app_version_code ?? "", LoggerDB.basicInfo.device_ip ?? "", LoggerDB.basicInfo.device_model ?? "", LoggerDB.basicInfo.Prodevice_name ?? "", LoggerDB.basicInfo.device_os ?? "", LoggerDB.basicInfo.device_os_version ?? "", LoggerDB.basicInfo.device_brand ?? "", LoggerDB.basicInfo.device_uuid ?? "", LoggerDB.basicInfo.device_oaid ?? "", LoggerDB.basicInfo.device_imei ?? "", LoggerDB.basicInfo.visit_mode, LoggerDB.basicInfo.userid ?? "", LoggerDB.basicInfo.is_root, LoggerDB.basicInfo.is_simulator, LoggerDB.basicInfo.is_proxy, LoggerDB.basicInfo.location ?? "", LoggerDB.basicInfo.lat ?? "", LoggerDB.basicInfo.lng ?? "", LoggerDB.basicInfo.launch_id ?? "", user.rawValue)
        return sql
    }
    
    @objc dynamic func getQueryLogContentSQL(_ service: String, _ module: String) -> String {
        var sql = String(format: "SELECT lc.*, IFNULL(GROUP_CONCAT(DISTINCT(sn.name)),'unknown') AS services_name, IFNULL(GROUP_CONCAT(DISTINCT(mn.name)),'unknown') AS modules_name FROM %@ AS lc LEFT JOIN %@ AS lm LEFT JOIN %@ AS mn ON lc.id = lm.log_id AND lm.tag_id = mn.id LEFT JOIN %@ AS ls LEFT JOIN %@ AS sn ON lc.id = ls.log_id AND ls.tag_id = sn.id", LoggerDB.logTable, LoggerDB.logModuleIndexTable, LoggerDB.moduleTable, LoggerDB.logServiceIndexTable, LoggerDB.serviceTable)
        if !service.isEmpty {
            sql.append(String(format: " WHERE sn.name = '%@'", service))
        }
        if !module.isEmpty {
            if sql.contains("WHERE") {
                sql.append(String(format: " AND mn.name = '%@'", module))
            }else {
                sql.append(String(format: " WHERE mn.name = '%@'", module))
            }
        }
        if sql.contains("WHERE") {
            sql.append(" AND lc.upload_status = 0")
        }else {
            sql.append(" WHERE lc.upload_status = 0")
        }
        
        sql.append(String(format: " GROUP BY lc.id LIMIT %ld", LoggerDB.queryLogMaxLimit))
        return sql
    }
    
    @objc dynamic func getQueryLogCountSQL(_ service: String, _ module: String) -> String {
        var sql = String(format: "SELECT COUNT(DISTINCT(lc.id)) AS 'count' FROM %@ AS lc LEFT JOIN %@ AS lm LEFT JOIN %@ AS mn ON lc.id = lm.log_id AND lm.tag_id = mn.id LEFT JOIN %@ AS ls LEFT JOIN %@ AS sn ON lc.id = ls.log_id AND ls.tag_id = sn.id WHERE sn.name = 'app' AND mn.name = 'DuLogger_Example' AND lc.upload_status = 0", LoggerDB.logTable, LoggerDB.logModuleIndexTable, LoggerDB.moduleTable, LoggerDB.logServiceIndexTable, LoggerDB.serviceTable)
        if !service.isEmpty {
            sql.append(String(format: " WHERE sn.name = '%@'", service))
        }
        if !module.isEmpty {
            if sql.contains("WHERE") {
                sql.append(String(format: " AND mn.name = '%@'", module))
            }else {
                sql.append(String(format: " WHERE mn.name = '%@'", module))
            }
        }
        if sql.contains("WHERE") {
            sql.append(" AND lc.upload_status = 0")
        }else {
            sql.append(" WHERE lc.upload_status = 0")
        }
        return sql
    }
    
    @objc dynamic func getUpdateUploadStatusSQL(_ service: String, _ module: String, targetStatus: Int, currentStatus: Int, lastID: Int) -> String {
        var sql = String(format: "UPDATE %@ SET upload_status = %ld, upload_time = %lf", LoggerDB.logTable, targetStatus, Date().timeIntervalSince1970)
        if !service.isEmpty {
            sql.append(String(format: " WHERE id IN (SELECT log_id FROM %@ WHERE tag_id IN (SELECT id FROM %@ WHERE name = '%@' GROUP BY id) GROUP BY id)", LoggerDB.logServiceIndexTable, LoggerDB.serviceTable, service))
        }
        if !module.isEmpty {
            if sql.contains("WHERE") {
                sql.append(String(format: " AND id IN (SELECT log_id FROM %@ WHERE tag_id IN (SELECT id FROM %@ WHERE name = '%@' GROUP BY id) GROUP BY id)", LoggerDB.logModuleIndexTable, LoggerDB.moduleTable, module))
            }else {
                sql.append(String(format: " WHERE id IN (SELECT log_id FROM %@ WHERE tag_id IN (SELECT id FROM %@ WHERE name = '%@' GROUP BY id) GROUP BY id)", LoggerDB.logModuleIndexTable, LoggerDB.moduleTable, module))
            }
        }
        if sql.contains("WHERE") {
            sql.append(String(format: " AND upload_status = %ld AND log_content.id <= %ld", currentStatus, lastID))
        }else {
            sql.append(String(format: " WHERE upload_status = %ld AND log_content.id <= %ld", currentStatus, lastID))
        }
        
        return sql
    }
    
    /// 拼接存储log tag绑定sql
    func getInsertLogTagIndexSQL(_ indexTable: String, _ logID: Int64, _ tagID: Int64) -> String {
        return String(format: "INSERT INTO %@ (log_id, tag_id) VALUES (%ld, %ld)", indexTable, logID, tagID)
    }
    
    @objc dynamic func getInsertTagSQL(_ table: String, _ tag: String) -> String {
        return String(format: "INSERT OR IGNORE INTO %@ (name) VALUES ('%@')", table, tag)
    }
}
