//
//  LoggerBasicInfo.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/20.
//

import UIKit

@objc public class LoggerBasicInfo: NSObject {
    @objc dynamic public var app_id: String?
    @objc dynamic public var app_name: String?
    @objc dynamic public var app_version: String?
    @objc dynamic public var app_version_code: String?
    @objc dynamic public var android_channel: String?
    @objc dynamic public var device_ip: String?
    @objc dynamic public var device_model: String?
    @objc dynamic public var Prodevice_name: String?
    @objc dynamic public var device_os: String?
    @objc dynamic public var device_os_version: String?
    @objc dynamic public var device_brand: String?
    @objc dynamic public var device_uuid: String?
    @objc dynamic public var device_oaid: String?
    @objc dynamic public var device_imei: String?
    @objc dynamic public var visit_mode: Bool = false
    @objc dynamic public var userid: String?
    @objc dynamic public var is_root: Bool = false
    @objc dynamic public var is_simulator: Bool = false
    @objc dynamic public var is_proxy: Bool = false
    @objc dynamic public var location: String?
    @objc dynamic public var lat: String?
    @objc dynamic public var lng: String?
    @objc dynamic public var launch_id: String?
}
