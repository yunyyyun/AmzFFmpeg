//
//  Date+Logger.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/19.
//

import Foundation


internal extension Date {
    func timeString() -> String {
        let formatter = DateFormatter()
        let locale = Locale(identifier: "zh")
        formatter.locale = locale
        formatter.dateStyle = .short
        formatter.timeStyle = .long
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss:SSSSSS"
        return formatter.string(from: self)
    }
}
