//
//  FileLogger.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/19.
//

import UIKit

internal class FileLogger: NSObject {
    
    @objc internal static dynamic let `default` = FileLogger()
    
    @objc private static dynamic var cacheDirectory: String = "Library/Caches/com.dewu.Logger/"
    
    @objc private dynamic var uploadRecord: [String: Int] = [:]
    
    @objc private dynamic var txtFileMaker: [String: LoggerTxtFileMaker] = [:]
    
    @objc private static dynamic var isSetup = false
        
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationResignActive), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    @objc internal static dynamic func setup(cacheDirectory: String, basicInfo: LoggerBasicInfo) {
        FileLogger.cacheDirectory = cacheDirectory
        if checkLeftSpace() {
            FileLogger.isSetup = true
            LoggerDB.default.setup(cacheDirectory: cacheDirectory, basicInfo: basicInfo)
        }
    }
    
    internal func log(level: Int, service: String, module: String, user: LoggerUser, time: Date, fileName: String, functionName: String, lineNumber: Int, _ content: Any) {
        if FileLogger.isSetup {
            LoggerDB.default.insertLog(level: level, service: service, module: module, user: user, time: time, fileName: fileName, functionName: functionName, lineNumber: lineNumber, content)
        }
    }
    
    @objc internal dynamic func set(expiredTime: Int) {
        LoggerDB.default.set(expiredTime: expiredTime)
    }
    
    /// 获取某个module或者某个service的log日志文件路径, 两个都传""为全工程log, 不建议使用两个""
    internal func getLogPathToUpload(_ service: String, module: String, completion: @escaping ([String]) -> Void) {
        var serviceKey = "all"
        var moduleKey = "all"
        if !service.isEmpty {
            serviceKey = service
            let allServiceKey = service.appending("_all")
            guard uploadRecord[allServiceKey] == nil else {
                completion([])
                Logger.default.warn(user: .bo, "当前模块log日志正在上传中, 结束上次上传之后再调用")
                return
            }
        }
        if !module.isEmpty {
            moduleKey = module
            let allModuleKey = "all_".appending(module)
            guard uploadRecord[allModuleKey] == nil else {
                completion([])
                Logger.default.warn(user: .bo, "当前模块log日志正在上传中, 结束上次上传之后再调用")
                return
            }
        }
        let key = serviceKey.appending("_").appending(moduleKey)
        guard uploadRecord[key] == nil else {
            completion([])
            Logger.default.warn(user: .bo, "当前模块log日志正在上传中, 结束上次上传之后再调用")
            return
        }
        
        let maker = LoggerTxtFileMaker()
        maker.setup(cacheDirectory: FileLogger.cacheDirectory, service: service, module: module)
        txtFileMaker[key] = maker
        
        LoggerDB.default.queryLogFrom(service, module) { [weak self] (logContent, isComplete, lastID) in
            guard let self = self else { return }
            maker.write(logContent, service: service, module: module)
            if isComplete {
                let allPath = maker.allFilePath(service: service, module: module)
                if lastID != 0 {
                    self.uploadRecord[key] = lastID
                }
                completion(allPath)
            }
        }
    }
    
    @objc internal dynamic func uploadLogFileSuccess(_ service: String, _ module: String) {
        var serviceKey = "all"
        var moduleKey = "all"
        if !service.isEmpty {
            serviceKey = service
        }
        if !module.isEmpty {
            moduleKey = module
        }
        let key = serviceKey.appending("_").appending(moduleKey)
        guard let lastID = uploadRecord[key] else {
            return
        }
        LoggerDB.default.updateLogStatusToUploadSuccess(service, module, lastID: lastID)
        uploadRecord.removeValue(forKey: key)
        txtFileMaker.removeValue(forKey: key)
    }
    
    @objc internal dynamic func uploadLogFileFailed(_ service: String, _ module: String) {
        var serviceKey = "all"
        var moduleKey = "all"
        if !service.isEmpty {
            serviceKey = service
        }
        if !module.isEmpty {
            moduleKey = module
        }
        let key = serviceKey.appending("_").appending(moduleKey)
        guard let lastID = uploadRecord[key] else {
            return
        }
        LoggerDB.default.updateLogStatusToUploadFailed(service, module, lastID: lastID)
        uploadRecord.removeValue(forKey: key)
        txtFileMaker.removeValue(forKey: key)
    }
    
    @objc private dynamic static func checkLeftSpace() -> Bool {
        do {
            let fileInfo = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())
            guard let size = fileInfo[.systemFreeSize] as? UInt64 else {
                return false
            }
            // 4G
            let o: UInt64 = 1024
            let f: UInt64 = 4
            // 4G, will not log to fil
            let min: UInt64 = f * o * o * o
            // 1G, will not log last line
            let minLog: UInt64 = o * o * o
            // flat noSpaceLeft
            if size < min {
                // log last line
                if size > minLog {
                    // TODO: no left space on deive, stop write...
                    return false
                }
                return false
            }
            return true
        } catch _ {
            return false
        }
    }
    
    // MARK: Notification
    
    @objc private dynamic func applicationResignActive() {
        LoggerDB.default.close()
    }
}
