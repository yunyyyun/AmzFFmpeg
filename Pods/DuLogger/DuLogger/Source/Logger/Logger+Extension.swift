//
//  Logger+Extension.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/19.
//

import Foundation

public extension Logger {
    
    @objc static dynamic let `default` = Logger(service: "app")
    
    @objc static dynamic let community = Logger(service: "community")
    
    @objc static dynamic let transaction = Logger(service: "transaction")
    
    @objc static dynamic let identify = Logger(service: "identify")
    
    @objc static dynamic let grow = Logger(service: "grow")
    
    @objc static dynamic let live = Logger(service: "live")
}
