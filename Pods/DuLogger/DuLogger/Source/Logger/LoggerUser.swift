//
//  LoggerUser.swift
//  DuLogger
//
//  Created by 景鹏博 on 2020/11/19.
//

import Foundation

public enum LoggerUser: String {
    case none
    case casa
    case king
    case mengwei
    case bo
    case mengyun
    case zhangyu
    case zengfanyi
    case zhangyue
    case daxiong
    case zhangliang
    case gx
    case wangyuetong
    case zhuyuankai
    case penglei
    case buxiaohu
    case zhangcheng
    case jiguanjie
    case xiaohua
    case rose
    case guojintao
    case yulong
    case sleve
    case wangding
    case yangyang
    case yuxiaoshuai
    case yuyi
    case honggang
    case tommy
    case zhaohaiting
    case jin
    case fupeng
    case zhangtao
    case dongnan
    case chengxianghe
    case zhanglongqing
    case abao
    case xiaoxin
    case zhendakang
    case shile
    case matrix
    case zhouyihua
    case orange
    case sunjigang
}
