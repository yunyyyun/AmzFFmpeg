# DuLogger

iOS端日志打印库, 支持大量log瞬时写入, 本地存储, 定时清理, 获取本地日志文件.

<!-- more -->



**我可以 :**

- 控制台日志打印
- 本地磁盘日志存储
- 本地日志过期自动清除
- swift 集合打印为熟悉的 OC 形式
- 根据 ***日志等级*** 屏蔽, 开启控制台输出, 本地写入
- 根据 ***打印人*** 屏蔽, 开启控制台输出, 本地写入
- 根据 ***Seriver 名*** 屏蔽, 开启控制台输出, 本地写入
- 根据 ***Module 名*** 屏蔽, 开启控制台输出, 本地写入
- 根据 ***文件名*** 屏蔽, 开启控制台输出, 本地写入
- 短时间 ***50 w*** 条日志写入无压力
- ***50 w*** 条日志文件输出 <0.9s

## 输出样式

```
2020-11-23 22:59:04:788000 [bo] [Verbose] [app] [DuLogger_Example] [ViewController.swift] [viewDidLoad()] [line: 65]: 
```
| 时间 | 用户 | 日志等级 | service | module | 文件名 | 函数名 | 打印行数 | 打印内容 |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
2020-11-23 22:59:04:788000 | [bo] | [Verbose] | [app] | [DuLogger_Example] | [ViewController.swift] | [viewDidLoad()] | [line: 65] | 

## 安装方式

```
pod "DuLogger"
```

## 使用方式

### 初始化

#### 只使用控制台打印

直接引入就可使用

#### 使用本地存储

```
let basicInfo = setupBasicInfo()
Logger.setup(basicInfo, nil)

/// 获取基本信息
private func setupBasicInfo() -> LoggerBasicInfo {
    let basicInfo = LoggerBasicInfo()
    basicInfo.app_id = CTMediator.sharedInstance().DuContext_App_Id()
    basicInfo.app_name = (Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String) ?? ""
    basicInfo.app_version = CTMediator.sharedInstance().DuContext_App_Version()
    basicInfo.app_version_code = CTMediator.sharedInstance().DuContext_App_BuildVersion()
    basicInfo.device_ip = CTMediator.sharedInstance().DuContext_Networking_IpAddress()
    basicInfo.device_model = CTMediator.sharedInstance().DuContext_Device_ModelName()
    basicInfo.Prodevice_name = CTMediator.sharedInstance().DuContext_Device_Platform()
    basicInfo.device_os = "iOS"
    basicInfo.device_os_version = UIDevice.current.systemVersion
    basicInfo.device_brand = "Apple"
    basicInfo.device_uuid = CTMediator.sharedInstance().DuContext_Device_UUID()
    basicInfo.device_oaid = ""
    basicInfo.device_imei = CTMediator.sharedInstance().DuContext_App_ShuMeiID()
    basicInfo.visit_mode = (CTMediator.sharedInstance().DuContext_User_Status() != 0)
    basicInfo.userid = CTMediator.sharedInstance().DuContext_User_UserIdString()
    basicInfo.is_root = CTMediator.sharedInstance().DuContext_Device_isBroken()
    basicInfo.is_simulator = CTMediator.sharedInstance().DuContext_Device_IsSimulator()
    basicInfo.is_proxy = CTMediator.sharedInstance().DuContext_Device_IsProxy()
    basicInfo.location = CTMediator.sharedInstance().DuContext_Location_City()
    basicInfo.lat = CTMediator.sharedInstance().DuContext_Location_Latitude()
    basicInfo.lng = CTMediator.sharedInstance().DuContext_Location_Longitude()
    return basicInfo
}

```

### 调用方式

目前分六个 service, 分别为`app`, `community`, `transaction`, `identify`, `grow`, `live`, 对应App中基础组件, 社区, 交易, 鉴别, 增长, 直播六块.

```
// app
Logger.default.verbose(user: .bo, "")
// 社区
Logger.community.verbose(user: .bo, "")
// 交易
Logger.transaction.verbose(user: .bo, "")
// 鉴别
Logger.identify.verbose(user: .bo, "")
// 增长
Logger.grow.verbose(user: .bo, "")
// 直播
Logger.live.verbose(user: .bo, "")
```

### 获取本地日志文件

每个 service, module, 同时只能在一处获取日志, 获取日志路径后, 一定要在使用完路径后调用`updateLogStatusToUploadSuccess(_, _, lastID: )` 或者 `updateLogStatusToUploadFailed(_, _, lastID: )`方法通知Logger使用完毕并更新状态, 否则会出现本次并未上传成功, 但是下一次已经获取不到对应log日志的现象

获取直播 service 下, 所有日志

```
Logger.live.getLogPathToUpload("") { (paths) in
    // upload
    <#code#>
    Logger.default.uploadLogFileSuccess("DuLivePusher")
    // or
    Logger.default.uploadLogFileFailed("DuLivePusher")
}
```

获取直播 service 下, DuLivePusher module的日志

```
Logger.live.getLogPathToUpload("DuLivePusher") { (paths) in
    // upload
    <#code#>
    Logger.default.uploadLogFileSuccess("DuLivePusher")
    // or
    Logger.default.uploadLogFileFailed("DuLivePusher")
}
```

获取某个service下, 某个module的日志. <font color=#DDDDDD>两个都传"", 则获取沙盒内所有日志(不建议使用, 可能会有大量 log 日志)</font>

```
Logger.getLogPathToUpload("service name", module: "module name") { (paths) in
    <#code#>
}
```

### 设置日志过期时间

```
// 默认 14 天, 秒为单位
Logger.set(expiredTime: 60 * 60 * 24 * 14)
```

目前日志用户和service是最初写的, 少了哪个大家直接补充一下, 或者名字不喜欢就直接修改一下就好了