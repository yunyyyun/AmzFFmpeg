//
//  BPage_Extension.swift
//
//  Created by casa on 2020/2/21.
//  Copyright © 2020 casa. All rights reserved.
//

import CTMediator

fileprivate let ModuleName = "DULeanCloudSDK"

public extension CTMediator {
	
	func DULeanCloudSDK_logout() {
		let params:[AnyHashable:Any] = [
			kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
		]
		performTarget(ModuleName, action: "logout", params: params, shouldCacheTarget: false)
	}
	
	func DULeanCloudSDK_didLogin() {
		let params:[AnyHashable:Any] = [
			kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
		]
		performTarget(ModuleName, action: "didLogin", params: params, shouldCacheTarget: false)
	}
	
	func DULeanCloudSDK_didChangeAPIEnv() {
		let params:[AnyHashable:Any] = [
			kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
		]
		performTarget(ModuleName, action: "didChangeAPIEnv", params: params, shouldCacheTarget: false)
	}
	
	func DULeanCloudSDK_launch() {
		let params:[AnyHashable:Any] = [
			kCTMediatorParamsKeySwiftTargetModuleName:ModuleName
		]
		performTarget(ModuleName, action: "launch", params: params, shouldCacheTarget: false)
	}
}
