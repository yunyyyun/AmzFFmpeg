//
//  DUCipher.swift
//  DuCipher
//
//  Created by Todd Cheng on 2020/4/27.
//  Copyright © 2020 DuApp. All rights reserved.
//

import Foundation
import CryptoSwift

public class DUCipher {
    private init() { }
    public static let shared = DUCipher()
    
    private let steamBufferSize = 512
    
    /// 使用duapp项目约定的key进行AES加密一段文字.
    ///
    /// - Parameter source: 原始文字
    /// - Returns: 加密完成后的文字
    public func du_aesEncryptor(source: String?) -> String? {
        guard let source = source?.bytes else { return nil }
        guard let retval = aesEncryptor(source, aesKey: "b46afc89e13025d7", padding: .pkcs5)?.toBase64() else { return nil }
        return retval
    }
    
    /// AES加密
    ///
    /// - Parameters:
    ///   - data: 待加密的数据   String.bytes  Data.bytes
    ///   - aesKey: aes key
    ///   - blockMode: blockMode 默认ECB
    ///   - padding: padding 默认pkcs7
    /// - Returns: 返回加密后的数据
    public func aesEncryptor(_ data: [UInt8], aesKey: String, blockMode: BlockMode = ECB(), padding: Padding = .pkcs7) -> [UInt8]? {
        do {
            let aes = try AES(key: aesKey.bytes, blockMode: blockMode, padding: padding)
            let ciphertext = try aes.encrypt(data)
            return ciphertext
        } catch {
            return nil
        }
    }
    
    /// AES解密
    ///
    /// - Parameters:
    ///   - data: 待解密的数据
    ///   - aesKey: aes key
    ///   - blockMode: blockMode 默认ECB
    ///   - padding: padding 默认pkcs7
    /// - Returns: 返回解密后的数据
    public func aesDecryptor(_ data: [UInt8], aesKey: String, blockMode: BlockMode = ECB(), padding: Padding = .pkcs7) -> [UInt8]? {
        do {
            let aes = try AES(key: aesKey.bytes, blockMode: blockMode, padding: padding)
            let ciphertext = try aes.decrypt(data)
            return ciphertext
        } catch {
            return nil
        }
    }
    
    /// AES加密
    ///
    /// - Parameters:
    ///   - data: 待加密的数据
    ///   - aesKey: aes key
    ///   - iv: iv
    /// - Returns: 返回加密后的数据
    public func aesEncryptor(_ data: [UInt8], aesKey: String, iv: String) -> [UInt8]? {
        do {
            let aes = try AES(key: aesKey, iv: iv)
            let ciphertext = try aes.encrypt(data)
            return ciphertext
        } catch {
            return nil
        }
    }
    
    /// AES解密
    ///
    /// - Parameters:
    ///   - data: 待解密的数据
    ///   - aesKey: aes key
    ///   - iv: iv
    /// - Returns: 返回解密后的数据
    public func aesDecryptor(_ data: [UInt8], aesKey: String, iv: String) -> [UInt8]? {
        do {
            let aes = try AES(key: aesKey, iv: iv)
            let ciphertext = try aes.decrypt(data)
            return ciphertext
        } catch {
            return nil
        }
    }
    
    // MARK: 数据流加解密
    // write until all is written
    private func writeTo(stream: OutputStream, bytes: Array<UInt8>) {
        var writtenCount = 0
        while stream.hasSpaceAvailable && writtenCount < bytes.count {
            writtenCount += stream.write(bytes, maxLength: bytes.count)
        }
    }
    
    /// AES 数据流加密
    ///
    /// - Parameters:
    ///   - data: 待加密数据
    ///   - aesKey: aesKey
    ///   - blockMode: blockMode 默认ECB
    ///   - padding: padding 默认pkcs7
    /// - Returns: 返回加密后的数据
    public func aesSteamEncryptor(_ data: Data, aesKey: String, blockMode: BlockMode = ECB(), padding: Padding = .pkcs7) -> Data? {
        do {
            let aes = try AES(key: aesKey.bytes, blockMode: blockMode, padding: padding) // key: "bb5186017a74213f"
            var encryptor = try aes.makeEncryptor()
            
            // prepare streams
            let inputStream = InputStream(data: data)
            let outputStream = OutputStream(toMemory: ())
            inputStream.open()
            outputStream.open()
            
            var buffer = Array<UInt8>(repeating: 0, count: steamBufferSize)
            
            // encrypt input stream data and write encrypted result to output stream
            while inputStream.hasBytesAvailable {
                let readCount = inputStream.read(&buffer, maxLength: buffer.count)
                if readCount > 0 {
                    try encryptor.update(withBytes: buffer[0 ..< readCount]) { bytes in
                        writeTo(stream: outputStream, bytes: bytes)
                    }
                }
            }
            
            // finalize encryption
            try encryptor.finish { bytes in
                writeTo(stream: outputStream, bytes: bytes)
            }
            
            // print result
            if let ciphertext = outputStream.property(forKey: Stream.PropertyKey(rawValue: Stream.PropertyKey.dataWrittenToMemoryStreamKey.rawValue)) as? Data {
                return ciphertext
            }
            
        } catch let error {
            print(error)
            return nil
        }
        return nil
    }
    
    /// AES 数据流解密
    ///
    /// - Parameters:
    ///   - data: 待解密数据
    ///   - aesKey: aesKey
    ///   - blockMode: blockMode 默认ECB
    ///   - padding: padding 默认pkcs7
    /// - Returns: 返回解密后的数据
    public func aesSteamDecryptor(_ data: Data, aesKey: String, blockMode: BlockMode = ECB(), padding: Padding = .pkcs7) -> Data? {
        do {
            
            let aes = try AES(key: aesKey.bytes, blockMode: blockMode, padding: padding)
            var decryptor = try aes.makeDecryptor()
            
            // prepare streams
            let inputStream = InputStream(data: data)
            let outputStream = OutputStream(toMemory: ())
            inputStream.open()
            outputStream.open()
            
            var buffer = Array<UInt8>(repeating: 0, count: steamBufferSize)
            
            // encrypt input stream data and write encrypted result to output stream
            while inputStream.hasBytesAvailable {
                let readCount = inputStream.read(&buffer, maxLength: buffer.count)
                if readCount > 0 {
                    try decryptor.update(withBytes: buffer[0 ..< readCount]) { bytes in
                        writeTo(stream: outputStream, bytes: bytes)
                    }
                }
            }
            
            // finalize encryption
            try decryptor.finish { bytes in
                writeTo(stream: outputStream, bytes: bytes)
            }
            
            // print result
            if let ciphertext = outputStream.property(forKey: Stream.PropertyKey(rawValue: Stream.PropertyKey.dataWrittenToMemoryStreamKey.rawValue)) as? Data {
                return ciphertext
            }
            
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
        return nil
    }
}

