//
//  VideosFileManager.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideosFileManager : NSObject

+(BOOL)creatDir:(NSString *)path;
+(BOOL)creatFile:(NSString*)filePath;
+(NSData*)readFileData:(NSString *)path;
+(BOOL)writeToFile:(NSString*)filePath contents:(NSData *)data;
+(NSArray*)getFileList:(NSString*)path;
+(NSDictionary*)getFileLevelList:(NSString*)path;
+(NSArray*)getAllFileList:(NSString*)path;

@end

NS_ASSUME_NONNULL_END
