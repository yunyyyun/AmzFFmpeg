//
//  AddFileViewController.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddFileViewController : UIViewController

@property (strong, nonatomic) NSString *rootPath;

@end

NS_ASSUME_NONNULL_END
