//
//  AddFileViewController.m
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/30.
//

#import "AddFileViewController.h"
#import "VideosFileManager.h"

@interface AddFileViewController ()

@property (weak, nonatomic) IBOutlet UITextView *urlTextView;
@property (weak, nonatomic) IBOutlet UITextView *fileNameTextView;

@end

@implementation AddFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    [_urlTextView becomeFirstResponder];
}

- (IBAction)onSave:(id)sender {
    NSString *url = _urlTextView.text;
    NSString *fileName = [_fileNameTextView.text stringByAppendingString: @".json"];
    
    if (url.length < 1 || fileName.length < 1 || _rootPath.length < 1) {
        return;
    }
    
    NSString *abPath = [_rootPath stringByAppendingString: fileName];
    [VideosFileManager writeToFile: abPath contents: [url dataUsingEncoding: NSUTF8StringEncoding]];
    
    [self.navigationController popViewControllerAnimated: true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
