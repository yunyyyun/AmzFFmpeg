//
//  MR0x32ViewController.m
//  FFmpegTutorial-iOS
//
//  Created by qianlongxu on 2020/8/4.
//  Copyright © 2020 Matt Reach's Awesome FFmpeg Tutotial. All rights reserved.
//
//
#import <UIKit/UIKit.h>
#import "Define.h"
#import "MR0x32ViewController.h"
#import "MRRWeakProxy.h"
#import <GLKit/GLKit.h>
#import "MR0x32VideoRenderer.h"
#import "MR0x32AudioRenderer.h"

@interface MR0x32ViewController ()<UITextViewDelegate,FFPlayer0x32Delegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (assign, nonatomic) NSInteger ignoreScrollBottom;
@property (weak, nonatomic) NSTimer *timer;


@property (weak, nonatomic) IBOutlet MR0x32VideoRenderer *renderView;
@property (nonatomic, strong) MR0x32AudioRenderer *audioRender;
@property (nonatomic, assign) BOOL started;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *durationLb;

@end

@implementation MR0x32ViewController

- (void)dealloc
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    if (self.player) {
        [self.player stop];
        self.player = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"😄😄😄";
    [self.indicatorView startAnimating];
    self.textView.delegate = self;
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    self.renderView.contentMode = UIViewContentModeScaleAspectFit;
    
    FFPlayer0x32 *player = [[FFPlayer0x32 alloc] init];
    // player.contentPath = @"http://116.128.128.147/vmtt.tc.qq.com/1098_0c02d6322fd6e7604ca2cd29d1e7debb.f0.mp4\?vkey\=DE2658E7C366EE9420AF0C1EAE9C4D56E859410578F7C9330D049E2ED65804C6E3E83B63440AAD9EC9BF014A4B98732F9F00F3B6DE9197F4774E4F04F91B52FB100B18A3F645856A81724B97585390CE2D341B6FEA5F1AA5";
    
//    player.contentPath = @"https://pull-stream-cn.poizon.com/dewuApp/release-19525347-1607935995.flv?auth_key=1608022395-0-0-c78829ada4a83fa433a73057b60c6c7e";
//
//   player.contentPath = @"https://ggkkmuup9wuugp6ep8d.exp.bcevod.com/mda-km671xd58s1yy16y/mda-km671xd58s1yy16y.mp4";
//
//    player.contentPath = @"https://pull-stream-cn.poizon.com/dewuApp/release-7573694-1607999256.m3u8?auth_key=1608085656-0-0-8adca559493cfe919bc5d9859f385ed3&aliyunols=on&lhs_start_unix_s_0=1608000417";
//
//    player.contentPath = @"https://pull-stream-cn.poizon.com/dewuApp/release-125389240-1608012959.m3u8?auth_key=1608099359-0-0-3d0575a697cda2de30d72ffb675ce607&aliyunols=on&lhs_start_unix_s_0=1608013134";
//
//    player.contentPath = @"https://pull-stream-cn.poizon.com/dewuApp/release-1514970982-1608010319.flv?auth_key=1608096719-0-0-87897ba1bfc8680c9217eb8895c834d0";
    
    __weakSelf__
    [player onError:^{
        __strongSelf__
        [self.indicatorView stopAnimating];
        self.textView.text = [self.player.error localizedDescription];
        self.player = nil;
        [self.timer invalidate];
        self.timer = nil;
    }];
    
    [player onVideoEnds:^{
        __strongSelf__
        self.textView.text = @"Video Ends.";
        //fix position not end.
        [self updatePlayedTime];
        [self.player stop];
        self.player = nil;
        [self.timer invalidate];
        self.timer = nil;
    }];
    player.supportedPixelFormats  = MR_PIX_FMT_MASK_NV12;
    player.supportedSampleFormats = MR_SAMPLE_FMT_MASK_AUTO;
//    for test fmt.
    player.supportedSampleFormats = MR_SAMPLE_FMT_MASK_S16 | MR_SAMPLE_FMT_MASK_FLT;
    player.supportedSampleFormats = MR_SAMPLE_FMT_MASK_S16P;
    player.supportedSampleFormats = MR_SAMPLE_FMT_MASK_FLTP;
    
    //设置采样率，如果播放之前知道音频的采样率，可以设置成实际的值，可避免播放器内部转换！
    int sampleRate = [MR0x32AudioRenderer setPreferredSampleRate:44100];
    player.supportedSampleRate = sampleRate;
    
    player.delegate = self;
    [player prepareToPlay];
    [player readPacket];
    self.player = player;
    
    MRRWeakProxy *weakProxy = [MRRWeakProxy weakProxyWithTarget:self];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:weakProxy selector:@selector(onTimer:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.timer = timer;
}

- (void)playAudio
{
    [self.audioRender paly];
}

- (void)reveiveFrameToRenderer:(CVPixelBufferRef)img
{
    NSLog(@"reveiveFrameToRenderer");
    CVPixelBufferRetain(img);
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.renderView displayPixelBuffer:img];
        CVPixelBufferRelease(img);
        
        if (!self.started) {
            [self playAudio];
            self.started = true;
        }
    });
}

- (void)onInitAudioRender:(MRSampleFormat)fmt
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self setupAudioRender:fmt];
    });
}

- (void)onDurationUpdate:(long)du
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self updatePlayedTime];
    });
}

- (NSString *)formatToTime:(long)du
{
    long h = du / 3600;
    long m = (du - h * 3600) / 60;
    long s = du % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",h,m,s];
}

- (void)updatePlayedTime
{
    long du = self.player.duration;
    if (du > 0) {
        NSString *played = [self formatToTime:(long)self.player.position];
        NSString *duration = [self formatToTime:self.player.duration];
        self.durationLb.text = [NSString stringWithFormat:@"%@/%@",played,duration];
        self.slider.value = self.player.position / self.player.duration;
    } else {
        self.durationLb.text = @"--:--:--/--:--:--";
        self.slider.value = 0.0;
    }
}

- (void)setupAudioRender:(MRSampleFormat)fmt
{
    self.audioRender = [MR0x32AudioRenderer new];
    [self.audioRender setPreferredAudioQueue:YES];
    [self.audioRender active];
    //播放器使用的采样率
    [self.audioRender setupWithFmt:fmt sampleRate:self.player.supportedSampleRate];
    __weakSelf__
    [self.audioRender onFetchPacketSample:^UInt32(uint8_t * _Nonnull buffer, UInt32 bufferSize) {
        __strongSelf__
        UInt32 filled = [self.player fetchPacketSample:buffer wantBytes:bufferSize];
        return filled;
    }];
    
    [self.audioRender onFetchPlanarSample:^UInt32(uint8_t * _Nonnull left, UInt32 leftSize, uint8_t * _Nonnull right, UInt32 rightSize) {
        __strongSelf__
        UInt32 filled = [self.player fetchPlanarSample:left leftSize:leftSize right:right rightSize:rightSize];
        return filled;
    }];
}

- (IBAction)onTogglePause:(UIButton *)sender
{
    [sender setSelected:!sender.isSelected];
    if (sender.isSelected) {
        [self.player pause];
        [self.audioRender pause];
    } else {
        [self.player play];
        [self.audioRender paly];
    }
}

- (void)onTimer:(NSTimer *)sender
{
    if ([self.indicatorView isAnimating]) {
        [self.indicatorView stopAnimating];
    }
    [self appendMsg:[self.player peekPacketBufferStatus]];
    if (self.ignoreScrollBottom > 0) {
        self.ignoreScrollBottom --;
    } else {
        [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length, 1)];
    }
    
    [self updatePlayedTime];
}

- (void)appendMsg:(NSString *)txt
{
    self.textView.text = txt;//[self.textView.text stringByAppendingFormat:@"\n%@",txt];
}

//滑动时就暂停自动滚到到底部
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.ignoreScrollBottom = NSIntegerMax;
}

//松开手了，不需要减速就当即设定5s后自动滚动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        self.ignoreScrollBottom = 5;
    }
}

//需要减速时，就在停下来之后设定5s后自动滚动
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.ignoreScrollBottom = 5;
}

@end
