//
//  MetalVideoRender.m
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/13.
//

@import Metal;
@import MetalKit;
@import simd;
@import ModelIO;

#import "MetalVideoRender.h"
#import "ShaderTypes.h"

@interface MetalVideoRender ()<MTKViewDelegate>

@property (nonatomic, weak) VideoRenderView * mtView;

@property (nonatomic, assign) CVMetalTextureCacheRef textureCache;
@property (nonatomic, assign) vector_uint2 viewportSize;

@property (nonatomic, strong) id<MTLDevice> device;
@property (nonatomic, strong) id<MTLCommandQueue> commandQueue;
@property (nonatomic, strong) id<MTLRenderPipelineState> pipelineState;
@property (nonatomic, strong) id<MTLDepthStencilState> depth;

@property (nonatomic, strong) id<MTLBuffer> uniformsBuffer;
@property (nonatomic, strong) id<MTLTexture> diffuseTexture;
@property (nonatomic, strong) id<MTLSamplerState> samplerTexture;

@property (nonatomic, strong) id<MTLBuffer> vertices;
@property (nonatomic, strong) id<MTLBuffer> convertMatrix;  // YUV -> RBG
@property (nonatomic, assign) NSUInteger numVertices;

@end

@implementation MetalVideoRender

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupMetal];
    }
    return self;
}

- (instancetype)initWith:(VideoRenderView *)view {
    self = [super init];
    if (self) {
        [self setMetalView: view];
        CVMetalTextureCacheCreate(NULL, NULL, self.mtView.device, NULL, &_textureCache); // TextureCache的创建
        [self setupMetal];
        self.viewportSize = (vector_uint2){self.mtView.drawableSize.width, self.mtView.drawableSize.height};

    }
    return self;
}

- (void)setMetalView:(VideoRenderView *)view {
    self.mtView = view;
    view.framebufferOnly = false;
    view.device = MTLCreateSystemDefaultDevice();
    view.delegate = self;
}

-(void)setupMetal {
    [self setupPipeline];
    [self setupVertex];
    [self setupMatrix];
}

// 设置渲染管线
-(void)setupPipeline {
    _device = _mtView.device;
    id<MTLLibrary> defaultLibrary = [_device newDefaultLibrary]; // .metal
    id<MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vertexShader"]; // 顶点 shader，vertexShader 是函数名
    id<MTLFunction> fragmentFunction = [defaultLibrary newFunctionWithName:@"samplingShader"]; // 片元 shader，samplingShader 是函数名
    
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = _mtView.colorPixelFormat;
    self.pipelineState = [_device newRenderPipelineStateWithDescriptor: pipelineStateDescriptor
                                                                 error: NULL]; // 创建图形渲染管道，耗性能操作不宜频繁调用
    self.commandQueue = [_device newCommandQueue]; // CommandQueue是渲染指令队列，保证渲染指令有序地提交到GPU
}

// 设置顶点缓存
- (void)setupVertex {
    static const LYVertex quadVertices[] =
    {   // 顶点坐标，分别是x、y、z、w；    纹理坐标，x、y；
        { {  1.0, -1.0, 0.0, 1.0 },  { 1.f, 1.f } },
        { { -1.0, -1.0, 0.0, 1.0 },  { 0.f, 1.f } },
        { { -1.0,  1.0, 0.0, 1.0 },  { 0.f, 0.f } },
        
        { {  1.0, -1.0, 0.0, 1.0 },  { 1.f, 1.f } },
        { { -1.0,  1.0, 0.0, 1.0 },  { 0.f, 0.f } },
        { {  1.0,  1.0, 0.0, 1.0 },  { 1.f, 0.f } },
    };
    self.vertices = [_device newBufferWithBytes: quadVertices
                                                     length:sizeof(quadVertices)
                                                    options:MTLResourceStorageModeShared]; // 创建顶点缓存
    self.numVertices = sizeof(quadVertices) / sizeof(LYVertex); // 顶点个数
}

// 设置好转换的矩阵，BT.601 full range
- (void)setupMatrix {
    matrix_float3x3 kColorConversion601FullRangeMatrix = (matrix_float3x3){
        (simd_float3){1.0,   1.0,    1.0},
        (simd_float3){0.0,  -0.343,  1.765},
        (simd_float3){1.4,  -0.711,  0.0},
    };
    
    vector_float3 kColorConversion601FullRangeOffset = (vector_float3){ -(16.0/255.0), -0.5, -0.5}; // 这个是偏移
    
    LYConvertMatrix matrix;
    // 设置参数
    matrix.matrix = kColorConversion601FullRangeMatrix;
    matrix.offset = kColorConversion601FullRangeOffset;
    
    self.convertMatrix = [_device newBufferWithBytes: &matrix
                                              length: sizeof(LYConvertMatrix)
                                             options: MTLResourceStorageModeShared];
}

// 设置纹理
- (void)setupTextureWithEncoder:(id<MTLRenderCommandEncoder>)encoder buffer:(CVPixelBufferRef)pixelBuffer {
    
    id<MTLTexture> textureY = nil;
    id<MTLTexture> textureUV = nil;
    // textureY 设置
    {
        size_t width = CVPixelBufferGetWidthOfPlane(pixelBuffer, 0);
        size_t height = CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
        MTLPixelFormat pixelFormat = MTLPixelFormatR8Unorm; // 这里的颜色格式不是RGBA

        CVMetalTextureRef texture = NULL; // CoreVideo的Metal纹理
        CVReturn status = CVMetalTextureCacheCreateTextureFromImage(NULL, _textureCache, pixelBuffer, NULL, pixelFormat, width, height, 0, &texture);
        if(status == kCVReturnSuccess)
        {
            textureY = CVMetalTextureGetTexture(texture); // 转成Metal用的纹理
            CFRelease(texture);
        }
    }
    
    // textureUV 设置
    {
        size_t width = CVPixelBufferGetWidthOfPlane(pixelBuffer, 1);
        size_t height = CVPixelBufferGetHeightOfPlane(pixelBuffer, 1);
        MTLPixelFormat pixelFormat = MTLPixelFormatRG8Unorm; // 2-8bit的格式
        
        CVMetalTextureRef texture = NULL; // CoreVideo的Metal纹理
        CVReturn status = CVMetalTextureCacheCreateTextureFromImage(NULL, _textureCache, pixelBuffer, NULL, pixelFormat, width, height, 1, &texture);
        if(status == kCVReturnSuccess)
        {
            textureUV = CVMetalTextureGetTexture(texture); // 转成Metal用的纹理
            CFRelease(texture);
        }
    }
    
    if(textureY != nil && textureUV != nil)
    {
        [encoder setFragmentTexture:textureY
                            atIndex:LYFragmentTextureIndexTextureY]; // 设置纹理
        [encoder setFragmentTexture:textureUV
                            atIndex:LYFragmentTextureIndexTextureUV]; // 设置纹理
    }
    
    MTKTextureLoader *loader = [[MTKTextureLoader alloc] initWithDevice: _device];
    id<MTLTexture> textureLookUp = [loader newTextureWithCGImage: [UIImage imageNamed: @"lookupTable"].CGImage
                                         options: @{MTKTextureLoaderOptionSRGB:@(NO)}
                                           error: nil];
    [encoder setFragmentTexture: textureLookUp
                        atIndex: LYFragmentTextureIndexTextureTT];
    
}

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    NSLog(@"drawInMTKView--00 ");
    
    id <CAMetalDrawable> drawable = _mtView.currentDrawable;
    MTLRenderPassDescriptor *renderPassDescriptor = _mtView.currentRenderPassDescriptor;
    if (renderPassDescriptor && drawable && pixelBuffer) {
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0, 0.5, 0.5, 1.0f);
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture;
        renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        
        id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
        id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
        [renderEncoder setViewport:(MTLViewport){0.0, 0.0, self.viewportSize.x, self.viewportSize.y, -1.0, 1.0 }]; // 设置显示区域
        [renderEncoder setRenderPipelineState: _pipelineState];
        [renderEncoder setVertexBuffer:self.vertices
                                offset:0
                               atIndex:LYVertexInputIndexVertices]; // 设置顶点缓存

        [self setupTextureWithEncoder:renderEncoder buffer: pixelBuffer];
        [renderEncoder setFragmentBuffer:self.convertMatrix
                                  offset:0
                                 atIndex:LYFragmentInputIndexMatrix];

        [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangle
                          vertexStart:0
                          vertexCount:self.numVertices]; // 绘制

        [renderEncoder endEncoding]; // 结束

        [commandBuffer presentDrawable: drawable];
        [commandBuffer commit];
    }
}

- (void)drawInMTKView:(nonnull MTKView *)view {
    // NSLog(@"drawInMTKView--11 nouse");
    
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
    self.viewportSize = (vector_uint2){size.width, size.height};
}


///// Initializes a texture buffer from an external image.
//private static func makeTexture(device: MTLDevice, textureImage: CGImage) throws -> (texture: MTLTexture, sampler: MTLSamplerState) {
//    let loader = MTKTextureLoader(device: device)
//    let texture = try loader.newTexture(cgImage: textureImage, options: [.origin: MTKTextureLoader.Origin.bottomLeft, .generateMipmaps: true])
//
//    let samplerDescriptor = MTLSamplerDescriptor().set {
//        ($0.sAddressMode, $0.tAddressMode) = (.repeat, .repeat) //`repeat`
//        ($0.minFilter, $0.magFilter, $0.mipFilter) = (.linear, .linear, .linear)
//    }
//    guard let sampler = device.makeSamplerState(descriptor: samplerDescriptor) else { throw Error.failedToCreateMetalSampler(device: device) }
//    return (texture, sampler)
//}

@end
