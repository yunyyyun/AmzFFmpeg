//
//  VideoRenderView.m
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#import "VideoRenderView.h"
#import "MetalVideoRender.h"

@interface VideoRenderView ()

@property (nonatomic, strong) MetalVideoRender * render;

@end

@implementation VideoRenderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // [self setPaused: true];
        _render = [[MetalVideoRender alloc] initWith: self];
    }
    return self;
}

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    [_render displayPixelBuffer: pixelBuffer];
}

@end
