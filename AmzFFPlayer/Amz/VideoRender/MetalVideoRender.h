//
//  MetalVideoRender.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import <CoreVideo/CVPixelBuffer.h>
#import "VideoRenderView.h"

@interface MetalVideoRender : NSObject

// - (void)setupMetal;

// - (void)setMetalView:(VideoRenderView *)view;
- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer;
- (instancetype)initWith: (VideoRenderView *)view;

@end

