//
//  VideoRenderView.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoRenderView : MTKView

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end

NS_ASSUME_NONNULL_END
