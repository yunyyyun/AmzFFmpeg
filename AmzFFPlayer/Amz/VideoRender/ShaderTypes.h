//
//  Header.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#ifndef ShaderTypes_h
#define ShaderTypes_h

#include <simd/simd.h>

typedef struct
{
    vector_float4 position;
    vector_float2 textureCoordinate;
} LYVertex;


typedef struct {
    matrix_float3x3 matrix;
    vector_float3 offset;
} LYConvertMatrix;



typedef enum LYVertexInputIndex
{
    LYVertexInputIndexVertices     = 0,
} LYVertexInputIndex;


typedef enum LYFragmentBufferIndex
{
    LYFragmentInputIndexMatrix     = 0,
} LYFragmentBufferIndex;


typedef enum LYFragmentTextureIndex
{
    LYFragmentTextureIndexTextureY     = 0,
    LYFragmentTextureIndexTextureUV     = 1,
    LYFragmentTextureIndexTextureTT     = 2,
} LYFragmentTextureIndex;

//// BT.601, which is the standard for SDTV.
//static const GLfloat kColorConversion601[] = {
//        1.164,  1.164, 1.164,
//          0.0, -0.392, 2.017,
//        1.596, -0.813,   0.0,
//};
//
//// BT.709, which is the standard for HDTV.
//static const GLfloat kColorConversion709[] = {
//        1.164,  1.164, 1.164,
//          0.0, -0.213, 2.112,
//        1.793, -0.533,   0.0,
//};
//
//// BT.601 full range (ref: http://www.equasys.de/colorconversion.html)
//static const GLfloat kColorConversion601FullRange[] = {
//    1.0,    1.0,    1.0,
//    0.0,    -0.343, 1.765,
//    1.4,    -0.711, 0.0,
//};

#endif /* ShaderTypes_h */
