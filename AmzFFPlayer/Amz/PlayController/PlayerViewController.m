//
//  PlayerViewController.m
//  FFmpegTutorial-iOS
//
//  Created by qianlongxu on 2020/8/4.
//  Copyright © 2020 Matt Reach's Awesome FFmpeg Tutotial. All rights reserved.
//
//
#import <UIKit/UIKit.h>
#import "Define.h"
#import "PlayerViewController.h"
#import "MRRWeakProxy.h"
#import <GLKit/GLKit.h>
// #import "MR0x32VideoRenderer.h"
// #import "MetalVideoRender.h"
#import "VideoRenderView.h"
#import "MR0x32AudioRenderer.h"


@interface PlayerViewController ()<UITextViewDelegate,FFPlayer0x32Delegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (assign, nonatomic) NSInteger ignoreScrollBottom;
@property (weak, nonatomic) NSTimer *timer;

@property (assign, nonatomic) CGFloat playViewScale;
@property (assign, nonatomic) CGFloat originWidth;
@property (assign, nonatomic) CGFloat originHeight;
@property (weak, nonatomic) IBOutlet UIView *playView;

// @property (strong, nonatomic) MR0x32VideoRenderer *renderView;
@property (strong, nonatomic) VideoRenderView *mtRenderView;
@property (nonatomic, strong) MR0x32AudioRenderer *audioRender;
@property (nonatomic, assign) BOOL started;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic, assign) BOOL lockSlider;
@property (weak, nonatomic) IBOutlet UILabel *durationLb;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation PlayerViewController

- (void)dealloc
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    if (self.player) {
        [self.player stop];
        self.player = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"😄😄😄";
    [self.playButton setImage: [UIImage imageNamed: @"pause"] forState: UIControlStateNormal];
    [self.playButton setImage: [UIImage imageNamed: @"play"] forState: UIControlStateSelected];
    
    [self.indicatorView startAnimating];
    self.textView.delegate = self;
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    // self.renderView.contentMode = UIViewContentModeScaleAspectFill;
    
    FFPlayer0x32 *player = [[FFPlayer0x32 alloc] init];
    
    __weakSelf__
    [player onError:^{
        __strongSelf__
        [self.indicatorView stopAnimating];
        self.textView.text = [self.player.error localizedDescription];
        self.player = nil;
        [self.timer invalidate];
        self.timer = nil;
    }];
    
    _playViewScale = 1.0;
    [player onVideoEnds:^{
        __strongSelf__
        self.textView.text = @"Video Ends.";
        //fix position not end.
        [self updatePlayedTime];
        [self.player stop];
        self.player = nil;
        [self.timer invalidate];
        self.timer = nil;
    }];
    player.supportedPixelFormats  = MR_PIX_FMT_MASK_NV12;
    player.supportedSampleFormats = MR_SAMPLE_FMT_MASK_AUTO;
    
    //设置采样率，如果播放之前知道音频的采样率，可以设置成实际的值，可避免播放器内部转换！
    int sampleRate = [MR0x32AudioRenderer setPreferredSampleRate: 44100];
    player.supportedSampleRate = sampleRate;
    player.delegate = self;
    
    self.player = player;
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTap];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureAction:)];
    [self.view addGestureRecognizer:pinchGesture];
    
    [_slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];// 针对值变化添加响应方法
    _slider.continuous = false;
    self.lockSlider = false;
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    self.player.contentPath = self.playPath;
    [self.player prepareToPlay];
    [self.player readPacket];
    
    MRRWeakProxy *weakProxy = [MRRWeakProxy weakProxyWithTarget:self];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:weakProxy selector:@selector(onTimer:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.timer = timer;
    
}

-(void)pinchGestureAction:(UIPinchGestureRecognizer *)sender{
    NSLog(@"pinchGestureAction: %lf", sender.scale);
    if (_playViewScale < 0.2 && sender.scale < 1.0) {
        return;
    }
    if (_playViewScale > 2.0 && sender.scale > 1.0) {
        return;
    }
    _playViewScale = _playViewScale*(1.0 + (sender.scale-1.0) * 0.02);
    self.playView.transform = CGAffineTransformScale(CGAffineTransformIdentity, _playViewScale, _playViewScale);
}

-(void)doubleTapAction:(UITapGestureRecognizer *)sender{
    if (self.bottomView.alpha == 0.0) {
        self.bottomView.alpha = 1.0;
        self.textView.alpha = 1.0;
    } else {
        [UIView animateWithDuration: 1 animations:^{
            self.bottomView.alpha = 0.0;
            self.textView.alpha = 0.0;
        }];
    }
}

- (void)playAudio
{
    [self.audioRender paly];
    
}

- (void)reveiveFrameToRenderer:(CVPixelBufferRef)img
{
    CGFloat frameWidth = (CGFloat)CVPixelBufferGetWidth(img);
    CGFloat frameHeight = (CGFloat)CVPixelBufferGetHeight(img);
    
    CVPixelBufferRetain(img);
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (self.mtRenderView.frame.size.width/self.mtRenderView.frame.size.height != frameWidth/frameHeight) {
            CGFloat playViewWidth = self.playView.frame.size.width;
            CGFloat playViewHeight = self.playView.frame.size.height;
            CGFloat x = 0;
            CGFloat y = 0;
            CGFloat w = 0;
            CGFloat h = 0;
            if (frameWidth/frameHeight < playViewWidth/playViewHeight) {
                h = playViewHeight;
                w = h*frameWidth/frameHeight;
                x = (playViewWidth - w)/2;
                y = 0;
            } else {
                w = playViewWidth;
                h = w*frameHeight/frameWidth;
                x = 0;
                y = (playViewHeight - h)/2;
            }
            CGRect f = CGRectMake(x, y, w, h);
            // NSLog(@"cur_size_is: (%lf %lf)|(%lf %lf %lf %lf)", frameWidth, frameHeight, x, y, w, h);
            if (self.mtRenderView == nil) {
                self.mtRenderView = [[VideoRenderView alloc] initWithFrame: f];
                [self.playView addSubview: self.mtRenderView];
            } else {
                self.mtRenderView.frame = f;
            }
            self.originWidth = w;
            self.originHeight = h;
            
        } else {
            // NSLog(@"cur_size_is equal: (%lf %lf)", frameWidth, frameHeight);
        }
        
        
        [self.mtRenderView displayPixelBuffer:img];
        CVPixelBufferRelease(img);
        
        if (!self.started) {
            [self playAudio];
            self.started = true;
        }
    });
}

- (void)onInitAudioRender:(MRSampleFormat)fmt
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self setupAudioRender:fmt];
    });
}

- (void)onDurationUpdate:(long)du
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self updatePlayedTime];
    });
}

- (NSString *)formatToTime:(long)du
{
    long h = du / 3600;
    long m = (du - h * 3600) / 60;
    long s = du % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",h,m,s];
}

- (void)updatePlayedTime
{
    long du = self.player.duration;
    if (du > 0) {
        if (_lockSlider) {
            return;
        }
        NSString *played = [self formatToTime:(long)self.player.position];
        NSString *duration = [self formatToTime:self.player.duration];
        self.durationLb.text = [NSString stringWithFormat:@"%@/%@",played,duration];
        self.slider.value = self.player.position / self.player.duration;
    } else {
        self.durationLb.text = @"00:00:00/00:00:00";
        self.slider.value = 0.0;
    }
}

- (void)setupAudioRender:(MRSampleFormat)fmt
{
    self.audioRender = [MR0x32AudioRenderer new];
    [self.audioRender setPreferredAudioQueue:YES];
    [self.audioRender active];
    //播放器使用的采样率
    [self.audioRender setupWithFmt:fmt sampleRate:self.player.supportedSampleRate];
    __weakSelf__
    [self.audioRender onFetchPacketSample:^UInt32(uint8_t * _Nonnull buffer, UInt32 bufferSize) {
        __strongSelf__
        UInt32 filled = [self.player fetchPacketSample:buffer wantBytes:bufferSize];
        return filled;
    }];
    
    [self.audioRender onFetchPlanarSample:^UInt32(uint8_t * _Nonnull left, UInt32 leftSize, uint8_t * _Nonnull right, UInt32 rightSize) {
        __strongSelf__
        UInt32 filled = [self.player fetchPlanarSample:left leftSize:leftSize right:right rightSize:rightSize];
        return filled;
    }];
}

- (IBAction)onTogglePause:(UIButton *)sender
{
    [sender setSelected:!sender.isSelected];
    if (sender.isSelected) {
        [self.player pause];
        [self.audioRender pause];
    } else {
        [self.player play];
        [self.audioRender paly];
    }
}

- (void)onTimer:(NSTimer *)sender
{
    if ([self.indicatorView isAnimating]) {
        [self.indicatorView stopAnimating];
    }
    [self appendMsg:[self.player peekPacketBufferStatus]];
    if (self.ignoreScrollBottom > 0) {
        self.ignoreScrollBottom --;
    } else {
        [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length, 1)];
    }
    
    [self updatePlayedTime];
}

- (void)appendMsg:(NSString *)txt
{
    self.textView.text = [self.textView.text stringByAppendingFormat:@"\n%@",txt];
}

//滑动时就暂停自动滚到到底部
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.ignoreScrollBottom = NSIntegerMax;
}

//松开手了，不需要减速就当即设定5s后自动滚动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        self.ignoreScrollBottom = 5;
    }
}

//需要减速时，就在停下来之后设定5s后自动滚动
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.ignoreScrollBottom = 5;
}

// slider变动时改变label值
- (void)sliderValueChanged:(id)sender {
    UISlider *slider = (UISlider *)sender;
    NSLog(@"asdasd is %.1f %ld", slider.value, self.player.duration);
    [self.player seekTo: slider.value * self.player.duration];
    
    self.lockSlider = true;
    dispatch_after(1.0, dispatch_get_main_queue(), ^{
        self.lockSlider = false;
    });
}

- (IBAction)closePage:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

- (BOOL)prefersHomeIndicatorAutoHidden {
    return true;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled: true];
    [self.navigationController setNavigationBarHidden:true animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled: false];
    [self.navigationController setNavigationBarHidden:false animated:animated];
}

@end
