//
//  PlayerViewController.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/15.
//

#import <UIKit/UIKit.h>
#import "FFPlayer0x32.h"

@interface PlayerViewController : UIViewController

@property (nonatomic, strong) NSString *playPath;
@property (nonatomic, strong) FFPlayer0x32 *player;

@end
