//
//  PlayViewCell.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *videoNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;

- (void) updateText: (NSString *) title;
+ (CGSize) itemSize;

@end

NS_ASSUME_NONNULL_END
