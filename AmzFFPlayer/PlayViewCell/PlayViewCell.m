//
//  PlayViewCell.m
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/22.
//

#import "PlayViewCell.h"

@implementation PlayViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat w = [PlayViewCell itemSize].width;
    self.contentView.layer.cornerRadius = w/16;
    self.contentView.layer.masksToBounds = true;
}

- (void) updateText: (NSString *) title {
    if ([title isEqualToString: @"add"]) {
        [_addImageView setHidden: false];
        _videoNameLabel.text = @"";
        self.contentView.backgroundColor = [UIColor systemOrangeColor];
    } else {
        [_addImageView setHidden: true];
        _videoNameLabel.text = [title stringByReplacingOccurrencesOfString: @"." withString: @"\n."];
        if ([title containsString: @".json"]) {
            self.contentView.backgroundColor = [UIColor systemBlueColor];
        } else {
            self.contentView.backgroundColor = [UIColor systemGreenColor];
        }
        
    }
}

+ (CGSize) itemSize {
    CGFloat w = 100;
    return CGSizeMake(w, w * 0.8);
}

@end
