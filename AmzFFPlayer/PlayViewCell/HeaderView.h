//
//  HeaderView.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *pathLabel;

@end

NS_ASSUME_NONNULL_END
