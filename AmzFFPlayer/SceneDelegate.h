//
//  SceneDelegate.h
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/14.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

