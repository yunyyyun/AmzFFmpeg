//
//  ViewController.m
//  AmzFFPlayer
//
//  Created by meng yun on 2020/12/14.
//

#import "ViewController.h"
#import "PlayerViewController.h"
#import "VideosFileManager.h"
#import "PlayViewCell.h"
#import "HeaderView.h"
#import "AddFileViewController.h"

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray<NSArray<NSString *> *> *subPathFiles;
@property (strong, nonatomic) NSMutableArray<NSString *> *headerTitles;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"💢💢💢";
    [self initPlayViews];
    
    
//    UIImageView *imgV = [[UIImageView alloc] initWithFrame: CGRectMake(40, 100, 333, 222)];
//    imgV.image = [self ttt: [UIImage imageNamed: @"111"] nnn: [UIImage imageNamed: @"222"]];
//    [self.view addSubview: imgV];
}

- (UIImage *)ttt: (UIImage *)fr nnn:(UIImage *) bg {
    UIGraphicsBeginImageContext(CGSizeMake(333, 222));
    
    [bg drawAtPoint: CGPointZero];
    [fr drawAtPoint: CGPointMake(33, 22)];
    
    UIImage *dd = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return dd;
}

- (void)initPlayViews {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _collectionView.collectionViewLayout = layout;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerNib: [UINib nibWithNibName: @"PlayViewCell" bundle: nil] forCellWithReuseIdentifier: @"PlayViewCell"];
    
    [_collectionView registerNib: [UINib nibWithNibName: @"HeaderView" bundle: nil]
      forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
             withReuseIdentifier: @"HeaderView"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    NSDictionary *d = [[VideosFileManager getFileLevelList: docPath] mutableCopy];
    self.subPathFiles = [[NSMutableArray alloc] init];
    self.headerTitles = [[NSMutableArray alloc] init];
    [d enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, NSArray<NSString *> * obj, BOOL * _Nonnull stop) {
        [_headerTitles addObject: key];
        [_subPathFiles addObject: [obj sortedArrayUsingSelector: @selector(compare:)]];
        
    }];
    [_collectionView reloadData];
    
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PlayViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"PlayViewCell" forIndexPath: indexPath];
    
    NSInteger index = indexPath.row;
    if (index < _subPathFiles[indexPath.section].count) {
        NSString *title = [[_subPathFiles[indexPath.section][index] componentsSeparatedByString: @"/"] lastObject];
        [cell updateText: title];
    } else {
        [cell updateText: @"add"];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row;
    NSString *videoPath = @"";
    if (index < _subPathFiles[indexPath.section].count) {
        videoPath = _subPathFiles[indexPath.section][index];
        if ([videoPath containsString: @"json"]) {  // 内容是 url
            NSData *d = [VideosFileManager readFileData: videoPath];
            NSString *s = [[NSString alloc] initWithData: d encoding: NSUTF8StringEncoding];
            videoPath = s;
        }
        PlayerViewController *vc = [[PlayerViewController alloc] init];
        vc.playPath = videoPath;
        [self.navigationController pushViewController: vc animated: true];
    } else {
        NSLog(@"click to add");
        AddFileViewController *vc = [[AddFileViewController alloc] initWithNibName: @"AddFileViewController" bundle: nil];
        if (_subPathFiles.count <= indexPath.section || _subPathFiles[indexPath.section].count <= 0) {
            vc.rootPath = [NSHomeDirectory() stringByAppendingString: @"/Documents/"];
        } else {
            NSString *s = _subPathFiles[indexPath.section][0];
            NSString *last = [[s componentsSeparatedByString: @"/"] lastObject];
            vc.rootPath = [s stringByReplacingOccurrencesOfString: last withString: @""];
        }
        
        [self.navigationController pushViewController: vc animated: true];
    }
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _subPathFiles[section].count + 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return _headerTitles.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    HeaderView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier: @"HeaderView" forIndexPath: indexPath];
    reusableview.pathLabel.text = [_headerTitles[indexPath.section] stringByAppendingString: @":"];
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 88);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [PlayViewCell itemSize];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 18;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 18;
}

@end
